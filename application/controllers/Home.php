<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    private $num_rows = 20;

    public function __construct()
    {
        parent::__construct();
        $this->load->Model('admin/Brands_model');$this->load->Model('admin/Products_model');
    }

    public function index($page = 0)
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('home');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['countQuantities'] = $this->Public_model->getCountQuantities();
		//print_r($data['countQuantities']);
        $data['bestSellers'] = $this->Public_model->getbestSellers();
        $data['newProducts'] = $this->Public_model->getNewProducts();
		//print_r($data['bestSellers']);
        $data['sliderProducts'] = $this->Public_model->getSliderProducts();
		$data['sliders'] = $this->Public_model->getAllActiveSlider();
        $data['lastBlogs'] = $this->Public_model->getLastBlogs();
        $data['products'] = $this->Public_model->getProducts($this->num_rows, $page, $_GET);
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
        $data['links_pagination'] = pagination('home', $rowscount, $this->num_rows);
        $this->render('home', $head, $data);
    }
public function is_logged_in(){
        
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $is_logged_in = $this->session->userdata('logged_user');
       
        if($is_logged_in=="")
        { //echo $is_logged_in; die();
            redirect('login');
        }
    }
    /*
     * Used from medical template
     * shop page
     */

    public function shop($page = 0)
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);$data['newProducts'] = $this->Public_model->getNewProducts();
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        $data['products'] = $this->Public_model->getProducts($this->num_rows, $page, $_GET);
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['links_pagination'] = pagination('shop/', $rowscount, $this->num_rows);
        $this->render('shop', $head, $data);
    }
  public function medicines_by_brands($page = 0)
    {  $data = array();
        $head = array();
		$brand_id="";
		if(isset($_GET) && !empty($_GET["brands_id"]))
		{
			$brand_id = $_GET["brands_id"];
		}
		$arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
		$data['product_by_brands'] = $this->Brands_model->get_product_by_brand($this->num_rows, $page, $_GET);
		$data['newProducts'] = $this->Public_model->getNewProducts();
		$data['brand_name'] = $this->Brands_model->get_brand_name($brand_id);
		
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        
        $rowscount = $this->Public_model->productsCount_brand($_GET,$brand_id);
		$data['links_pagination'] = pagination('medicines-by-brands', $rowscount, $this->num_rows);
       // $data['links_pagination'] = pagination('medicines-by-brands', $rowscount, $this->num_rows);
	//$this->load->view('templates/medical/medicines_by_brands',$data );
	$this->render('medicines_by_brands',$head ,$data);
	} 
	
	
	public function medicines_by_disease($page = 0)
    {  $data = array();
        $head = array();
		 $disease=$this->uri->segment(2); 
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands'); 
		$data['newProducts'] = $this->Public_model->getNewProducts();
        $data['disease'] = $this->Brands_model->get_disease();
		$data['product_by_disease'] = $this->Brands_model->get_product_by_disease($disease);
		$data['disease_name'] = $this->Brands_model->get_disease_name($disease);
		
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        $data['products'] = $this->Public_model->getProducts($this->num_rows, $page, $_GET);
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['links_pagination'] = pagination('home', $rowscount, $this->num_rows);
	
	$this->render('medicines_by_disease',$head ,$data);
	}
	public function online_medicine_order($page = 0)
    {  $data = array();
        $head = array();
		$brand_id=$this->uri->segment(3);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
		//$data['product_by_brands'] = $this->Brands_model->get_product_by_brand($brand_id);
		$data['brand_name'] = $this->Brands_model->get_brand_name($brand_id);
		
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        $data['products'] = $this->Public_model->getProducts($this->num_rows, $page, $_GET);
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['links_pagination'] = pagination('home', $rowscount, $this->num_rows);
	
	$this->render('online_medicine_order',$head ,$data);
	}
	public function variants($page = 0)
    {  $data = array();
        $head = array();
		$brand_id=$this->uri->segment(3);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
		$data['product_by_brands'] = $this->Brands_model->get_product_by_brand($brand_id);
		$data['brand_name'] = $this->Brands_model->get_brand_name($brand_id);
		
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');
        $data['products'] = $this->Public_model->getProducts($this->num_rows, $page, $_GET);
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['links_pagination'] = pagination('home', $rowscount, $this->num_rows);
	//$this->load->view('templates/medical/variants',$data );
	$this->render('variants',$head ,$data);
	}
	public function upload_prescription($page = 0)
    {  $data = array();
        $head = array();
		$this->is_logged_in();
		$brand_id=$this->uri->segment(3);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $all_categories = $this->Public_model->getShopCategories();
        $data['home_categories'] = $this->getHomeCategories($all_categories);
        $data['all_categories'] = $all_categories;
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
        $data['brands'] = $this->Brands_model->getBrands();
	//	$data['product_by_brands'] = $this->Brands_model->get_product_by_brand($brand_id);
		$data['brand_name'] = $this->Brands_model->get_brand_name($brand_id);
		
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');      
	$this->render('upload_prescription',$head ,$data);
	}
	public function ask_expert()
    {  $data = array();
        $head = array();
		$brand_id=$this->uri->segment(3);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		if (isset($_POST['send'])) {
            $result = $this->sendEmail_con();
            if ($result) {
                $this->session->set_flashdata('resultSend', 'Email is sened!');
            } else {
                $this->session->set_flashdata('resultSend', 'Email send error!');
            }
            redirect('/ask-expert');
        }
	   $this->render('ask_experts',$head ,$data);
	} 
	public function add_to_compair()
    {  $data = array();
        $head = array();
		$brand_id=$this->uri->segment(3);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$Ip_address = $_SERVER['REMOTE_ADDR'];
        $data['compair_list_items'] = $this->Public_model->get_compair_list_items($Ip_address);
        $data['showBrands'] = $this->Home_admin_model->getValueStore('showBrands');
		
        $data['brands'] = $this->Brands_model->getBrands();
		$data['product_by_brands'] = $this->Brands_model->get_product_com_brand($brand_id);
		
		$data['brand_name'] = $this->Brands_model->get_brand_name($brand_id);
		$data['page'] ='';
        $data['showOutOfStock'] = $this->Home_admin_model->getValueStore('outOfStock');
        $data['shippingOrder'] = $this->Home_admin_model->getValueStore('shippingOrder');       
        $rowscount = $this->Public_model->productsCount($_GET);
        $data['links_pagination'] = pagination('home', $rowscount, $this->num_rows);
		
	    $this->render('add_to_compair',$head ,$data);
	}  
	
	
public function add_to_compair_list() {
$Ip_address = $_SERVER['REMOTE_ADDR'];
$product_id =$this->uri->segment(2);
		$data = array(
			'product_id' => $product_id,
            'Ip_address' => $Ip_address,			
			'status' => 1
			);

		$wish['check_compair'] = $this->Public_model->check_compair($data);
		$data11['product'] = $this->Public_model->getOneProduct($product_id);
		if (!empty($wish['check_compair'])) {
			$w_id = $wish['check_compair'][0]['c_id'];
			$status = $wish['check_compair'][0]['status'];
			if ($status == 0) {
				$st = 1;
			} else {
				$st = 0;
			}
			$data1 = array(
				'status' => $st,
			);
			
			// set flash data
        $this->session->set_flashdata('flash_welcome', $data11['product']['title']."&nbsp;&nbsp;".'Added To Comair.');
         
			$this->db->where('c_id', $w_id);
			$this->db->update('product_compair', $data1);
			redirect("add-to-compare"); 
			
		} else {
			$this->session->set_flashdata('flash_welcome', $data11['product']['title']."&nbsp;&nbsp;".'Added To compare.');
			$result = $this->Public_model->insert_compair_list($data);
			if ($result) {
				
				
				$this->session->set_flashdata('message',$data['product']['title']. ' Added To compare List.'); 
				redirect("add-to-compare");
			}

		}
	
	} 
	
	public function about_us()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'About Us';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('about',$head ,$data);
	}
	public function return_policy()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'About Us';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('about',$head ,$data);
	}
	public function product_support()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'About Us';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('product_support',$head ,$data);
	}
	public function customer_service()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'About Us';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('customer_service',$head ,$data);
	}public function faqs()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH faqs';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('faqs',$head ,$data);
	} 
	public function track_order()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH track order';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		if (isset($_POST['track'])) {
          $order_id=$this->input->post('order_id');
           $email=$this->input->post('email');
		   $data["order_id"] =$order_id;
		   $data["email"] =$email;
		   $data["my_order_status"]=$this->Public_model->get_track_order($order_id,$email);	
          	//print_r($data["my_order_status"]); die();
        }
	    $this->render('track_order',$head ,$data);
	}
	public function save_prescription()
	{ 
			$user_id=$this->session->userdata('logged_user');
			$status="Pending Review";
			$p_id=$this->input->post('p_id');
            $ff ='';
			  $img='';
			if(!empty($_FILES['image-file']['name'])) {				
				$imgDir = "attachments/prescription/";
				
					$allowed = array('image/jpg','image/jpeg','image/png','image/gif');
				            $ext = pathinfo($_FILES['image-file']['name'], PATHINFO_EXTENSION);
				 
				 if(!in_array($_FILES['image-file']['type'],$allowed)) 
						  { 
							 $msg='Sorry, only JPG ,PNG files Are allowed.';
								$this->session->set_flashdata('message', $msg);
	                              redirect('upload-prescription','refresh');
					           									
					 
						 }else{
				
				$file_name = $_FILES["image-file"]["name"];			
			    // $extension = end((explode(".", $file_name)));	
 $tmp = explode('.', $file_name);
$extension = end($tmp);				 
			    $upload_file = $imgDir.$file_name;	
				if(move_uploaded_file($_FILES['image-file']['tmp_name'],$upload_file)){			  
				$source_image = $imgDir.$file_name;
				$img = "compress-".$file_name;
			    $image_destination = $imgDir.$img;
				 $compress_images = $this->compressImage($source_image, $image_destination);			 
			}
						 }
		 } else{

				
				if($this->input->post('old_pic')!='')
				{
					$img=$this->input->post('old_pic');
					
					
				}
				else{
					$img='';
				}
			}
			$data=array(
					'user_id' => $user_id,
					'prescription_image'=> $img,	
					'status' => $status,'payment_status' => "Pending",'order_status' => "Pending Call Review Yet",
					'uploaded_date' => date("y-m-d")
				);

			if($p_id!='')
			{
				$this->db->where('prescription_id', $p_id);
				$this->db->update('users_prescription', $data);
				$this->session->set_flashdata('message', 'Your Prescription updated Successfully..');
				redirect('prescription-process');
			}
			 
				else {
					$this->Public_model->insert_prescription($data);	
					$this->session->set_flashdata('message', 'Your Prescription Save Successfully..');
					redirect('prescription-process');
				}
			
	}
	
	public function upload_prescription_process()
    {  $data = array();
        $head = array();
	//	print_r($_POST); die();
		if($this->input->post('save')!=FALSE)
		{  
	$p_id = $this->input->post('p_id');
	$patient_name = $this->input->post('patient_name');
	$flat_name = $this->input->post('flat_name');
	$street_name = $this->input->post('street_name');
	$address = $this->input->post('address');
	$mobile_number = $this->input->post('mobile_number');
	$pincode = $this->input->post('pincode');
	      
		  $data=array(
					'patient_name' => $patient_name,'flat_no' => $flat_name,
					'street_name' => $street_name,
					'address' => $address,
					'phone' => $mobile_number,
					'Pincode' => $pincode,
					'order_id' => "ASH-".time(),
					'is_deleted' =>'0'
				);
			  $this->db->where('prescription_id', $p_id);
				$this->db->update('users_prescription', $data);
				$this->session->set_flashdata('message', 'Your Prescription updated Successfully..');
			redirect('my-prescription');	
				
		}
		
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription Upload process';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('prescription_process',$head ,$data);
	}
	public function my_prescription()
    {  $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription Upload process';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		$user_id=$this->session->userdata('logged_user');
		$data["prescriptions"]=$this->Public_model->get_users_prescription($user_id);
	    $this->render('my_prescription',$head ,$data);
	}
	
	public function orders_detail()
    {  $data = array();
        $head = array();
		$id =$this->uri->segment(2);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription orders Detail';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		$user_id=$this->session->userdata('logged_user');
		$data["single_item"]=$this->Public_model->get_single_prescription($id);
		$data['newProducts'] = $this->Public_model->getNewProducts();
	    $this->render('orders_details',$head ,$data);
	}
	
	public function orders_detail_shop($page = 0)
    {  $data = array();
        $head = array();
		$id =$this->uri->segment(2);
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription orders Detail';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		$data['userInfo'] = $this->Public_model->getUserProfileInfo($_SESSION['logged_user']);
        $rowscount = $this->Public_model->getUserOrdersHistoryCount($_SESSION['logged_user']);
        $data["single_order"]=$this->Public_model->get_single_order($id); 
		$data["single_address"]=$this->Public_model->get_single_order_address($data["single_order"]["id"]);
		$data['newProducts'] = $this->Public_model->getNewProducts();
        $data['links_pagination'] = pagination('myaccount', $rowscount, $this->num_rows, 2);
		
	    $this->render('orders_details_shop',$head ,$data);
	}
	//////////////////Delete Function///////
	public function delete_pres() {
		 $id=$this->uri->segment(2); 
		 $data=array(
					'is_deleted' => '1'
				);
		$this->db->where('prescription_id', $id);
		$this->db->update('users_prescription', $data);
		$this->session->set_flashdata('message', 'Your Order deleted Successfully..');
		redirect('my-prescription');
	}
	public function ajaxPro()
    {
        $query = $this->input->get('query');
		$this-> db->select('title as name');
        $this->db->like('title', $query);


        $data = $this->db->get("products_translations")->result_array();
    
        echo json_encode($data); die();
    }
    private function getHomeCategories($categories)
    {

        /*
         * Tree Builder for categories menu
         */

        function buildTree(array $elements, $parentId = 0)
        {
            $branch = array();
            foreach ($elements as $element) {
                if ($element['sub_for'] == $parentId) {
                    $children = buildTree($elements, $element['id']);
                    if ($children) {
                        $element['children'] = $children;
                    }
                    $branch[] = $element;
                }
            }
            return $branch;
        }

        return buildTree($categories);
    }

    /*
     * Called to add/remove quantity from cart
     * If is ajax request send POST'S to class ShoppingCart
     */

    public function manageShoppingCart()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->shoppingcart->manageShoppingCart();
    } 
	

    /*
     * Called to remove product from cart
     * If is ajax request and send $_GET variable to the class
     */

    public function removeFromCart()
    {
        $backTo = $_GET['back-to'];
        $this->shoppingcart->removeFromCart();
        $this->session->set_flashdata('deleted', lang('deleted_product_from_cart'));
        redirect(LANG_URL . '/' . $backTo);
    }

    public function clearShoppingCart()
    {
        $this->shoppingcart->clearShoppingCart();
    }

    public function viewProduct($id)
    {
        $data = array();
        $head = array();
        $data['product'] = $this->Public_model->getOneProduct($id);
		$Ip_address = $_SERVER['REMOTE_ADDR'];
        $data['viewed_product'] = $this->Public_model->get_all_viewed_pro($Ip_address,$id); 
		$data['viewed_product'] = $this->Public_model->get_all_viewed_pro_ip($Ip_address);
		$data_insert=array("product_id"=>$id,
			            "date_time"=>date("Y-m-d h:i:s"),"Ip_address"=>$Ip_address);
		if(!empty($data['viewed_product'])){
			
				foreach($data['viewed_product'] as $pro){		
			$data['single_viewed'] = $this->Public_model->get_single_viewed_pro($Ip_address);
			//echo $data['single_viewed']["product_id"]."</br>".$pro["product_id"];
			if($data['single_viewed']["product_id"]==$pro["product_id"]){
			$this->db->where('v_id', $pro["v_id"]);
				$this->db->update('viewed_product', $data_insert);	
			}else{
		//$this->Public_model->insert_view_product($data_insert);
			}
				}
				
		 
			
		}else{
		$this->Public_model->insert_view_product($data_insert);
			}
        $data['sameCagegoryProducts'] = $this->Public_model->sameCagegoryProducts($data['product']['shop_categorie'], $id);
        if ($data['product'] === null) {
            show_404();
        }
        $vars['publicDateAdded'] = $this->Home_admin_model->getValueStore('publicDateAdded');
        $this->load->vars($vars);
        $head['title'] = $data['product']['title'];
        $description = url_title(character_limiter(strip_tags($data['product']['description']), 130));
        $description = str_replace("-", " ", $description) . '..';
        $head['description'] = $description;
        $head['keywords'] = str_replace(" ", ",", $data['product']['title']);
        $this->render('view_product', $head, $data);
    }
public function add_to_wish_list() {
	
	if($this->session->userdata('logged_user')){
$user_id = $this->session->userdata('logged_user');
$product_id =$this->uri->segment(2);
		$data_inser = array(
			'user_id' => $user_id,
			'product_id' => $product_id,			
			'status' => 1
		);

		$wish['check_wish'] = $this->Public_model->check_wish($data);
		$data['product'] = $this->Public_model->getOneProduct($product_id);
		if (!empty($wish['check_wish'])) {
			$w_id = $wish['check_wish'][0]['w_id'];
			$status = $wish['check_wish'][0]['status'];
			if ($status == 0) {
				$st = 1;
			} else {
				$st = 0;
			}
			$data1 = array(
				'status' => $st,
			);
			
			// set flash data
        $this->session->set_flashdata('flash_welcome', $data['product']['title']."&nbsp;&nbsp;".'Added To Wishlist.');
         
			$this->db->where('w_id', $w_id);
			$this->db->update('wish_list', $data_inser);
			redirect($_SERVER['HTTP_REFERER']); 
			
		} else {
			$this->session->set_flashdata('flash_welcome', $data['product']['title']."&nbsp;&nbsp;".'Added To Wishlist.');
			$result = $this->Public_model->insert_wish_list($data_inser);
			if ($result) {
				
				
			$this->session->set_flashdata('message',$data['product']['title']. ' Added To Wishlist.'); 
				redirect($_SERVER['HTTP_REFERER']);
			}

		}
	}else{
		redirect('login');
	}
	}

	public function delete_wish_list($w_id) {
		$this->db->where('w_id', $w_id);
		$this->db->delete('wish_list');
		redirect($_SERVER['HTTP_REFERER']);
	}
	public function delcpmitem($w_id) { 
		
		$this->db->where('c_id', $w_id);
		//$this->db->where('Ip_address', $w_id);
		$this->db->delete('product_compair');// die($w_id);
		redirect($_SERVER['HTTP_REFERER']);
	} 
    public function confirmLink($md5)
    {
        if (preg_match('/^[a-f0-9]{32}$/', $md5)) {
            $result = $this->Public_model->confirmOrder($md5);
            if ($result === true) {
                $data = array();
                $head = array();
                $head['title'] = '';
                $head['description'] = '';
                $head['keywords'] = '';
                $this->render('confirmed', $head, $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function discountCodeChecker()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $result = $this->Public_model->getValidDiscountCode($_POST['enteredCode']);
        if ($result == null) {
            echo 0;
        } else {
            echo json_encode($result);
        }
    }
    public function compailist()
    {  $data = array();
        $head = array();
		
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription orders Detail';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
		$user_id=$this->session->userdata('logged_user');
		$Ip_address = $_SERVER['REMOTE_ADDR'];
		$data["compair_list_items"]=$this->Public_model->get_compair_list_items($Ip_address);
	    $this->render('compair_view',$head ,$data);
	}
    public function sitemap()
    {
        header("Content-Type:text/xml");
        echo '<?xml version="1.0" encoding="UTF-8"?>
                <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $products = $this->Public_model->sitemap();
        $blogPosts = $this->Public_model->sitemapBlog();

        foreach ($blogPosts->result() as $row1) {
            echo '<url>

      <loc>' . base_url('blog/' . $row1->url) . '</loc>

      <changefreq>monthly</changefreq>

      <priority>0.1</priority>

   </url>';
        }

        foreach ($products->result() as $row) {
            echo '<url>

      <loc>' . base_url($row->url) . '</loc>

      <changefreq>monthly</changefreq>

      <priority>0.1</priority>

   </url>';
        }

        echo '</urlset>';
    }
public function compressImage($source_image, $compress_image) {
		$image_info = getimagesize($source_image);	
		if ($image_info['mime'] == 'image/jpeg') { 
			$source_image = imagecreatefromjpeg($source_image);
			imagejpeg($source_image, $compress_image, 75);
		} elseif ($image_info['mime'] == 'image/gif') {
			$source_image = imagecreatefromgif($source_image);
			imagegif($source_image, $compress_image, 75);
		} elseif ($image_info['mime'] == 'image/png') {
			$source_image = imagecreatefrompng($source_image);
			imagepng($source_image, $compress_image, 6);
		}	    
		return $compress_image;
	}
	private function sendEmail_con()
    {
        $myEmail = $this->Home_admin_model->getValueStore('contactsEmailTo');
        if (filter_var($myEmail, FILTER_VALIDATE_EMAIL) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $this->load->library('email');

            $this->email->from($_POST['email'], $_POST['name']);
            $this->email->to($myEmail);

            $this->email->subject($_POST['subject']);
            $this->email->message($_POST['message']);

            $this->email->send();
            return true;
        }
        return false;
    } public function forget_pass()
    {$data = array();
        $head = array();
	
		if($this->input->post('forget')!=FALSE)
		{  
	     $email = $this->input->post('email');
		 $chk = $this->Public_model->countPublicUsersWithEmail($email,0);	
		 if($chk>0){ 
		 
			 $this->session->set_flashdata('message', 'If there is an account associated  you will receive an email with a link to reset your password.');
			 redirect(base_url()."login");
		 }	else{
			 $this->session->set_flashdata('message', 'We are unable to locate the requested login information. Please contact aspharmapvtltd for help.');
		 }	
		}
		
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription Upload process';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('forget_pass',$head ,$data);
	}
	public function pass_reset()
    {$data = array();
        $head = array();
	$user_id =$this->uri->segment(2);
		if($this->input->post('resetpass')!=FALSE)
		{  
	     $pass1 = $this->input->post('newpass');  
		 $pass1 = $this->input->post('renewpass');
		
		 //echo md5($user_id);
		 $chk = $this->Public_model->getUserProfileInfo(0);	
		 if($chk>0){ 
		 
			 $this->session->set_flashdata('message', 'If there is an account associated  you will receive an email with a link to reset your password.');
			 redirect(base_url()."login");
		 }	else{
			 $this->session->set_flashdata('message', 'We are unable to locate the requested login information. Please contact aspharmapvtltd for help.');
		 }	
		}
		
        $arrSeo = $this->Public_model->getSeo('shop');
        $head['title'] = 'ASH prescription Upload process';
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
		$data['page'] ='';
	    $this->render('forget_pass_reset',$head ,$data);
	}
}
