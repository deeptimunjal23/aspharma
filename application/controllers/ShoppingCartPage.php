<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ShoppingCartPage extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->Model('Public_model');
		$this->load->Model('admin/Brands_model');
    }

    public function index()
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Public_model->getSeo('shoppingcart');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
       //$data['cartItems']['array']=''; 
	   $data['brands'] = $this->Brands_model->getBrands();//$this->load->view("templates/medical/top_head");
		 //$this->load->view("templates/medical/shopping_cart", $data);
      $this->render('shopping_cart',$head ,$data);
    }

}
