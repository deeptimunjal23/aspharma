<div id="products">
    <?php
    if ($this->session->flashdata('result_delete')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_delete') ?></div>
        <hr>
        <?php
    }
    if ($this->session->flashdata('result_publish')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
        <hr>
        <?php
    } 
    ?>
    <h1><img src="<?= base_url('assets/imgs/products-img.png') ?>" class="header-img" style="margin-top:-2px;"> ORDER DETAILS</h1>
	<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-success" style="text-align:center">      
    <?php echo $this->session->flashdata('message')?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-sm-9" style="margin-top: 1px;">
        <div class="orderDetailsContent">
   <div class="" id="orderStatusContainer">
      <div class="mapTitle showDesktop">Order Details</div>
      <div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606">Name: </span><span class="_374y6 _3aKoJ" style="color:red">
			<?php echo $single_address["first_name"];?></span></div>
            <div><span class="_2tYiJ"style="color:#d88606"> Order No: </span><span class=""><strong># <?php echo $single_order["order_id"];?></strong></span>
			</br>
Placed On: <?= date('d.M.Y / H:i:s', $single_order['date']); ?></div>
         </div>
         <div class="_2vCS5">
           
           </div>
      </div>
   </div>
   <div id="orderStatusContainer">
      <div class="_2O7an">Delivery Address Details</div>
      <div class="_3Tg-d">
         
         <div class="_10XI8 ZKHfL">
            <div class="_3W6vY"><?php echo ucfirst($single_address["first_name"]);?></div>
            <div class="_365Xc">  <?php echo $single_address["address"];?></div>
            <div class="_365Xc"> <?php echo $single_address["first_name"];?></div>
            <div class="_365Xc"><?php echo $single_address["city"];?></div> 
			<div class="_365Xc">Pincode : <?php echo $single_address["post_code"];?></div>
			<div class="_365Xc"><img width="20" src="https://image.flaticon.com/icons/svg/35/35459.svg">  <?php echo $single_address["phone"];?></div>
         </div>
      </div>
   </div>
   <div class="_3Tg">
      <div class="HOjXH">
         <div>Products</div>
        
      </div>
      <div>
         <div class="_3Tg-d">
		 <ul class="tabled-data">
            <?php $arr_products = unserialize($single_order['products']);
			foreach ($arr_products as $product_id => $product_quantity) {
				$productInfo = modules::run('admin/ecommerce/products/getProductInfo', $product_quantity['product_info']["id"], true);
				$product_name = $this->Products_model->get_single_product($productInfo["id"]);
				//print_r($product_quantity);?>
			
                        <li>
                            
                            <div class="value"><img src="<?= base_url('attachments/shop_images/' . $productInfo['image']) ?>" alt="Product" style="width:100px; margin-right:10px;" class="img-responsive">
				</br>
							</div>
                        </li>
                        <li>
                      <label>   <a  class="_1zv5A" href="<?php echo base_url().$product_name["url"];?>"><?php echo $product_name["title"];?></a></label>
                        </li>
                        <li>
                            <label>Qty</label>
                            <div class="value"><?php echo $product_quantity["product_quantity"]; 
			
			?></div>
                        </li>
						<?php }?>
                    </ul>
			
            <div class="_9ocZk">
</br>
<?php if(!empty($single_address['notes'])){?>
<h3>Extra Note </h3>
<ul class="tabled-data">
                        <li>
                        
                            <div class="value"><?php echo $single_address['notes'];?></div>
                        </li>
						</ul>
<?php } ?>
</div>
         </div>
      </div>
   </div>
  <div class="_3Eaqj">
      <div class="HOjXH">
         <div>Payment Detail</div>
         <div class="_1xNu6"></div>
      </div>
      <div>
         <div class="_3Tg-d">
            
            <div class="_9ocZk">
			<ul class="tabled-data">
                        <li>
                            <label>Amount</label>
                            <div class="value">
							<?php if(!empty($single_order['amount'])){echo "Rs ".$single_order['amount'];}else{ echo "Pending Amount estimate";} ?></div>
                        </li>
                        <li>
                            <label>Payment Type</label>
                            <div class="value"><?php echo $single_order['payment_type'];?></div>
                        </li>
                        <li>
                            <label>Order Status</label>
                            <div class="value">
							<?php if($single_order['processed']=="0")
							{ echo "processed";}else{
							echo "Completed";	
							}?>
							</div>
                        </li>
                        
                        <li>
                            <label></label>
                            <div class="value"></div>
                        </li>
                    </ul>
			</div> 
         </div>
      </div>
   </div><div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606"> </span><span class="_374y6 _3aKoJ" style="color:red">
			</span>  <div class="table-responsive">
                <table class="table">
                 
                  <tbody>
                    <tr>
                      <td>Order Status</td>
                      <td><select class="form-control" name="order_status" required="">
					  <option value=""></option>
					  <option <?php if($single_order["order_status"]=="canceled"){ echo "selected";}; ?> value="canceled">Canceled</option>
					  <option <?php if($single_order["order_status"]=="canceled"){ echo "selected";}; ?> value="closed">Closed</option>
					  
					  <option <?php if($single_order["order_status"]=="Pending Call Review Yet"){ echo "selected";}; ?> value="Pending Call Review Yet">Pending Call Review Yet</option><option <?php if($single_order["order_status"]=="fraud"){ echo "selected";}; ?> value="fraud">Suspected Fraud</option>
					  <option <?php if($single_order["order_status"]=="holded"){ echo "selected";}; ?> value="holded">On Hold</option><option <?php if($single_order["order_status"]=="payment_review"){ echo "selected";}; ?> value="payment_review">Payment Review</option><option <?php if($single_order["order_status"]=="pending"){ echo "selected";}; ?> value="pending">Pending</option><option <?php if($single_order["order_status"]=="pending_payment"){ echo "selected";}; ?> value="pending_payment">Pending Payment</option><option <?php if($single_order["order_status"]=="pending_paypal"){ echo "selected";}; ?> value="pending_paypal">Pending PayPal</option><option <?php if($single_order["order_status"]=="processing"){ echo "selected";}; ?> value="processing">Processing</option></select></td>
                      
                    </tr>
                    <tr><td>Extra Note</td><td colspan="2"><textarea type="text" name="admin_extra_note"><?php echo $single_order["admin_extra_note"]; ?></textarea>
					</td></tr>
					
					<tr><td></td><td></td><td><button type="submit" name="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button></td></tr>
                    
                  </tbody>
                </table>
              </div>
            
         </div>
            <div><span class="_2tYiJ" style="color:#d88606"></span><span class=""><strong></strong></span></div>
         </div>
       </div>
</div>
</div>   
      <style>.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}.card-title {
    margin-bottom: .75rem;
    font-weight: 600;
    font-size: 16px;
    color: #1a262b;
}</style><link rel="stylesheet" href="<?php echo base_url()?>/assets/med/css/order.css">
<style>#container {position: absolute; height: 100px; width: 170px; top: 200px; left: 200px;}

.popover {top: 0; left: 0px; position: relative;}

.big_img {height: 250px; position: absolute; top: -40px; left: 180px; display:none;}

#container:hover .big_img {display:block;}.tabled-data li:first-child {
    padding-top: 0;
}
.tabled-data li {
    border-bottom: 1px solid #e0e0e0;
    color: #3d3d3d;
    font-size: 14px;
    padding: 7px 0;
}</style>