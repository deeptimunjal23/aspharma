<script src="<?= base_url('assets/ckeditor/ckeditor.js') ?>"></script>
<h1 style="color: #337ab7;
    font-size: 15px;
    font-weight: 600"><img src="<?= base_url('assets/imgs/shop-cart-add-icon.png') ?>" class="header-img" style="margin-top:-3px;"> Basic Drug Information</h1>
<hr>
<?php
$timeNow = time();
if (validation_errors()) {
    ?>
    <hr>
    <div class="alert alert-danger"><?= validation_errors() ?></div>
    <hr>
    <?php
}
if ($this->session->flashdata('result_publish')) {
    ?>
    <hr>
    <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
    <hr>
    <?php
}
?><section class="content">
                <div class="row">
            <div class="col-sm-12">
                <div class="column">
                                         <a href="#" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> Go To Product List</a>
                    
                                         <a href="#" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>Manage Medicine </a>
                                    </div>
            </div>
        </div>

             <!-- Add Product -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>New Medicine</h4>
                        </div>
                    </div>
                    
					
					<form method="POST" action="<?php echo base_url(); ?>admin/publish" enctype="multipart/form-data">
    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : $timeNow ?>" name="folder" accept-charset="utf-8">
                    <div class="panel-body">
					
                  <?php
    $i = 0;
    foreach ($languages as $language) {
        ?><div class="locale-container locale-container-<?= $language->abbr ?>" <?= $language->abbr == MY_DEFAULT_LANGUAGE_ABBR ? 'style="display:block;"' : '' ?>><input type="hidden" name="translations[]" value="<?= $language->abbr ?>">
                           <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="product_name" class="col-sm-4 col-form-label">Medicine Name <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" tabindex="1"  type="text" id="product_name" placeholder="Medicine Name" required="" name="title[]" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['title']) ? $trans_load[$language->abbr]['title'] : '' ?>" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                               <div class="form-group row">
                                    <label for="generic_name" class="col-sm-4 col-form-label">Generic Name  <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
									
                                        <input class="form-control" name="generic_name" type="text" id="generic_name" placeholder="Generic name" tabindex="2" required="required" value="<?php if(isset($single_item)){
		echo $single_item['genric_name'];
		}
		?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
							   <div class="form-group row">
                                    <label for="product_model" class="col-sm-4 col-form-label">Medicine Type <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
 <select class="form-control" id="category_id1" name="medicine_type[]" required="required" >
    <option value="">Select One</option>
 
  <option value="Tablet" <?php if(isset($single_item)){if($single_item['medicine_type']=="Tablet"){echo "selected";} }?> >Tablet</option>
                                        
                                            <option value="Liquid" <?php if(isset($single_item)){if($single_item['medicine_type']=="Liquid"){echo "selected";} }?>>Liquid</option>
<option value="Injection" <?php if(isset($single_item)){if($single_item['medicine_type']=="Injection"){echo "selected";} }?>>Injection</option>
<option value="Spray" <?php if(isset($single_item)){if($single_item['medicine_type']=="Spray"){echo "selected";} }?>>Spray</option><option value="Syrup" <?php if(isset($single_item)){if($single_item['medicine_type']=="Syrup"){echo "selected";} }?>>Syrup</option>
<option value="Capsul" <?php if(isset($single_item)){if($single_item['medicine_type']=="Capsul"){echo "selected";} }?>>Capsul</option>
                                        
                                                                                </select>
                                    </div>
                                </div>
                            
                              </div>
                           <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="unit" class="col-sm-4 col-form-label">Unit  <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2-hidden-accessible" id="unit" name="unit" required="required">
										
                                             <option value="">Select One</option>
											 <?php foreach($all_unit as $u){?>
											 <option <?php if(isset($single_item)){if($single_item['unit']==$u["unit_id"]){echo "selected";} }?> value="<?php echo $u["unit_id"];?>"><?php echo $u["name"];?></option>
											 <?php } ?>
                                           
 
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
 <div class="row"><div class="col-sm-6">
                             </div>
                         </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="product_name" class="col-sm-4 col-form-label">Sell  Price <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" tabindex="1" id="input-14" name="price[]" placeholder="price" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['price']) ? $trans_load[$language->abbr]['price'] : '' ?>" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                               <div class="form-group row">
                                    <label for="generic_name" class="col-sm-4 col-form-label">MRP Price <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="old_price[]" placeholder="Special Price" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['old_price']) ? $trans_load[$language->abbr]['old_price'] : '' ?>" id="input-15" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>  
						
						<div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Weight</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="input-14" name="Weight" value="<?php  if(isset($single_item)){ echo $single_item["weight"];  } ?>">
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label">Stock</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="stock">
  <option <?php if(isset($single_item)){if($single_item['stock_status']=="1"){echo "selected";} }?> value="1">In Stock</option>
<option <?php if(isset($single_item)){if($single_item['stock_status']=="0"){echo "selected";} }?> value="0">Out of Stock</option>
                                  </select>
                  </div>
                </div>
 <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label"> Product  New from </label>
                  <div class="col-sm-4">
				  <input type="hidden" value="<?php if(isset($single_item)){ echo $single_item['id']; }else{ echo "0";}?>" name="pro_id">
                    <input type="date" class="form-control" id="txtFrom" name="date_new_from" value="<?php if(isset($single_item)){
						echo $single_item['new_from_date'];}?>" >
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label"> Product  New to </label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" id="txtTo" name="date_new_to" value="<?php if(isset($single_item)){
						echo$single_item['new_to_date'];}?>" >
                  </div>
                </div>
				<div class="row">
                  <label for="input-6" class="col-sm-2 col-form-label">Status</label>
                  <div class="col-sm-4">
    <select class="form-control valid" id="input-6" name="status" required="">
                     
  <option <?php if(isset($single_item)){if($single_item['status']=="Enabled"){echo "selected";} }?> value="Enabled">Enabled</option>
 <option  <?php if(isset($single_item)){if($single_item['status']=="Disabled"){echo "selected";} }?> value="Disabled">Disabled</option>
                    </select>
                  </div>
                </div>
			
				
					  </div>
                                   </div>
            </div>
        </div>
    </section>
<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
			  
			  
                  <i class="fa fa-user-circle-o"></i>
                   General Info
                </h4>
                 <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
             
                 <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Short Description</label>
                  <div class="col-sm-10">
                    <textarea name="basic_description[]" id="basic_description<?= $i ?>" rows="50" class="form-control"><?= $trans_load != null && isset($trans_load[$language->abbr]['basic_description']) ? $trans_load[$language->abbr]['basic_description'] : '' ?></textarea>
                    <script>
                        CKEDITOR.replace('basic_description<?= $i ?>');
                        CKEDITOR.config.entities = false;
                    </script>
                  </div>
                </div>
				
<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                    <textarea name="description[]" id="description<?= $i ?>" rows="50" class="form-control"><?= $trans_load != null && isset($trans_load[$language->abbr]['description']) ? $trans_load[$language->abbr]['description'] : '' ?></textarea>
                <script>
                    CKEDITOR.replace('description<?= $i ?>');
                    CKEDITOR.config.entities = false;
                </script>
                  </div>
                </div>

				<?php
        $i++;
    }
    ?>				
				
                
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Other Info
                </h4>
            
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Cover Image</label>
				  <?php
        if (isset($_POST['image']) && $_POST['image'] != null) {
            $image = 'attachments/shop_images/' . $_POST['image'];
            if (!file_exists($image)) {
                $image = 'attachments/no-image.png';
            }
            ?>
                  <div class="col-sm-10">
                    <img src="<?= base_url($image) ?>" class="img-responsive img-thumbnail" style="max-width:300px; margin-bottom: 5px;">
                  </div>
                </div>
		<input type="hidden" name="old_image" value="<?= $_POST['image'] ?>">
            <?php if (isset($_GET['to_lang'])) { ?>
                <input type="hidden" name="image" value="<?= $_POST['image'] ?>">
			<?php }
        }
        ?>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label"</label>
				  <input type="file" id="userfile" name="userfile">
				  </div>
                  <div class="col-sm-10">
                    <div class="form-group bordered-group">
        <div class="others-images-container">
            <?= $otherImgs ?>
        </div>
		
        <a href="javascript:void(0);" data-toggle="modal" data-target="#modalMoreImages" class="btn btn-default">Upload more images</a>
    </div>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Shop Categories</label>
                  <div class="col-sm-10">
                    <select class=" form-control " name="shop_categorie">
					<option value="">Select Categories</option>
            <?php foreach ($shop_categories as $key_cat => $shop_categorie) { ?>
                <option <?= isset($_POST['shop_categorie']) && $_POST['shop_categorie'] == $key_cat ? 'selected=""' : '' ?> value="<?= $key_cat ?>">
                    <?php
                    foreach ($shop_categorie['info'] as $nameAbbr) {
                        if ($nameAbbr['abbr'] == $this->config->item('language_abbr')) {
                            echo $nameAbbr['name'];
                        }
                    }
                    ?>
                </option>
            <?php } ?>
        </select>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Quantity</label>
                  <div class="col-sm-10">
                    <input type="number" placeholder="number" name="quantity" value="<?= @$_POST['quantity'] ?>" class="form-control" id="quantity">
                  </div>
                </div>
				 <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Brand</label>
                  <div class="col-sm-10">
                   <select class="form-control" name="brand_id">
			   <option value="">Select brands</option>
                <?php foreach ($brands as $brand) { ?>
                    <option 

<?php if(isset($single_item))
					{ if($brand['id']==$single_item['brand_id']){echo "selected";}}?>					 value="<?= $brand['id'] ?>"><?= $brand['name'] ?></option>
                <?php } ?>
            </select>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Medicine Category</label>
                  <div class="col-sm-10">
 <select class="form-control" name="medicine_category">
<option value="">Select Medicine Category </option>
<option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Antibiotics"){echo "selected";} }?> value="Antibiotics">Antibiotics </option>
<option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Allopathic"){echo "selected";} }?> value="Antibiotics">Allopathic </option> 
<option  <?php if(isset($single_item)){if($single_item['medicinec_category']=="Paracetamol"){echo "selected";} }?> value="Paracetamol">Paracetamol </option> <option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Histasinee"){echo "selected";} }?> value="Histasinee">Histasinee </option>   <option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Analgesics"){echo "selected";} }?>  value="Analgesics">Analgesics </option> <option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Antiseptics"){echo "selected";} }?> value="Antiseptics">Antiseptics </option>  <option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Antipyretics"){echo "selected";} }?> value="Antipyretics">Antipyretics </option><option <?php if(isset($single_item)){if($single_item['medicinec_category']=="Mood Stabilizers"){echo "selected";} }?> value="Mood Stabilizers">Mood Stabilizers </option>
            </select>
                 
                  </div>
                </div>
				
				<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Disease</label>
                  <div class="col-sm-10">
                   <select class="form-control" id="disease_name" name="disease_name">
			<?php $disease =$this->Products_model->get_disease();?>
			     <option value="">Select Disease</option>
                <?php foreach ($disease as $diseases) { ?>
                    <option <?php if(isset($single_item))
					{ if($diseases['id']==$single_item['disease']){echo "selected";}}						?> value="<?= $diseases['id'] ?>"><?= $diseases['name'] ?></option>
                <?php } ?>
            </select>
                  </div>
                </div>
				
                <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Feature product</label>
                  <div class="col-sm-10">
                    <select  class="form-control" name="is_feature">
		
		
		<option <?php if(isset($single_item)){
		if($single_item['is_feature']=="0"){ echo "selected";}
		}
		?> value="0">No</option>
		<option <?php if(isset($single_item)){ if($single_item['is_feature']=="1"){ echo "selected";} }?> value="1">Yes</option>
		
		</select>
                  </div>
                </div>
				
				<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Position</label>
                  <div class="col-sm-10">
                       <input type="text" placeholder="Position number" name="position" value="<?= @$_POST['position'] ?>" class="form-control">
                  </div>
                </div>
                <div class="form-footer">
                    <a href="<?= base_url('admin/products') ?>"class="btn btn-danger" name="submit"><i class="fa fa-times"></i> CANCEL</a>
                   
					<button type="submit" name="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Publish</button>
                </div>
              
            </div>
          </div>
        </div>
      </div></form><!--End Row-->

               
			 
            </div>
         

		 </div>
       
	   </div>
      </div>
<div class="modal fade" id="modalMoreImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload more images</h4>
            </div>
            <div class="modal-body">
                <form id="uploadImagesForm">
                    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : $timeNow ?>" name="folder">
                    <label for="others">Select images</label>
                    <input type="file" name="others[]" id="others" multiple />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default finish-upload">
                    <span class="finish-text">Finish</span>
                    <img src="<?= base_url('assets/imgs/load.gif') ?>" class="loadUploadOthers" alt="">
                </button>
            </div>
        </div>
    </div>
</div>
<!-- virtualProductsHelp -->
<div class="modal fade" id="virtualProductsHelp" tabindex="-1" role="dialog" aria-labelledby="virtualProductsHelp">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">What are virtual products?</h4>
            </div>
            <div class="modal-body">
                Sometimes we want to sell products that are for electronic use such as books. In the box below, you can enter links to products that can be downloaded after you confirm the order as "Processed" through the "Orders" tab, an email will be sent to the customer entered with the entire text entered in the "virtual products" field.
                We have left only the possibility to add links in this field because sometimes it is necessary that the electronic stuff you provide for downloading will be uploaded to other servers. If you want, you can add your files to "file manager" and take the links to them to add to the "virtual products"
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
