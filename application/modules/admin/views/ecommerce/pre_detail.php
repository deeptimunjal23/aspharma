<div id="products">
    <?php
    if ($this->session->flashdata('result_delete')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_delete') ?></div>
        <hr>
        <?php
    }
    if ($this->session->flashdata('result_publish')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
        <hr>
        <?php
    } 
    ?>
    <h1><img src="<?= base_url('assets/imgs/products-img.png') ?>" class="header-img" style="margin-top:-2px;"> ORDER DETAILS</h1>
	<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-success" style="text-align:center">      
    <?php echo $this->session->flashdata('message')?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="well hidden-xs"> 
                <div class="row">
                    <form method="POST" id="searchProductsForm" action="<?php echo base_url(); ?>admin/prescription-order-detail/<?php echo $single_item["prescription_id"]; ?>">
                     <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Order # <?php echo$single_item["order_id"]; ?> </h5>
			  <div class="table-responsive">
               <table class="table table-sm">
                <thead>
                  <tr>
                    <th scope="col">Patient Name</th>
                    <th scope="col"><?php echo ucfirst($single_item["patient_name"]); ?></th>
                   
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
</th>
                  </tr>
                </tbody>
              </table>
               <table class="table table-sm">
                <thead>
                  <tr>
                    <th scope="col">DELIVERY ADDRESS DETAILS:</th>
                    <th scope="col"></th>
                   
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Flat No. : <?php echo $single_item["flat_no"];?>
</th>
                  </tr><tr>
                    <th scope="row">Street Name. : <?php echo $single_item["street_name"];?>
</th>
                  </tr><tr>
                    <th scope="row"> Address : <?php echo $single_item["address"];?>
</th>
                  </tr><tr>
                    <th scope="row"> Pincode : <?php echo $single_item["pincode"];?>
</th>
                  </tr>
				  <tr>
                    <th scope="row"> <img width="20" src="https://image.flaticon.com/icons/svg/35/35459.svg">  <?php echo $single_item["phone"];?>
</th>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Order Date : <?php 
			   echo date("d-M-Y", strtotime($single_item["uploaded_date"]))."\n";?></h5>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Order status</th>
                      <th scope="col"><?php if($single_item["is_deleted"]=="1"){?>
                  <span class="u-boldText" style="color: rgb(240, 89, 101);">Order Cancelled</span>
				 <?php }else{ ?>
				 <span class="u-boldText" style="color:green"><?php echo $single_item["order_status"]; ?></span>
				 <?php } ?>
				 
				 </th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row"></th>
                      <?php if($single_item["amount"]=="0.00"){?>
					  <td>Amount</td>
                      <?php }else{?>
					   <td>Payment Amount</td>
					   <?php } ?>
					  
					  
                      <?php if($single_item["amount"]=="0.00"){?>
					  <td><input type="number" class="form-control" name="amount"></td>
                      <?php }else{?>
					   <td><?php echo $single_item["amount"];?></td>
					   <?php } ?>
                    </tr>
                    
                    
                  </tbody>
                </table>
              </div>
			  <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Payment Status</th>
                      <th scope="col"><?php if($single_item["payment_status"]!=""){?>
                  <span class="u-boldText" style="color: rgb(240, 89, 101);"><?php echo $single_item["payment_status"];?></span>
				
				 <?php } ?></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row"></th>
                      <td>Order Status</td>
                      <td><select class="form-control" name="order_status" required="">
					  <option value=""></option>
					  <option <?php if($single_item["order_status"]=="canceled"){ echo "selected";}; ?> value="canceled">Canceled</option>
					  <option <?php if($single_item["order_status"]=="canceled"){ echo "selected";}; ?> value="closed">Closed</option>
					  
					  <option <?php if($single_item["order_status"]=="Pending Call Review Yet"){ echo "selected";}; ?> value="Pending Call Review Yet">Pending Call Review Yet</option><option <?php if($single_item["order_status"]=="fraud"){ echo "selected";}; ?> value="fraud">Suspected Fraud</option>
					  <option <?php if($single_item["order_status"]=="holded"){ echo "selected";}; ?> value="holded">On Hold</option><option <?php if($single_item["order_status"]=="payment_review"){ echo "selected";}; ?> value="payment_review">Payment Review</option><option <?php if($single_item["order_status"]=="pending"){ echo "selected";}; ?> value="pending">Pending</option><option <?php if($single_item["order_status"]=="pending_payment"){ echo "selected";}; ?> value="pending_payment">Pending Payment</option><option <?php if($single_item["order_status"]=="pending_paypal"){ echo "selected";}; ?> value="pending_paypal">Pending PayPal</option><option <?php if($single_item["order_status"]=="processing"){ echo "selected";}; ?> value="processing">Processing</option></select></td>
                      
                    </tr>
                    <tr><td>Extra Note</td><td colspan="2"><textarea type="text" name="admin_extra_note"><?php echo $single_item["admin_extra_note"]; ?></textarea>
					
					<input name="p_id"type="hidden" value="<?php echo $single_item["prescription_id"]; ?>"/></td></tr>
					
					<tr><td></td><td></td><td><button type="submit" name="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button></td></tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div> <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">MEDICINES </h5>
			  <div class="table-responsive">
               <table class="table table-sm">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">
					</th>
                   <?php	if(!empty($single_item['prescription_image'])) {
									$file_name =$single_item['prescription_image'];
									$fileUrl = base_url().'attachments/prescription/'.$file_name;
?>
                    <img  src="<?php echo $fileUrl; ?>"  />
                  </tr>
				  <tr><td>
				  
				 <?php $file = $fileUrl;
 echo '<p><a taget="_blank" href="'.$file.'">Download</a>'?>
				  </td></tr><?php } ?>
                </thead>
                
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
					 </form>
                </div>
            </div>
            <hr>
          
    </div>
	<style>.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}.card-title {
    margin-bottom: .75rem;
    font-weight: 600;
    font-size: 16px;
    color: #1a262b;
}</style>