<div id="products">
    <?php
    if ($this->session->flashdata('result_delete')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_delete') ?></div>
        <hr>
        <?php
    }
    if ($this->session->flashdata('result_publish')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
        <hr>
        <?php
    } 
    ?>
    <h1><img src="<?= base_url('assets/imgs/products-img.png') ?>" class="header-img" style="margin-top:-2px;"> medicine unit </h1>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="column">
                                         <a href="<?php echo base_url()?>/admin/add-unit" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> Add</a>
                    
                                         
                                    </div>
            <hr>
            <?php
            if ($get_all_unit) {
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th>Sr No</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sr=1;
                            foreach ($get_all_unit as $key => $value) {?>
                                <tr> 
								<td><?php echo $value["name"];?></td>
                                    <td>
                                        <div class="pull-right">
                                            <a href="<?= base_url('admin/edit-unit/' . $value["unit_id"]) ?>" class="btn btn-info">Edit</a>
                                            <a href="<?= base_url('admin/delete_unit/?delete=' . $value["unit_id"]) ?>"  class="btn btn-danger confirm-delete">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                              <?php         $sr++;
                              }
                          
                          ?>
                        </tbody>
                    </table>
                </div>
                <?//= //$links_pagination ?>
            </div>
            <?php
        } else {
            ?>
            <div class ="alert alert-info">No unit found!</div>
        <?php } ?>
    </div>