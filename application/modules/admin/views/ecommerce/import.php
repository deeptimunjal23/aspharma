
<h1><img src="<?= base_url('assets/imgs/blogger.png') ?>" class="header-img" style="margin-top:-2px;">Upload Product</h1>
<hr>
<div class="row">
    <div class="col-sm-8 col-md-7">
        <?php if (validation_errors()) { ?>
            <hr>
            <div class="alert alert-danger"><?= validation_errors() ?></div>
            <hr>
        <?php }
        ?>
        <?php if ($this->session->flashdata('result_publish')) { ?>
            <hr>
            <div class="alert alert-danger"><?= $this->session->flashdata('result_publish'); ?></div>
            <hr>
        <?php }
        ?>
        <form id="personal-info" action="<?php echo base_url(); ?>admin/import" id="jq-validation-form" enctype="multipart/form-data" method="post">
            
                
                <div class="form-group"> 
                    <label>select file </label>
                    <input type="file" class="form-control" id="csv" placeholder="Enter Unit " name="csv">
					
					
                </div>
                <div class="form-group">
                

                <button type="submit" name="save" class="btn btn-default">save</button>
            <?php  ?>
                <a href="<?= base_url('admin/publish') ?>" class="btn btn-info">Cancel</a>
            <?php  ?>
            </div>
            
        </form>
    </div>
</div>