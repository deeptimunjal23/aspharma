<div id="products">
    <?php
    if ($this->session->flashdata('result_delete')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_delete') ?></div>
        <hr>
        <?php
    }
    if ($this->session->flashdata('result_publish')) {
        ?>
        <hr>
        <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
        <hr>
        <?php
    } 
    ?>
    <h1><img src="<?= base_url('assets/imgs/products-img.png') ?>" class="header-img" style="margin-top:-2px;"> Prescription Orders</h1>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            
            <hr>
            <?php
            if ($products) {
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Order Id</th>
                                <th>Patient Name</th>
                                <th>Order Status</th>
                               
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($products as $row) {
                                
								$u_path = base_url().'attachments/prescription/';
								 echo $u_path .$row['prescription_image'];
                                if ($row['prescription_image'] != "" && file_exists($u_path .$row['prescription_image'])) {
                                    $image = base_url($u_path . $row['prescription_image']);
                                } else {
                                    $image = base_url('attachments/no-image.png');
                                }
                                ?>

                                <tr>
                                    <td>
                                        <img src="<?= $image ?>" alt="No Image" class="img-thumbnail" style="height:100px;">
                                    </td>
                                    <td>
                                        <?= $row["order_id"];?>
                                    </td>
                                    <td>
                                        <?= $row["patient_name"] ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($row["order_status"] =="SHIPPING") {
                                            $color = 'label-success';
                                        }
                                        if ($row["order_status"] =="Pending Call Review Yet") {
                                            $color = 'label-warning';
                                        }
                                        if ($row["order_status"] =="SHIPPING") {
                                            $color = 'label-danger';
                                        }
                                        ?>
                                        <span style="font-size:12px;" class="label <?= $color ?>">
                                            <?= $row["order_status"] ; ?>
                                        </span>
                                    </td>
                                    
                                    
                                    <td>
                                        <div class="pull-right">
                                            <a href="<?= base_url('admin/prescription-order-detail/' . $row["prescription_id"]) ?>" class="btn btn-info">View</a>
                                            <a href="<?= base_url('admin/products?delete=' . $row["prescription_id"]) ?>"  class="btn btn-danger confirm-delete">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?//= //$links_pagination ?>
            </div>
            <?php
        } else {
            ?>
            <div class ="alert alert-info">No Order found!</div>
        <?php } ?>
    </div>