
<h1><img src="<?= base_url('assets/imgs/blogger.png') ?>" class="header-img" style="margin-top:-2px;"> Shipping Methods</h1>
<hr>
<div class="row">
    <div class="col-sm-8 col-md-7">
        <?php if (validation_errors()) { ?>
            <hr>
            <div class="alert alert-danger"><?= validation_errors() ?></div>
            <hr>
        <?php }
        ?>
        <?php if ($this->session->flashdata('result_publish')) { ?>
            <hr>
            <div class="alert alert-danger"><?= $this->session->flashdata('result_publish'); ?></div>
            <hr>
        <?php }
        ?>
        <form id="personal-info" action="<?php echo base_url(); ?>admin/shipping-method" id="jq-validation-form" enctype="multipart/form-data" method="post">
            
                
                <div class="form-group"> 
                    <label>Title </label>
                    <input type="text" class="form-control" id="title" placeholder="Enter title " name="title"  value="<?php if(!empty($shipping)){ echo $shipping['title'];} ?>" required>
					
					
                </div><div class="form-group"> 
                    <label>Method Name </label>
                    <input type="text" class="form-control" id="method_name" placeholder="Enter Method Name " name="method_name"  value="<?php if(!empty($shipping)){ echo $shipping['method_name'];} ?>" required>
					
					
                </div>
			<?php /*	<div class="form-group"> 
                    <label>Type </label>
 <select id="carriers_type" name="carriers_type" class="form-control">
<option <?php if($shipping['type']=="O"){echo "selected";}?> value="O">Per Order</option>
<option <?php if($shipping['type']=="I"){echo "selected";}?> value="I" >Per Item</option>
</select>
</div> */?>
<div class="form-group"> 
 <label> Handling Fee Type </label>
 <select class="form-control" name="handling_fee_type">
<option <?php if($shipping['handling_fee_type']=="F"){echo "selected";}?> value="F" selected="selected">Fixed</option>
<option <?php if($shipping['handling_fee_type']=="P"){echo "selected";}?> value="P">Percent</option>
</select>
					
					
                </div>
				<div class="form-group"> 
                    <label>Amount </label>
                    <input type="text" class="form-control" id="handling_fee" placeholder="Enter handling fee " name="handling_fee"  value="<?php if(!empty($shipping)){ echo $shipping['handling_fee'];} ?>" required>
                </div>
				<div class="form-group"> 
                    <label>Free Shipping Over amount </label>
                    <input type="text" class="form-control" id="free_shipping" placeholder="Enter handling fee " name="free_shipping"  value="<?php if(!empty($shipping)){ echo $shipping['free_shipping'];} ?>" required>
                </div>
				
                <div class="form-group">
                
<input type="hidden"  name="shipping_id"  value="<?php if(!empty($shipping)){ echo $shipping['shipping_id'];} ?>" >
                <button type="submit" name="save" class="btn btn-default">save</button>
            <?php  ?>
                <a href="<?= base_url('admin/home') ?>" class="btn btn-info">Cancel</a>
            <?php  ?>
            </div>
            
        </form>
    </div>
</div>