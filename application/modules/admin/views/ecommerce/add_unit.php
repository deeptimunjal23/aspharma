
<h1><img src="<?= base_url('assets/imgs/blogger.png') ?>" class="header-img" style="margin-top:-2px;"> Upload slider</h1>
<hr>
<div class="row">
    <div class="col-sm-8 col-md-7">
        <?php if (validation_errors()) { ?>
            <hr>
            <div class="alert alert-danger"><?= validation_errors() ?></div>
            <hr>
        <?php }
        ?>
        <?php if ($this->session->flashdata('result_publish')) { ?>
            <hr>
            <div class="alert alert-danger"><?= $this->session->flashdata('result_publish'); ?></div>
            <hr>
        <?php }
        ?>
        <form id="personal-info" action="<?php echo base_url(); ?>admin/save-unit" id="jq-validation-form" enctype="multipart/form-data" method="post">
            
                
                <div class="form-group"> 
                    <label>Name </label>
                    <input type="text" class="form-control" id="name" placeholder="Enter Unit " name="name"  value="<?php if(!empty($single_unit)){ echo $single_unit['name'];} ?>" required>
					
					
                </div>
                <div class="form-group">
                
<input type="hidden"  name="unit_id"  value="<?php if(!empty($single_unit)){ echo $single_unit['unit_id'];} ?>" >
                <button type="submit" name="submit" class="btn btn-default">save</button>
            <?php  ?>
                <a href="<?= base_url('admin/medicine-unit') ?>" class="btn btn-info">Cancel</a>
            <?php  ?>
            </div>
            
        </form>
    </div>
</div>