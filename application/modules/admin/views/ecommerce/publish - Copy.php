<script src="<?= base_url('assets/ckeditor/ckeditor.js') ?>"></script>
<h1><img src="<?= base_url('assets/imgs/shop-cart-add-icon.png') ?>" class="header-img" style="margin-top:-3px;"> Basic Drug Information</h1>
<hr>
<?php
$timeNow = time();
if (validation_errors()) {
    ?>
    <hr>
    <div class="alert alert-danger"><?= validation_errors() ?></div>
    <hr>
    <?php
}
if ($this->session->flashdata('result_publish')) {
    ?>
    <hr>
    <div class="alert alert-success"><?= $this->session->flashdata('result_publish') ?></div>
    <hr>
    <?php
}
?><section class="content">
                <div class="row">
            <div class="col-sm-12">
                <div class="column">
                                         <a href="http://pharmacyv5.bdtask.com/version8.1/Cproduct/add_product_csv" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> Import Medicine (CSV)</a>
                    
                                         <a href="http://pharmacyv5.bdtask.com/version8.1/Cproduct/manage_product" class="btn btn-primary m-b-5 m-r-2"><i class="ti-align-justify"> </i>  Manage Medicine gg</a>
                                    </div>
            </div>
        </div>

             <!-- Add Product -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>New Medicine</h4>
                        </div>
                    </div>
                    
					
					<form method="POST" action="" enctype="multipart/form-data">
    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : $timeNow ?>" name="folder" accept-charset="utf-8">
                    <div class="panel-body">
					
                  <?php
    $i = 0;
    foreach ($languages as $language) {
        ?><div class="locale-container locale-container-<?= $language->abbr ?>" <?= $language->abbr == MY_DEFAULT_LANGUAGE_ABBR ? 'style="display:block;"' : '' ?>><input type="hidden" name="translations[]" value="<?= $language->abbr ?>">
                           <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="product_name" class="col-sm-4 col-form-label">Medicine Name <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" tabindex="1"  type="text" id="product_name" placeholder="Medicine Name" required="" name="title[]" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['title']) ? $trans_load[$language->abbr]['title'] : '' ?>" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                               <div class="form-group row">
                                    <label for="generic_name" class="col-sm-4 col-form-label">Generic name  <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="generic_name" type="text" id="generic_name" placeholder="Generic name" tabindex="2" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="box_size" class="col-sm-4 col-form-label">SKU <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="sku"
					<?php if(isset($single_item['sku'])){ echo $single_item['sku'];} ?> required="required">
                                    </div>
                                </div>
                            </div>
                           <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="unit" class="col-sm-4 col-form-label">Unit  <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2-hidden-accessible" id="unit" name="unit" required="required" tabindex="-1" aria-hidden="true">
                                             <option value="">Select One</option>
                                             <option value="m">Meter (M)</option>
                                             <option value="Box">Box</option>
                                             <option value="pc">Piece (Pc)</option>
                                             <option value="Mg">Milli Gram(mg)</option>
                                             <option value="ml">Milli liter(ml)</option>
                                             <option value="Gm">Gram</option>
                                             <option value="kg">Kilogram (Kg)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
 <div class="row"><div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="product_model" class="col-sm-4 col-form-label">Medicine Type <i class="text-danger">*</i></label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2-hidden-accessible" id="category_id" name="type_name" required="required" tabindex="-1" aria-hidden="true">
                                        <option value="">Select One</option>
                                                                                
                                            <option value="Tablet">Tablet</option>
                                        
                                            <option value="Liquid">Liquid</option>
                                        
                                                                                </select>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="category_id" class="col-sm-4 col-form-label">Medicine Category</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2-hidden-accessible" id="category_id" name="category_id" tabindex="-1" aria-hidden="true">
                                        <option value="">Select One</option>
                                                                                
                                            <option value="GI1746YIPR983T5">Antibiotics</option>
                                        
                                            <option value="2PJ9BIGXCU8WP4S">Paracetamol</option>
                                        
                                            <option value="9Q3JQELIXJ2I6O2">Histasinee</option>
                                        
                                            <option value="FAS73VNYZOFDXEW">Analgesics</option>
                                        
                                            <option value="ISJCEPPBMJWIPHB">Antiseptics</option>
                                        
                                            <option value="WYC1FHRJ5E1RUZA">Antipyretics</option>
                                        
                                            <option value="X6IQIQU7KEEO82E">Mood Stabilizers</option>
                                        
                                                                                </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                               <div class="form-group row">
                                    <label for="image" class="col-sm-4 col-form-label">Sell Price <i class="text-danger">*</i> </label>
                                    <div class="col-sm-8">
                                         <input class="form-control text-right" name="price" type="text" onkeyup="Checkprice()" required="required" id="price" placeholder="0.00" tabindex="10" min="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">


                             <div class="col-sm-6">
                               <div class="form-group row">
                                    <label for="tax" class="col-sm-4 col-form-label">Tax </label>
                                    <div class="col-sm-8">
                                       <select name="tax" class="form-control dont-select-me" tabindex="11">
                                                <option>Select One</option>
                                                                                        
                                                <option value="5">5%</option>
                                            
                                                <option value="11.5">11.5%</option>
                                            
                                                                                        </select>
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-6">

                               <div class="form-group row">
                                 <label for="" class="col-sm-4 col-form-label"></label>
                                   <div class="col-sm-8">
                                    <span id="prft" style="color:green;font-size: 15px"></span>
                                </div>
                                </div>
                            </div>
                        </div>

                     </div>
                    </form>                </div>
            </div>
        </div>
    </section>
<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
			  
			  
                  <i class="fa fa-user-circle-o"></i>
                   General Info
                </h4>
                 <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
             
                 <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Short Description</label>
                  <div class="col-sm-10">
                    <textarea name="basic_description[]" id="basic_description<?= $i ?>" rows="50" class="form-control"><?= $trans_load != null && isset($trans_load[$language->abbr]['basic_description']) ? $trans_load[$language->abbr]['basic_description'] : '' ?></textarea>
                    <script>
                        CKEDITOR.replace('basic_description<?= $i ?>');
                        CKEDITOR.config.entities = false;
                    </script>
                  </div>
                </div>
				
<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                    <textarea name="description[]" id="description<?= $i ?>" rows="50" class="form-control"><?= $trans_load != null && isset($trans_load[$language->abbr]['description']) ? $trans_load[$language->abbr]['description'] : '' ?></textarea>
                <script>
                    CKEDITOR.replace('description<?= $i ?>');
                    CKEDITOR.config.entities = false;
                </script>
                  </div>
                </div>

				<?php
        $i++;
    }
    ?>				
				
                
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Other Info
                </h4>
            <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Price</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="input-14" name="price[]" placeholder="price" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['price']) ? $trans_load[$language->abbr]['price'] : '' ?>" required="">
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label">Special Price</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="old_price[]" placeholder="Special Price" value="<?= $trans_load != null && isset($trans_load[$language->abbr]['old_price']) ? $trans_load[$language->abbr]['old_price'] : '' ?>" id="input-15" required="">
					
					
                  </div>
                </div>

                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Weight</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="input-14" name="Weight" value="" required="">
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label">Stock</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="stock">
                                    <option value="In Stock">In Stock</option>
                                    <option value="Out of Stock">Out of Stock</option>
                                  </select>
                  </div>
                </div>
 <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label"> Product  New from </label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" id="txtFrom" name="date_new_from" value="" required="">
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label"> Product  New to </label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" id="txtTo" name="date_new_to" value="" required="">
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-6" class="col-sm-2 col-form-label">Status</label>
                  <div class="col-sm-4">
                    <select class="form-control valid" id="input-6" name="staus" required="" aria-invalid="false">
                     
                        <option value="Enabled">Enabled</option>
						   <option value="Disabled">Disabled</option>
                    </select>
                  </div>
                </div>
			
				<div class="form-group row">
                  
                </div>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Cover Image</label>
				  <?php
        if (isset($_POST['image']) && $_POST['image'] != null) {
            $image = 'attachments/shop_images/' . $_POST['image'];
            if (!file_exists($image)) {
                $image = 'attachments/no-image.png';
            }
            ?>
                  <div class="col-sm-10">
                    <img src="<?= base_url($image) ?>" class="img-responsive img-thumbnail" style="max-width:300px; margin-bottom: 5px;">
                  </div>
                </div>
		<input type="hidden" name="old_image" value="<?= $_POST['image'] ?>">
            <?php if (isset($_GET['to_lang'])) { ?>
                <input type="hidden" name="image" value="<?= $_POST['image'] ?>">
			<?php }
        }
        ?>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label"</label>
				  <input type="file" id="userfile" name="userfile">
				  </div>
                  <div class="col-sm-10">
                    <div class="form-group bordered-group">
        <div class="others-images-container">
            <?= $otherImgs ?>
        </div>
        <a href="javascript:void(0);" data-toggle="modal" data-target="#modalMoreImages" class="btn btn-default">Upload more images</a>
    </div>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Shop Categories</label>
                  <div class="col-sm-10">
                    <select class="selectpicker form-control show-tick show-menu-arrow" name="shop_categorie">
            <?php foreach ($shop_categories as $key_cat => $shop_categorie) { ?>
                <option <?= isset($_POST['shop_categorie']) && $_POST['shop_categorie'] == $key_cat ? 'selected=""' : '' ?> value="<?= $key_cat ?>">
                    <?php
                    foreach ($shop_categorie['info'] as $nameAbbr) {
                        if ($nameAbbr['abbr'] == $this->config->item('language_abbr')) {
                            echo $nameAbbr['name'];
                        }
                    }
                    ?>
                </option>
            <?php } ?>
        </select>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Quantity</label>
                  <div class="col-sm-10">
                    <input type="text" placeholder="number" name="quantity" value="<?= @$_POST['quantity'] ?>" class="form-control" id="quantity">
                  </div>
                </div>
				 <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Brand</label>
                  <div class="col-sm-10">
                   <select class="selectpicker form-control" name="brand_id">
			
                <?php foreach ($brands as $brand) { ?>
                    <option <?= isset($_POST['brand_id']) && $_POST['brand_id'] == $brand['id'] ? 'selected' : '' ?> value="<?= $brand['id'] ?>"><?= $brand['name'] ?></option>
                <?php } ?>
            </select>
                  </div>
                </div>
				
                <div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Feature product</label>
                  <div class="col-sm-10">
                    <select  class="form-control" name="is_feature">
		
		
		<option <?php if(isset($single_item)){
		if($single_item['is_feature']=="0"){ echo "selected";}
		}
		?> value="0">No</option>
		<option <?php if(isset($single_item)){ if($single_item['is_feature']=="1"){ echo "selected";} }?> value="1">Yes</option>
		
		</select>
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-17" class="col-sm-2 col-form-label">Position</label>
                  <div class="col-sm-10">
                       <input type="text" placeholder="Position number" name="position" value="<?= @$_POST['position'] ?>" class="form-control">
                  </div>
                </div>
                <div class="form-footer">
                    <a href="<?= base_url('admin/products') ?>"class="btn btn-danger" name="submit"><i class="fa fa-times"></i> CANCEL</a>
                    <button type="submit" class="btn btn-success" name="save"><i class="fa fa-check-square-o"></i> Publish</button>
                </div>
              
            </div>
          </div>
        </div>
      </div></form><!--End Row-->

               
			 
            </div>
         

		 </div>
       
	   </div>
      </div>
<div class="modal fade" id="modalMoreImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload more images</h4>
            </div>
            <div class="modal-body">
                <form id="uploadImagesForm">
                    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : $timeNow ?>" name="folder">
                    <label for="others">Select images</label>
                    <input type="file" name="others[]" id="others" multiple />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default finish-upload">
                    <span class="finish-text">Finish</span>
                    <img src="<?= base_url('assets/imgs/load.gif') ?>" class="loadUploadOthers" alt="">
                </button>
            </div>
        </div>
    </div>
</div>
<!-- virtualProductsHelp -->
<div class="modal fade" id="virtualProductsHelp" tabindex="-1" role="dialog" aria-labelledby="virtualProductsHelp">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">What are virtual products?</h4>
            </div>
            <div class="modal-body">
                Sometimes we want to sell products that are for electronic use such as books. In the box below, you can enter links to products that can be downloaded after you confirm the order as "Processed" through the "Orders" tab, an email will be sent to the customer entered with the entire text entered in the "virtual products" field.
                We have left only the possibility to add links in this field because sometimes it is necessary that the electronic stuff you provide for downloading will be uploaded to other servers. If you want, you can add your files to "file manager" and take the links to them to add to the "virtual products"
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
            type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/start/jquery-ui.css"
          rel="Stylesheet" type="text/css" />
<script type="text/javascript">
        $(function () {
            $("#txtFrom").datepicker({
                numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $("#txtTo").datepicker("option", "minDate", dt);
                }
            });
            $("#txtTo").datepicker({
                numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() - 1);
                    $("#txtFrom").datepicker("option", "maxDate", dt);
                }
            });
        });
    </script>