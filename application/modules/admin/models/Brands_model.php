<?php

class Brands_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getBrands()
    {
        $result = $this->db->get('brands');
        return $result->result_array();
    }

    public function setBrand($name)
    {
        if (!$this->db->insert('brands', array('name' => $name))) {
            log_message('error', print_r($this->db->error(), true));
            show_error(lang('database_error'));
        }
    }

    public function deleteBrand($id)
    {
        if (!$this->db->where('id', $id)->delete('brands')) {
            log_message('error', print_r($this->db->error(), true));
            show_error(lang('database_error'));
        }
    }
   function get_product_by_brand($limit = null, $start = null, $big_get){ 
      if($start=="1"){$start="0";}//echo $start; die();
	   if ($limit !== null && $start !== null) {
            $this->db->limit(12, $start);
        }
		if(!empty($big_get["brands_id"])){
		$this->db->where('products.brand_id',$big_get["brands_id"]);
		}else{
		$this->db->limit(12, $start);	
		}
		$this-> db->select('*');
		 $this -> db -> from('products');
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        
		if (isset($big_get['product_order'])) { 
			if($big_get['product_order']=="newest"){
			$this->db->order_by("products.id", "DESC");}else{
				$this->db->order_by("products.id", "ASC");
				
			}
        }
		if (isset($big_get['price_range'])) { 
			if($big_get['price_range']=="low"){
			$this->db->order_by("products_translations.price", "DESC");}else{
				$this->db->order_by("products_translations.price", "ASC");
				
			}
        }if (isset($big_get['order_quantity'])) { 
			if($big_get['order_quantity']=="low"){
			$this->db->order_by("products.quantity", "DESC");}else{
				$this->db->order_by("products.quantity", "ASC");
				
			}
        } 	
		$query = $this ->db->get();
		$q= $this->db->last_query();
			//print_r($q);	
		return $query->result_array();
		
	}
	function get_product_by_disease($disease){
		$this->db->where('products_translations.disease',$disease);
		$this-> db->select('*');
		 $this -> db -> from('products');
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        		
		$query = $this ->db->get();
$q=$this->db->last_query();
//print_r($q);		
		return $query->result_array();
		
	}
	
	function get_product_com_brand($brand_id){ 
	$this->db->where('products.brand_id',$brand_id);
	$this-> db->select('*');
	$query = $this ->db->get();	
			
		return $query->result_array();
	}
	function get_brand_name($brand_id){
		$this->db->where('id',$brand_id);
		$this-> db->select('*');
        $this -> db -> from('brands'); 		
		$query = $this ->db->get();			
		return $query->row_array();
		
	}function get_disease(){
		
		$this-> db->select('*');
        $this -> db -> from('disease'); 		
		$query = $this ->db->get();			
		return $query->result_array();
		
	} 
	function get_disease_name($brand_id){
		$this->db->where('id',$brand_id);
		$this-> db->select('*');
        $this -> db -> from('disease'); 		
		$query = $this ->db->get();			
		return $query->row_array();
		
	}
}
