<?php

class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function deleteProduct($id)
    {
        $this->db->trans_begin();
        $this->db->where('for_id', $id);
        if (!$this->db->delete('products_translations')) {
            log_message('error', print_r($this->db->error(), true));
        }

        $this->db->where('id', $id);
        if (!$this->db->delete('products')) {
            log_message('error', print_r($this->db->error(), true));
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            show_error(lang('database_error'));
        } else {
            $this->db->trans_commit();
        }
    }

    public function productsCount($search_title = null, $category = null)
    {
        if ($search_title != null) {
            $search_title = trim($this->db->escape_like_str($search_title));
            $this->db->where("(products_translations.title LIKE '%$search_title%')");
        }
        if ($category != null) {
            $this->db->where('shop_categorie', $category);
        }
        $this->db->join('products_translations', 'products_translations.for_id = products.id', 'left');
        $this->db->where('products_translations.abbr', MY_DEFAULT_LANGUAGE_ABBR);
        return $this->db->count_all_results('products');
    }

    public function getProducts($limit, $page, $search_title = null, $orderby = null, $category = null, $vendor = null)
    {
        if ($search_title != null) {
            $search_title = trim($this->db->escape_like_str($search_title));
            $this->db->where("(products_translations.title LIKE '%$search_title%')");
        }
        if ($orderby !== null) {
            $ord = explode('=', $orderby);
            if (isset($ord[0]) && isset($ord[1])) {
                $this->db->order_by('products.' . $ord[0], $ord[1]);
            }
        } else {
            $this->db->order_by('products.position', 'asc');
        }
        if ($category != null) {
            $this->db->where('shop_categorie', $category);
        }
        if ($vendor != null) {
            $this->db->where('vendor_id', $vendor);
        }
        $this->db->join('vendors', 'vendors.id = products.vendor_id', 'left');
        $this->db->join('products_translations', 'products_translations.for_id = products.id', 'left');
        $this->db->where('products_translations.abbr', MY_DEFAULT_LANGUAGE_ABBR);
        $query = $this->db->select('vendors.name as vendor_name, vendors.id as vendor_id, products.*, products_translations.title, products_translations.description, products_translations.price, products_translations.old_price, products_translations.abbr, products.url, products_translations.for_id, products_translations.basic_description')->get('products', $limit, $page);
        return $query->result();
    }
public function listing_pro($categorie = 20)
    {
         $this->db->where('products.brand_id',$categorie);
		$this-> db->select('*');
		
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        $this -> db -> from('products');
	    $this->db->limit(8);
        $this->db->order_by('products.id','RANDOM');	
		$query = $this ->db->get();
        $q =$this->db->last_query();		
		return $query->result_array();
    }
    public function numShopProducts()
    {
        return $this->db->count_all_results('products');
    }

    public function getOneProduct($id)
    {
        $this->db->select('vendors.name as vendor_name, vendors.id as vendor_id, products.*, products_translations.price');
        $this->db->where('products.id', $id);
        $this->db->join('vendors', 'vendors.id = products.vendor_id', 'left');
        $this->db->join('products_translations', 'products_translations.for_id = products.id', 'inner');
        $this->db->where('products_translations.abbr', MY_DEFAULT_LANGUAGE_ABBR);
        $query = $this->db->get('products');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function productStatusChange($id, $to_status)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('products', array('visibility' => $to_status));
        return $result;
    }

    public function setProduct($post, $id = 0)
    {       
        if (!isset($post['virtual_products'])) {
            $post['virtual_products'] = null;
        } $shop_categorie="";
			if(isset($post['shop_categorie']))
			{
				$shop_categorie =$post['shop_categorie'];
			}
			$brand_id ='';
			if(isset($post['brand_id']))
			{
				$brand_id =$post['brand_id'];
			}
			//print_r($post['shop_categorie']); die();
        $this->db->trans_begin();
        $is_update = false;
        if ($id > 0) {
            $is_update = true;
            if (!$this->db->where('id', $id)->update('products', array(
                        'image' => $post['image'] != null ? $_POST['image'] : $_POST['old_image'],
                        'shop_categorie' => $shop_categorie,
                        'quantity' => $post['quantity'],         
                        'position' => $post['position'],'is_feature' => $post['is_feature'],
                        'virtual_products' => $post['virtual_products'],
                        'brand_id' => $brand_id,
                        'time_update' => time()
                    ))) {
                log_message('error', print_r($this->db->error(), true));
            }
        } else {
            /*
             * Lets get what is default tranlsation number
             * in titles and convert it to url
             * We want our plaform public ulrs to be in default 
             * language that we use
             */
            $i = 0;
            foreach ($_POST['translations'] as $translation) {
                if ($translation == MY_DEFAULT_LANGUAGE_ABBR) {
                    $myTranslationNum = $i;
                }
                $i++;
            }
			
			
			
			$shop_categorie="";
			if(isset($post['shop_categorie']))
			{
				$shop_categorie =$post['shop_categorie'];
			}$brand_id="";
			if(isset($post['brand_id']))
			{
				$brand_id =$post['brand_id'];
			}
			
            if (!$this->db->insert('products', array(
                        'image' => $post['image'],
                        'shop_categorie' => $shop_categorie,
                        'quantity' => $post['quantity'],
                        'position' => $post['position'],
                        'folder' => $post['folder'],
                        'is_feature' => $post['is_feature'],
                        'new_from_date' => $post['date_new_from'],
                        'new_to_date' => $post['date_new_to'],
                        'brand_id' =>$brand_id,
                        'status' => $post['status'],
                        'time' => time()
                    ))) {
                log_message('error', print_r($this->db->error(), true));
            }
            $id = $this->db->insert_id();

            $this->db->where('id', $id);
            if (!$this->db->update('products', array(
                        'url' => except_letters($_POST['title'][$myTranslationNum]) . '_' . $id
                    ))) {
                log_message('error', print_r($this->db->error(), true));
            }
        }
        $this->setProductTranslation($post, $id, $is_update);
		
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            show_error(lang('database_error'));
        } else {
            $this->db->trans_commit();
        }
    }

    private function setProductTranslation($post, $id, $is_update)
    {
        $i = 0;
        $current_trans = $this->getTranslations($id);
        foreach ($post['translations'] as $abbr) {
            $arr = array();
            $emergency_insert = false;
            if (!isset($current_trans[$abbr])) {
                $emergency_insert = true;
            }
            $post['title'][$i] = str_replace('"', "'", $post['title'][$i]);
			$post['is_feature'][$i] = str_replace('"', "'", $post['is_feature'][$i]);
            $post['price'][$i] = str_replace(' ', '', $post['price'][$i]);
            $post['price'][$i] = str_replace(',', '.', $post['price'][$i]);
            $post['price'][$i] = preg_replace("/[^0-9,.]/", "", $post['price'][$i]);
            $post['old_price'][$i] = str_replace(' ', '', $post['old_price'][$i]);
            $post['old_price'][$i] = str_replace(',', '.', $post['old_price'][$i]);
            $post['old_price'][$i] = preg_replace("/[^0-9,.]/", "", $post['old_price'][$i]);
			//print_r($post); die();
			$genric_name='';
			$medicine_category='';
			$weight ='';
			$disease_name ='';
			$unit ='';
			$stock_status ='';
			if(isset($post['generic_name']))
			{ $genric_name =$post['generic_name'];	
			}
			if(isset($post['Weight']))
			{ $weight =$post['Weight'];	
			}
			if(isset($post['disease_name']))
			{ $disease_name =$post['disease_name'];	
			}
			if(isset($post['unit']))
			{ $unit =$post['unit'];	
			}if(isset($post['medicine_category']))
			{ $medicine_category =$post['medicine_category'];	
			}
            $arr = array(
                'title' => $post['title'][$i],
                'basic_description' => $post['basic_description'][$i],
                'description' => $post['description'][$i],
                'price' => $post['price'][$i],
                'old_price' => $post['old_price'][$i],
                'abbr' => $abbr,
                'for_id' => $id,
                        'medicine_type' => $post['medicine_type'][$i],
						'genric_name' => $genric_name,
						'unit' => $unit,
						'weight' => $weight,
						'disease' => $disease_name,
						'medicinec_category' => $medicine_category,
						'stock_status' => $post['stock'][$i]
            );
			//echo "<pre>"; print_r($arr); die();
            if ($is_update === true && $emergency_insert === false) {
                $abbr = $arr['abbr'];
                unset($arr['for_id'], $arr['abbr'], $arr['url']);
                if (!$this->db->where('abbr', $abbr)->where('for_id', $id)->update('products_translations', $arr)) {
                    log_message('error', print_r($this->db->error(), true));
                }
            } else {
                if (!$this->db->insert('products_translations', $arr)) {
                    log_message('error', print_r($this->db->error(), true));
                }
            }
            $i++;
        }
    }

    public function getTranslations($id)
    {
        $this->db->where('for_id', $id);
        $query = $this->db->get('products_translations');
        $arr = array();
        foreach ($query->result() as $row) {
            $arr[$row->abbr]['title'] = $row->title;
			
            $arr[$row->abbr]['basic_description'] = $row->basic_description;
            $arr[$row->abbr]['description'] = $row->description;
            $arr[$row->abbr]['price'] = $row->price;
            $arr[$row->abbr]['old_price'] = $row->old_price;
        }
        return $arr;
    }
   function get_single_product($id){
		$this->db->where('products.id',$id);
		$this-> db->select('*');
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        $this -> db -> from('products'); 		
		$query = $this ->db->get();			
		return $query->row_array();
		
	}
	function get_disease(){
		$this-> db->select('*');
        $this -> db -> from('disease'); 		
		$query = $this ->db->get();			
		return $query->result_array();
		
	}function get_single_disease(){
		$this->db->where('id',$id);
		$this-> db->select('*');
        $this -> db -> from('disease'); 		
		$query = $this ->db->get();			
		return $query->row_array();
		
	} 
	function get_feature_products(){ 
	$this->db->where('products.is_feature',"1");
		$this-> db->select('*');
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        $this -> db -> from('products');
		//$this->db->limit(15);
        $this->db->order_by("products.id", "asc"); 		
		$query = $this ->db->get();			
		return $query->result_array();
	}
	function get_products_listing(){
		$this-> db->select('*');
		$this->db->join('products_translations', 'products.id=products_translations.id', 'left');
        $this -> db -> from('products');
		$this->db->limit(0,15);
        $this->db->order_by("products.id", "asc"); 		
		$query = $this ->db->get();			
		return $query->result_array();
	}
	
	 function get_all_unit(){
		
		$this-> db->select('*');
        $this -> db -> from('tbl_unit'); 		
		$query = $this ->db->get();			
		return $query->result_array();
		
	}
	 function insert_import($param)
	{
      $this->db->insert('products', $param);
      $insert_id = $this->db->insert_id();

      return  $insert_id;
      
		
	}
	function insert_import_detail($param)
	{
    return  $this->db->insert('products_translations', $param);
      
		
	} function get_single_brand_byname($id){
		$this->db->where('full_name',$id);
		
        $this -> db -> from('brands'); 		
		$query = $this ->db->get();			
		return $query->row_array();
		
	}
}
