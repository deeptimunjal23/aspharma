<?php

/*
 * @Author:    Atish Kumar
 *  Gitgub:    atish.net@gmail.com
 */
 require_once(APPPATH.'/libraries/Classes/PHPExcel.php');
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Prescription extends ADMIN_Controller
{

    private $num_rows = 10;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Products_model', 'Languages_model', 'Categories_model', 'Public_model'));
    }

    public function index($page = 0)
    {
        $this->login_check();
        $data = array();
        $head = array();
        $head['title'] = 'Administration - View Prescription Orders';
        $head['description'] = '!';
        $head['keywords'] = '';

        if (isset($_GET['delete'])) {
            $this->Products_model->deleteprescription($_GET['delete']);
            $this->session->set_flashdata('result_delete', 'product is deleted!');
            $this->saveHistory('Delete prescription id - ' . $_GET['delete']);
            redirect('admin/prescription');
        }

        unset($_SESSION['filter']);
        $search_title = null;
        if ($this->input->get('search_title') !== NULL) {
            $search_title = $this->input->get('search_title');
            $_SESSION['filter']['search_title'] = $search_title;
            $this->saveHistory('Search for product title - ' . $search_title);
        }
        $orderby = null;
        if ($this->input->get('order_by') !== NULL) {
            $orderby = $this->input->get('order_by');
            $_SESSION['filter']['order_by '] = $orderby;
        }
        $category = null;
        if ($this->input->get('category') !== NULL) {
            $category = $this->input->get('category');
            $_SESSION['filter']['category '] = $category;
            $this->saveHistory('Search for product code - ' . $category);
        }
        $vendor = null;
        if ($this->input->get('show_vendor') !== NULL) {
            $vendor = $this->input->get('show_vendor');
        }
		
		
        $data['products_lang'] = $products_lang = $this->session->userdata('admin_lang_products');
        $rowscount = $this->Products_model->productsCount($search_title, $category);
        $data['products'] = $this->Public_model->get_all_prescription();
        $data['links_pagination'] = pagination('admin/products', $rowscount, $this->num_rows, 3);
        $this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/user_prescription', $data);
        $this->load->view('_parts/footer');
    }

    public function getProductInfo($id, $noLoginCheck = false)
    {
        /* 
         * if method is called from public(template) page
         */
        if ($noLoginCheck == false) {
            $this->login_check();
        }
        return $this->Products_model->getOneProduct($id);
    }

    /*
     * called from ajax
     */

    
	public function download()
    {
        $this->login_check();
        $this->DownloadAnything();
    }
    public function prescription_orders_detail()
    {  
	
	$data = array();
        $head = array();
		$id =$this->uri->segment(3);
        $this->login_check();
        $data = array();
        $head = array();
        $head['title'] = 'Administration - View Prescription Orders';
        $head['description'] = '!';
        $head['keywords'] = '';
		$data["single_item"]=$this->Public_model->get_single_prescription($id);
		if (isset($_POST['submit'])) {
     $p_id = $this->input->post('p_id');
	  //$payment_status = $this->input->post('payment_status');
	  $amount =$data["single_item"]["amount"];
	  if(isset($_POST['amount'])){
	  $amount = $this->input->post('amount');
	  }
	  $order_status = $this->input->post('order_status');
	  $admin_extra_note = $this->input->post('admin_extra_note');
            $data=array(
					'order_status' => $order_status,'amount' => $amount,
					'admin_extra_note' => $admin_extra_note,
					
				);
			  $this->db->where('prescription_id', $p_id);
				$this->db->update('users_prescription', $data);
				$this->session->set_flashdata('message', 'Your Prescription Order updated Successfully..');
			redirect(base_url().'admin/prescription-order-detail/'.$id);
			
			}
		
		
	    $this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/pre_detail', $data);
        $this->load->view('_parts/footer');
	}
public function sliders()
	{       $data = array();
        $head = array();
        $head['title'] = 'Administration - View Prescription Orders';
        $head['description'] = '!';
        $head['keywords'] = '';
           $data['get_sliders']=$this->Public_model->get_all_sliders();
		  $this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/index-v2', $data);
        $this->load->view('_parts/footer');
			//$this->load->view('slider/index-v2',$data);
	}
	public function add_slider()
	{       $data = array();
        $head = array();
        $head['title'] = 'Administration - View Prescription Orders';
        $head['description'] = '!';
        $head['keywords'] = '';
		$data["slider"]="";
		$this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/add_slider-v2', $data);
        $this->load->view('_parts/footer');
	}	

	public function save_slider()
	{  
			if (isset($_POST['submit'])) {	
			//$pic=$this->input->post('pic');
			$status="active";
			$tittle=$this->input->post('tittle');
			$sub_tittle=$this->input->post('sub_tittle');
			$description=$this->input->post('description');
				
			$link=$this->input->post('link');			
			$slider=$this->input->post('slider_id');
            $ff ='';
			  $img='';
			if(!empty($_FILES['pic']['name'])) {				
					$imgDir = "uploads/slider/";
				
					$allowed = array('image/jpg','image/jpeg','image/png','image/gif');
				            $ext = pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION);
				 
				 if(!in_array($_FILES['pic']['type'],$allowed)) 
						  { 
							 $msg='Sorry, only JPG ,PNG files Are allowed.';
								$this->session->set_flashdata('message', $msg);
	                              redirect('index.php/cpanel/add_slider','refresh');
					           									
					 
						 }else{
				
				$file_name = $_FILES["pic"]["name"];			
			     $extension = end((explode(".", $file_name)));			
			    $upload_file = $imgDir.$file_name;	
				if(move_uploaded_file($_FILES['pic']['tmp_name'],$upload_file)){			  
				$source_image = $imgDir.$file_name;
				$img = "compress-".$file_name;
			    $image_destination = $imgDir.$img;
				 $compress_images = $this->compressImage($source_image, $image_destination);			 
			}
						 }
		 } else{

				
				if($this->input->post('old_pic')!='')
				{
					$img=$this->input->post('old_pic');
					
					
				}
				else{
					$img='';
				}
			}
			$data=array(
					'tittle' => $tittle,
					'sub_tittle' => $sub_tittle, 
					'link' => $link, 
					'description'=>$description,
					'pic'=> $img,					
					'status' => $status, 
				);

			if($slider!='')
			{
				$this->db->where('id', $slider);
				$this->db->update('tbl_slider', $data);
				$this->session->set_flashdata('message', 'Your data updated Successfully..');
				redirect('admin/sliders');
			}
		
				 
				else {
					$this->Public_model->insert_slider($data);	
					$this->session->set_flashdata('message', 'Your data Save Successfully..');
					redirect(base_url().'admin/add-slider');
				}
			
			}
	}

	public function edit_slider(){
		$data = array();
        $head = array();
        $head['title'] = 'Administration - View Prescription Orders';
        $head['description'] = '!';
        $head['keywords'] = '';
		 $url=$this->uri->segment(3);		
		$data['slider']=$this->Public_model->get_single_slider($url);	
	      $this->load->view('_parts/header', $head);
			$this->load->view('ecommerce/add_slider-v2', $data);
			$this->load->view('_parts/footer');
	
	}
	public function shipping(){
		$data = array();
        $head = array();
        $head['title'] = 'Administration - shipping';
        $head['description'] = '!';
        $head['keywords'] = '';
		 		
		$data['shipping']=$this->Public_model->get_single_shipping(1);
        if (isset($_POST['save'])) {	
			$title=$this->input->post('title');
			$method_name=$this->input->post('method_name');
			$handling_fee_type=$this->input->post('handling_fee_type');
			$handling_fee=$this->input->post('handling_fee');
			$status="active";
			$shipping_id=$this->input->post('shipping_id');$free_shipping=$this->input->post('free_shipping');
			$data=array(
					'title' => $title,
					'method_name' => $method_name, 
					'handling_fee' => $handling_fee, 
					'handling_fee_type'=>$handling_fee_type,
					'status' => $status,'free_shipping' => $free_shipping 
				);
				if($shipping_id!='')
			{
				$this->db->where('shipping_id', $shipping_id);
				$this->db->update('shipping', $data);
				$this->session->set_flashdata('message', 'Your data updated Successfully..');
				redirect('admin/shipping-method');
			} 
			}

		
	      $this->load->view('_parts/header', $head);
			$this->load->view('ecommerce/shipping', $data);
			$this->load->view('_parts/footer');
	
	}
	////
	public function import()
	  { $data = array();
        $head = array();
        $head['title'] = 'Administration - Product-import';
        $head['description'] = '!';
        $head['keywords'] = '';
		   if (isset($_POST['save'])) {
	
		$allowed =  array('xlsx','Csv',"xls");   
						$ext = pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION);
				//print_r($ext);	die();
			             if(!in_array($ext,$allowed) ) {
							$msg='Sorry, only xlsx  files are allowed.';
							$this->session->set_flashdata('message', $msg);
						 }
						else{
						$filename= $_FILES["csv"]["tmp_name"];
						$distpath=$filename;
						$inputFileName = $distpath;
							try {
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
							
					}catch(Exception $e) {
						die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//echo "<pre>";	print_r($allDataInSheet); die();
	 unset($allDataInSheet['1']);
foreach($allDataInSheet as $key=>$rec){
	               	$sn = trim($rec["A"]);
					$title = trim($rec["B"]);
					$genric = trim($rec["E"]);
					$genric_name = trim($rec["D"]);
						
					$status = 'Enabled';
				$brand=	$this->Products_model->get_single_brand_byname($genric);
				$brand_id = $brand["id"];
			
					$params = array(
                                'visibility' => "1" ,
								'shop_categorie' => "3" ,'is_feature' => "0" ,
								'brand_id' => $brand_id ,'quantity' => "1000",
								'status' => $status
							);
			
					 $savetodb=  $this->Products_model->insert_import($params);
					 $url=	except_letters($title) . '_' . $savetodb;
					 $data_url=array("url"=>$url);
					 $this->db->where('id', $savetodb);
				$this->db->update('products', $data_url);
					 $params_detail = array(
                                'for_id' => $savetodb ,
                                'stock_status' => "1" ,
								'genric_name' => $genric_name ,
								'abbr' => "en",               	
								'title' => $title,
								'price' => "300",
								'old_price' => "350"
							);
					 $this->Products_model->insert_import_detail($params_detail);		
				   $this->session->set_flashdata('message', 'Prodcuts Data Imported Successfully.');
					
					
					
}	redirect('admin/import','refresh');
}
 	}
        $this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/import', $data);
        $this->load->view('_parts/footer');
	  }
	public function medicine_unit()
	{       $data = array();
        $head = array();
        $head['title'] = 'Administration - medicine-unit';
        $head['description'] = '!';
        $head['keywords'] = '';
           $data['get_all_unit']=$this->Public_model->get_all_unit2();
		  $this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/unit', $data);
        $this->load->view('_parts/footer');
			//$this->load->view('slider/index-v2',$data);
	}
	public function add_unit()
	{       $data = array();
        $head = array();
        $head['title'] = 'Administration - add unit';
        $head['description'] = '!';
        $head['keywords'] = '';
		$data["slider"]="";
		$this->load->view('_parts/header', $head);
        $this->load->view('ecommerce/add_unit', $data);
        $this->load->view('_parts/footer');
	}	

	public function save_unit()
	{  
			if (isset($_POST['submit'])) {	
			
			$name=$this->input->post('name');
			$unit=$this->input->post('unit_id');
			
			$data=array(
					'name' => $name 
				);

			if($unit!='')
			{
				$this->db->where('unit_id', $unit);
				$this->db->update('tbl_unit', $data);
				$this->session->set_flashdata('message', 'Your data updated Successfully..');
				redirect('admin/medicine-unit');
			}
		
				 
				else {
					$this->Public_model->insert_unit($data);	
					$this->session->set_flashdata('message', 'Your data Save Successfully..');
					redirect(base_url().'admin/medicine-unit');
				}
			
			}
	}

	public function edit_unit(){
		$data = array();
        $head = array();
        $head['title'] = 'Administration - add_unit';
        $head['description'] = '!';
        $head['keywords'] = '';
		 $url=$this->uri->segment(3);		
		$data['single_unit']=$this->Public_model->get_single_unit($url);
		//print_r($data['single_unit']);
	      $this->load->view('_parts/header', $head);
			$this->load->view('ecommerce/add_unit', $data);
			$this->load->view('_parts/footer');
			
	}
	function RemoveSpaces($url){
     
	  $url = preg_replace('/\s+/', '-', trim($url));
	  $url = str_replace("         ","-",$url);
	  $url = str_replace("        ","-",$url);
	  $url = str_replace("       ","-",$url);
	  $url = str_replace("      ","-",$url);
	  $url = str_replace("     ","-",$url);
	  $url = str_replace("    ","-",$url);
	  $url = str_replace("   ","-",$url);
	  $url = str_replace("  ","-",$url);
	  $url = str_replace(" ","-",$url);
	
     return $url;
     
}

function RemoveUrlSpaces($url){

        $url = preg_replace('/\s+/', '%20', trim($url));  
        $url = str_replace("         ","%20",$url);
        $url = str_replace("        ","%20",$url);
        $url = str_replace("       ","%20",$url);
        $url = str_replace("      ","%20",$url);
        $url = str_replace("     ","%20",$url);
        $url = str_replace("    ","%20",$url);
	    $url = str_replace("   ","%20",$url);
	    $url = str_replace("  ","%20",$url);
	    $url = str_replace(" ","%20",$url);
		
        return $url;
     
}
function DownloadAnything($file, $newfilename = '', $mimetype='', $isremotefile = false){
         
        $formattedhpath = "";
        $filesize = "";

        if(empty($file)){
           die('Please enter file url to download...!');
           exit;
        }
     
         //Removing spaces and replacing with %20 ascii code
         $file = RemoveUrlSpaces($file);
          
          
        if(preg_match("#http://#", $file)){
          $formattedhpath = "url";
        }else{
          $formattedhpath = "filepath";
        }
        
        if($formattedhpath == "url"){

          $file_headers = @get_headers($file);
  
          if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
           die('File is not readable or not found...!');
           exit;
          }
          
        }elseif($formattedhpath == "filepath"){

          if(@is_readable($file)) {
               die('File is not readable or not found...!');
               exit;
          }
        }
        
        
       //Fetching File Size Located in Remote Server
       if($isremotefile && $formattedhpath == "url"){
          
          
          $data = @get_headers($file, true);
          
          if(!empty($data['Content-Length'])){
          $filesize = (int)$data["Content-Length"];
          
          }else{
               
               ///If get_headers fails then try to fetch filesize with curl
               $ch = @curl_init();

               if(!@curl_setopt($ch, CURLOPT_URL, $file)) {
                 @curl_close($ch);
                 @exit;
               }
               
               @curl_setopt($ch, CURLOPT_NOBODY, true);
               @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               @curl_setopt($ch, CURLOPT_HEADER, true);
               @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
               @curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
               @curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
               @curl_exec($ch);
               
               if(!@curl_errno($ch))
               {
                    
                    $http_status = (int)@curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    if($http_status >= 200  && $http_status <= 300)
                    $filesize = (int)@curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
               
               }
               @curl_close($ch);
               
          }
          
       }elseif($isremotefile && $formattedhpath == "filepath"){
         
	   die('Error : Need complete URL of remote file...!');
           exit;
		   
       }else{
         
		   if($formattedhpath == "url"){
		   
			   $data = @get_headers($file, true);
			   $filesize = (int)$data["Content-Length"];
			   
		   }elseif($formattedhpath == "filepath"){
		   
		       $filesize = (int)@filesize($file);
			   
		   }
		   
       }
       
       if(empty($newfilename)){
          $newfilename =  @basename($file);
       }else{
          //Replacing any spaces with (-) hypen
          $newfilename = RemoveSpaces($newfilename);
       }
       
       if(empty($mimetype)){
          
       ///Get the extension of the file
       $path_parts = @pathinfo($file);
       $myfileextension = $path_parts["extension"];

        switch($myfileextension)
        {
          
            ///Image Mime Types
            case 'jpg':
            $mimetype = "image/jpg";
            break;
            case 'jpeg':
            $mimetype = "image/jpeg";
            break;
            case 'gif':
            $mimetype = "image/gif";
            break;
            case 'png':
            $mimetype = "image/png";
            break;
            case 'bm':
            $mimetype = "image/bmp";
            break;
            case 'bmp':
            $mimetype = "image/bmp";
            break;
            case 'art':
            $mimetype = "image/x-jg";
            break;
            case 'dwg':
            $mimetype = "image/x-dwg";
            break;
            case 'dxf':
            $mimetype = "image/x-dwg";
            break;
            case 'flo':
            $mimetype = "image/florian";
            break;
            case 'fpx':
            $mimetype = "image/vnd.fpx";
            break;
            case 'g3':
            $mimetype = "image/g3fax";
            break;
            case 'ief':
            $mimetype = "image/ief";
            break;
            case 'jfif':
            $mimetype = "image/pjpeg";
            break;
            case 'jfif-tbnl':
            $mimetype = "image/jpeg";
            break;
            case 'jpe':
            $mimetype = "image/pjpeg";
            break;
            case 'jps':
            $mimetype = "image/x-jps";
            break;
            case 'jut':
            $mimetype = "image/jutvision";
            break;
            case 'mcf':
            $mimetype = "image/vasa";
            break;
            case 'nap':
            $mimetype = "image/naplps";
            break;
            case 'naplps':
            $mimetype = "image/naplps";
            break;
            case 'nif':
            $mimetype = "image/x-niff";
            break;
            case 'niff':
            $mimetype = "image/x-niff";
            break;
            case 'cod':
            $mimetype = "image/cis-cod";
            break;
            case 'ief':
            $mimetype = "image/ief";
            break;
            case 'svg':
            $mimetype = "image/svg+xml";
            break;
            case 'tif':
            $mimetype = "image/tiff";
            break;
            case 'tiff':
            $mimetype = "image/tiff";
            break;
            case 'ras':
            $mimetype = "image/x-cmu-raster";
            break;
            case 'cmx':
            $mimetype = "image/x-cmx";
            break;
            case 'ico':
            $mimetype = "image/x-icon";
            break;
            case 'pnm':
            $mimetype = "image/x-portable-anymap";
            break;
            case 'pbm':
            $mimetype = "image/x-portable-bitmap";
            break;
            case 'pgm':
            $mimetype = "image/x-portable-graymap";
            break;
            case 'ppm':
            $mimetype = "image/x-portable-pixmap";
            break;
            case 'rgb':
            $mimetype = "image/x-rgb";
            break;
            case 'xbm':
            $mimetype = "image/x-xbitmap";
            break;
            case 'xpm':
            $mimetype = "image/x-xpixmap";
            break;
            case 'xwd':
            $mimetype = "image/x-xwindowdump";
            break;
            case 'rgb':
            $mimetype = "image/x-rgb";
            break;
            case 'xbm':
            $mimetype = "image/x-xbitmap";
            break;
            case "wbmp":
            $mimetype = "image/vnd.wap.wbmp";
            break;
          
            //Files MIME Types
            
            case 'css':
            $mimetype = "text/css";
            break;
            case 'htm':
            $mimetype = "text/html";
            break;
            case 'html':
            $mimetype = "text/html";
            break;
            case 'stm':
            $mimetype = "text/html";
            break;
            case 'c':
            $mimetype = "text/plain";
            break;
            case 'h':
            $mimetype = "text/plain";
            break;
            case 'txt':
            $mimetype = "text/plain";
            break;
            case 'rtx':
            $mimetype = "text/richtext";
            break;
            case 'htc':
            $mimetype = "text/x-component";
            break;
            case 'vcf':
            $mimetype = "text/x-vcard";
            break;
           
           
            //Applications MIME Types
            
            case 'doc':
            $mimetype = "application/msword";
            break;
            case 'xls':
            $mimetype = "application/vnd.ms-excel";
            break;
            case 'ppt':
            $mimetype = "application/vnd.ms-powerpoint";
            break;
            case 'pps':
            $mimetype = "application/vnd.ms-powerpoint";
            break;
            case 'pot':
            $mimetype = "application/vnd.ms-powerpoint";
            break;
          
            case "ogg":
            $mimetype = "application/ogg";
            break;
            case "pls":
            $mimetype = "application/pls+xml";
            break;
            case "asf":
            $mimetype = "application/vnd.ms-asf";
            break;
            case "wmlc":
            $mimetype = "application/vnd.wap.wmlc";
            break;
            case 'dot':
            $mimetype = "application/msword";
            break;
            case 'class':
            $mimetype = "application/octet-stream";
            break;
            case 'exe':
            $mimetype = "application/octet-stream";
            break;
            case 'pdf':
            $mimetype = "application/pdf";
            break;
            case 'rtf':
            $mimetype = "application/rtf";
            break;
            case 'xla':
            $mimetype = "application/vnd.ms-excel";
            break;
            case 'xlc':
            $mimetype = "application/vnd.ms-excel";
            break;
            case 'xlm':
            $mimetype = "application/vnd.ms-excel";
            break;
           
            case 'msg':
            $mimetype = "application/vnd.ms-outlook";
            break;
            case 'mpp':
            $mimetype = "application/vnd.ms-project";
            break;
            case 'cdf':
            $mimetype = "application/x-cdf";
            break;
            case 'tgz':
            $mimetype = "application/x-compressed";
            break;
            case 'dir':
            $mimetype = "application/x-director";
            break;
            case 'dvi':
            $mimetype = "application/x-dvi";
            break;
            case 'gz':
            $mimetype = "application/x-gzip";
            break;
            case 'js':
            $mimetype = "application/x-javascript";
            break;
            case 'mdb':
            $mimetype = "application/x-msaccess";
            break;
            case 'dll':
            $mimetype = "application/x-msdownload";
            break;
            case 'wri':
            $mimetype = "application/x-mswrite";
            break;
            case 'cdf':
            $mimetype = "application/x-netcdf";
            break;
            case 'swf':
            $mimetype = "application/x-shockwave-flash";
            break;
            case 'tar':
            $mimetype = "application/x-tar";
            break;
            case 'man':
            $mimetype = "application/x-troff-man";
            break;
            case 'zip':
            $mimetype = "application/zip";
            break;
            case 'xlsx':
            $mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            break;
            case 'pptx':
            $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            break;
            case 'docx':
            $mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            break;
            case 'xltx':
            $mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
            break;
            case 'potx':
            $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.template";
            break;
            case 'ppsx':
            $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
            break;
            case 'sldx':
            $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.slide";
            break;
          
            ///Audio and Video Files
            
            case 'mp3':
            $mimetype = "audio/mpeg";
            break;
            case 'wav':
            $mimetype = "audio/x-wav";
            break;
            case 'au':
            $mimetype = "audio/basic";
            break;
            case 'snd':
            $mimetype = "audio/basic";
            break;
            case 'm3u':
            $mimetype = "audio/x-mpegurl";
            break;
            case 'ra':
            $mimetype = "audio/x-pn-realaudio";
            break;
            case 'mp2':
            $mimetype = "video/mpeg";
            break;
            case 'mov':
            $mimetype = "video/quicktime";
            break;
            case 'qt':
            $mimetype = "video/quicktime";
            break;
            case 'mp4':
            $mimetype = "video/mp4";
            break;
            case 'm4a':
            $mimetype = "audio/mp4";
            break;
            case 'mp4a':
            $mimetype = "audio/mp4";
            break;
            case 'm4p':
            $mimetype = "audio/mp4";
            break;
            case 'm3a':
            $mimetype = "audio/mpeg";
            break;
            case 'm2a':
            $mimetype = "audio/mpeg";
            break;
            case 'mp2a':
            $mimetype = "audio/mpeg";
            break;
            case 'mp2':
            $mimetype = "audio/mpeg";
            break;
            case 'mpga':
            $mimetype = "audio/mpeg";
            break;
            case '3gp':
            $mimetype = "video/3gpp";
            break;
            case '3g2':
            $mimetype = "video/3gpp2";
            break;
            case 'mp4v':
            $mimetype = "video/mp4";
            break;
            case 'mpg4':
            $mimetype = "video/mp4";
            break;
            case 'm2v':
            $mimetype = "video/mpeg";
            break;
            case 'm1v':
            $mimetype = "video/mpeg";
            break;
            case 'mpe':
            $mimetype = "video/mpeg";
            break;
            case 'avi':
            $mimetype = "video/x-msvideo";
            break;
            case 'midi':
            $mimetype = "audio/midi";
            break;
            case 'mid':
            $mimetype = "audio/mid";
            break;
            case 'amr':
            $mimetype = "audio/amr";
            break;
            
            
            default:
            $mimetype = "application/octet-stream";
        
            
        }
        
        
       }
        
        
          //off output buffering to decrease Server usage
          @ob_end_clean();
        
          if(ini_get('zlib.output_compression')){
            ini_set('zlib.output_compression', 'Off');
          }
        
          header('Content-Description: File Transfer');
          header('Content-Type: '.$mimetype);
          header('Content-Disposition: attachment; filename='.$newfilename.'');
          header('Content-Transfer-Encoding: binary');
          header("Expires: Wed, 07 May 2013 09:09:09 GMT");
	    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	    header('Cache-Control: post-check=0, pre-check=0', false);
	    header('Cache-Control: no-store, no-cache, must-revalidate');
	    header('Pragma: no-cache');
          header('Content-Length: '.$filesize);
        
        
          ///Will Download 1 MB in chunkwise
          $chunk = 1 * (1024 * 1024);
          $nfile = @fopen($file,"rb");
          while(!feof($nfile))
          {
                 
              print(@fread($nfile, $chunk));
              @ob_flush();
              @flush();
          }
          @fclose($filen);
               


}
public function compressImage($source_image, $compress_image) {
		$image_info = getimagesize($source_image);	
		if ($image_info['mime'] == 'image/jpeg') { 
			$source_image = imagecreatefromjpeg($source_image);
			imagejpeg($source_image, $compress_image, 75);
		} elseif ($image_info['mime'] == 'image/gif') {
			$source_image = imagecreatefromgif($source_image);
			imagegif($source_image, $compress_image, 75);
		} elseif ($image_info['mime'] == 'image/png') {
			$source_image = imagecreatefrompng($source_image);
			imagepng($source_image, $compress_image, 6);
		}	    
		return $compress_image;
	} 
}
