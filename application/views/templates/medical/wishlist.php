<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>My Account</h1>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>  
      
    </div><!-- /.body -->
</div><!-- /.widget -->

            <div class="widget">
	<h1 class="border">special offers</h1>
	<ul class="product-list">
        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img alt="" src="<?php echo base_url()?>assets/med/images/products/product-small-01.jpg">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Netbook Acer </a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>

        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img width="200" height=" 100px" alt="" src="<?php echo base_url()?>/assets/med/images/products/Pro-1.png">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">PowerShot Elph 115 16MP Digital Camera</a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>
 </ul>
</div><!-- /.widget -->
           
         </div>
		 <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
    <div class="tab-content">
	   <div id="list-view" class="tab-pane in active">
                <div class="products-list">
                   <?php ///print_r($mywishlist);
				   foreach($mywishlist as $product){
					   $b = $this->Brands_model->get_brand_name($product['brand_id']);
					   ?>
                    <div class="product-item product-item-holder">
                        <div class="ribbon red"><span>sale</span></div> 
                        <div class="ribbon blue"><span>new!</span></div>
                        <div class="row">
                            <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                </div>
                            </div><!-- /.image-holder -->
                            <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                <div class="body">
                                    <div class="label-discount green"><?php
$oldFigure = $product['price'];
 $newFigure = $product['old_price'];
 
 $percentChange = (1 - $oldFigure / $newFigure) * 100;
 
 
$total_off= round($percentChange);
									if($total_off >0){
									 echo "-" .round($percentChange) . "%"." sale";
									}?></div>
                                    <div class="title">
                                        <a href="#"><?php echo $product['title']; ?></a>
                                    </div>
                                    <div class="brand"><?php echo $b['name']?></div>
                                    <div class="excerpt">
                                        <p><?php echo $product['basic_description']; ?></p>
                                    </div>
                                    <div class="addto-compare">
                                        <a class="btn-add-to-compare" href="<?php echo base_url()?>add-to-compair/<?php $product['id']; ?>">add to compare list</a>
                                    </div>
                                </div>
                            </div><!-- /.body-holder -->
                            <div class="no-margin col-xs-12 col-sm-3 price-area">
                                <div class="right-clmn">
                                    <div class="price-current">₹. <?php echo number_format($product['price'],2);?></div>
                                    <div class="price-prev">₹.<?php echo number_format($product['old_price'],2);?></div>
                                    <div class="availability"><label>availability:</label><span class="available">  in stock</span></div>
                                    <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                  
                                </div>
                            </div><!-- /.price-area -->
                        </div><!-- /.row -->
                    </div><!-- /.product-item -->

<?php  } ?>
                   </div><!-- /.products-list -->

              </div><!-- /.products-grid #list-view -->

       
   </div>  
   </div> </div>
</div>
<style>h2 {
    color: #34ade2;
}</style>