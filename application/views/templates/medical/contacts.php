<?php  $this->load->view('templates/medical/top_head'); ?>
<div class="container">
		<div class="row">
			<?php
                if ($this->session->flashdata('resultSend')) {
                    ?>
                    <hr>
                    <div class="alert alert-info"><?= $this->session->flashdata('resultSend') ?></div>
                    <hr>
                <?php }
                ?>
			<div class="col-md-8">
				<section class="section leave-a-message">
					<h2 class="bordered">Keep In Touch With Us </h2>
					
					<form id="contact-form" class="contact-form cf-style-1 inner-top-xs" method="POST" action="">
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Your Name*</label>
                                <input class="le-input" name="name" placeholder="Enter Your Name" required>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Your Email*</label>
                                <input class="le-input" name="email" placeholder="Enter email" required>
                            </div>
                        </div><!-- /.field-row -->
                        
                        <div class="field-row">
                            <label>Mobile</label>
                            <input type="text" class="le-input" placeholder="Enter Mobile Number" name="subject">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Your Message</label>
                            <textarea rows="8" class="le-input" name="message" id="message"></textarea>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge">Send Message</button>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.contact-form -->
				</section><!-- /.leave-a-message -->
			</div><!-- /.col -->

			<div class="col-md-4">
				<section class="our-store section inner-left-xs">
					<h2 class="bordered">Visit Our  Store</h2>
					
					<p>X-17, F Block, Pocket X, Okhla Phase II, Okhla Industrial Area, New Delhi, Delhi 110020 Phone: 09871716364</br>
					Email: <a href="mailto:support@yourstore.com">support@aspharmapvtltd.com</a></p>
				</section><!-- /.our-store -->
			</div><!-- /.col -->

		</div><!-- /.row -->
	</div>