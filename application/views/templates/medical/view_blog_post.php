<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><style>
.add-to-cart img {
    display: none;
}
#small_carousel a.right.carousel-control {
    height: auto;
    background: none;
    line-height: 20px;
    top: -44px;
    bottom: auto;
    font-size: 5px;
    padding: 0px;
	 opacity: 1;
}
#small_carousel a.left.carousel-control {
    line-height: normal;
    top: -44px;
    bottom: auto;
    font-size: 5px;
    background: none;
    padding: 0;
    right: 26px !important;
    left: auto !important;
    opacity: 1;
}
#small_carousel .product-list.item .inner a.add-to-cart.btn-add.more-blue {
    background: #8cc652;
    color: #fff;
    padding: 6px 20px;
    border-radius: 4px;
	display: inline-block;
}
ol.carousel-indicators {
    display: none;
}
.product-list.item h2 {
    margin-top: 10px;
}
a.info-btn.gradient-color {
    display: none;
}
#small_carousel .product-list.item .inner a.add-to-cart.btn-add {
    display: none;
}
#small_carousel .product-list.item .inner {
    padding: 20px 15px 30px;
    background: #f2f2f2;
    margin-bottom: 20px;
    text-align: center;
    line-height: 30px;
}
.filter-sidebar .title {
    background: #8cc652;
    font-size: 20px;
    padding: 8px 20px;
    margin: 10px 0px;
    color: #fff;
}
div#search-input-blog {
    background: #f2f2f2;
    padding: 10px 11px;
    margin-top: 20px;
    position: relative;
}
div#search-input-blog button.btn.btn-danger {
    background: #8cc652;
    position: absolute;
    right: 0;
    z-index: 2;
    border-color: #8cc652;
}
div#search-input-blog button.btn.btn-danger span.glyphicon.glyphicon-search::before {
    content: "\f002";
    font-family: fontawesome;
}
.blog-home-left-categ {
    display: inline-flex;
    margin-top: 20px;
}
div#search-input-blog input.search-query.form-control {
    border: 1px solid #ccc;
    border-radius: 0px;
}
div#latest-blog .alone.title h3 {
    margin: 20px 0px;
    text-transform: uppercase;
}
#latest-blog .caption p.description {
    margin: 10px 0px;
    line-height: 26px;
}
#latest-blog .caption {
    padding: 20px 20px;
}
</style>
<?php  $this->load->view('templates/medical/top_head'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-3 left-col-archive">
            <?= $archives ?>
            <a href="<?= LANG_URL . '/blog' ?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> <?= lang('go_back') ?></a>
			 <div class="filter-sidebar">
                <div class="title">
                    <span><?= lang('best_sellers') ?></span>
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                </div>
                <?= $load::getProducts($bestSellers, '', true) ?>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="alone title">
                <span><?= $article['title'] ?></span>
            </div>
            <span class="blog-preview-time">
                <i class="fa fa-clock-o"></i>
                <?= date('M d, y', $article['time']) ?>
            </span>
            <div class="thumbnail blog-detail-thumb">
                <img src="<?= base_url('attachments/blog_images/' . $article['image']) ?>" alt="<?= $article['title'] ?>">
            </div>
            <div class="blog-description">
                <?= $article['description'] ?>
            </div>
        </div>
    </div>
</div>