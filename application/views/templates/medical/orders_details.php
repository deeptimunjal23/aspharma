<?php  $this->load->view('templates/medical/top_head'); ?>

<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>My Account</h1>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>
      
    </div><!-- /.body -->
</div><!-- /.widget -->

            <div class="widget">
	<h1 class="border">special offers</h1>
	<ul class="product-list">
        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img alt="" src="<?php echo base_url();?>assets/med/images/products/product-small-01.jpg">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Netbook Acer </a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>

        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img width="200" height=" 100px" alt="" src="<?php echo base_url()?>/assets/med/images/products/Pro-1.png">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Order History</a>
                    <div class="price">
                        <div class="price-prev"></div>
                        <div class="price-current"></div>
                    </div>
                </div>  
            </div>
        </li>
 </ul>
</div><!-- /.widget -->
           
         </div>
        <div class="col-sm-9" style="margin-top: 1px;">
        <div class="orderDetailsContent">
   <div class="" id="orderStatusContainer">
      <div class="mapTitle showDesktop">Order Details</div>
      <div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606">Patient: </span><span class="_374y6 _3aKoJ" style="color:red">
			<?php echo ucfirst($single_item["patient_name"]);?></span></div>
            <div><span class="_2tYiJ"style="color:#d88606"> Order No: </span><span class=""><strong><?php echo $single_item["order_id"];?></strong></span></div>
         </div>
         <div class="_2vCS5">
            <div class="_217or">
               <span class="_2tYiJ"style="color:#d88606">Order Status:</span>
               <span class="_374y6">
			     <?php 
				 $disable="0";
				 if($single_item["is_deleted"]=="1"){?>
                  <span class="u-boldText" style="color: rgb(240, 89, 101);">Order Cancelled</span>
				 <?php $disable="1"; }elseif($single_item["order_status"]=="canceled"){ ?>
				 <span class="u-boldText" style="color:red"><?php echo "Canceled"; ?></span>
				 <?php $disable="1"; }elseif($single_item["order_status"]=="closed"){ ?>
				 <span class="u-boldText" style="color:red"><?php echo "Closed"; ?></span>
				 <?php }elseif($single_item["order_status"]=="Pending Call Review Yet"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Pending Call Review Yet"; ?></span>
				 <?php $disable="1"; }elseif($single_item["order_status"]=="fraud"){ ?>
				 <span class="u-boldText" style="color:red"><?php echo "Suspected Fraud"; ?></span>
				 <?php } elseif($single_item["order_status"]=="holded"){ ?>
				 <span class="u-boldText" style="color:brown"><?php echo "On Hold"; ?></span>
				 <?php }elseif($single_item["order_status"]=="payment_review"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Payment Review"; ?></span>
				 <?php }elseif($single_item["order_status"]=="pending"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Pending Review"; ?></span>
				 <?php }elseif($single_item["order_status"]=="pending_payment"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Pending Payment"; ?></span>
				 <?php }elseif($single_item["order_status"]=="payment_review"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Payment Review"; ?></span>
				 <?php }elseif($single_item["order_status"]=="pending_payment"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Pending PayPal"; ?></span>
				 <?php }elseif($single_item["order_status"]=="processing"){ ?>
				 <span class="u-boldText" style="color:green"><?php echo "Processing PayPal"; ?></span>
				 <?php }?>
                  <div class="_1kf4y"></div>
               </span>
               <div class="_26nvV"></div>
            </div>
            <div class="_27Wwk _1FOxy">
               <img alt="o-To" class="_22qPT" src="https://cdn3.iconfinder.com/data/icons/eldorado-stroke-transport/40/truck_1-512.png">
               <div class="_3w7sW _1FOxy _1BNYc"><span class="_2tYiJ">Delivery Date:</span><span class="_374y6 u-boldText"><?php 
			   echo date("d-M-Y", strtotime($single_item["uploaded_date"]))."\n";?></span></div>
            </div>
         </div>
      </div>
   </div>
   <div id="orderStatusContainer">
      <div class="_2O7an">Delivery Address Details</div>
      <div class="_3Tg-d">
         
         <div class="_10XI8 ZKHfL">
            <div class="_3W6vY"><?php echo ucfirst($single_item["patient_name"]);?></div>
            <div class="_365Xc"> Flat No. : <?php echo $single_item["flat_no"];?></div>
            <div class="_365Xc">Street Name : <?php echo $single_item["street_name"];?></div>
            <div class="_365Xc"><?php echo $single_item["address"];?></div> 
			<div class="_365Xc">Pincode : <?php echo $single_item["pincode"];?></div>
			<div class="_365Xc"><img width="20" src="https://image.flaticon.com/icons/svg/35/35459.svg">  <?php echo $single_item["phone"];?></div>
         </div>
      </div>
   </div>
   <div class="_3Tg">
      <div class="HOjXH">
         <div>MEDICINES</div>
        
      </div>
      <div>
         <div class="_3Tg-d">
            
            <div class="_9ocZk"><?php	if(!empty($single_item['prescription_image'])) {
									$file_name =$single_item['prescription_image'];
									$fileUrl = base_url().'attachments/prescription/'.$file_name;
?>
                    <img class="popover" src="<?php echo $fileUrl; ?>" style="display:block"  />
					<img class="big_img" src="http://www.ace-loan-finder.co.uk/images/credit_card-02.jpeg" />
<?php } ?>
</br>
<?php if(!empty($single_item['admin_extra_note'])){?>
<h3>Extra Note From Supplier</h3>
<ul class="tabled-data">
                        <li>
                        
                            <div class="value"><?php echo $single_item['admin_extra_note'];?></div>
                        </li>
						</ul>
<?php } ?>
</div>
         </div>
      </div>
   </div>
  <div class="_3Eaqj">
      <div class="HOjXH">
         <div>Payment Detail</div>
         <div class="_1xNu6">*estimated price</div>
      </div>
      <div>
         <div class="_3Tg-d">
            
            <div class="_9ocZk">
			<ul class="tabled-data">
                        <li>
                            <label>Amount</label>
                            <div class="value">
							<?php if(!empty($single_item['amount'])){echo "Rs ".$single_item['amount'];}else{ echo "Pending Amount estimate";} ?></div>
                        </li>
                        <li>
                            <label>Payment Method</label>
                            <div class="value">COD</div>
                        </li>
                        <li>
                            <label>Payment Status</label>
                            <div class="value">Pending</div>
                        </li>
                        <li>
                            <label>color</label>
                            <div class="value">white</div>
                        </li>
                        <li>
                            <label></label>
                            <div class="value"></div>
                        </li>
                    </ul>
			</div> 
         </div>
      </div>
   </div><div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606"> </span><span class="_374y6 _3aKoJ" style="color:red">
			</span></div>
            <div><span class="_2tYiJ" style="color:#d88606"><a class="add-to-cart add-cart-button" data-goto="" href="javascript:void(0);" data-id="1">
                         <span class="le-button"> pay Now</span>
                        </a> </span><span class=""><strong></strong></span></div>
         </div>
       </div>
</div>
</div>   
       </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url()?>/assets/med/css/order.css">
<style>#container {position: absolute; height: 100px; width: 170px; top: 200px; left: 200px;}

.popover {top: 0; left: 0px; position: relative;}

.big_img {height: 250px; position: absolute; top: -40px; left: 180px; display:none;}

#container:hover .big_img {display:block;}</style>