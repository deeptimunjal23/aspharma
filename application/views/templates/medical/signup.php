<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-6">
			<?php
  if($this->session->flashdata('userError')){?>
  <div class="alert alert-danger" style="text-align:center">      
    <?php 
	foreach ($this->session->flashdata('userError') as $error) {
                                echo $error . '<br>';
                            }
	?>
	
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered">Create New Account</h2>
	<form role="form" class="login-form cf-style-1" method="POST" action="<?php echo base_url()?>register">
						<div class="field-row">
                            <label>Name</label>
                            <input type="text" class="le-input" placeholder="Enter Name" name="name"value="<?php if(isset($_POST['name'])){echo$_POST['name'];} ?>">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Phone</label>
                            <input type="text" class="le-input" placeholder="phone" name="phone">
                        </div>
						<div class="field-row">
                            <label>Email</label>
                            <input type="email" class="le-input" placeholder="Email" name="email">
                        </div><div class="field-row">
                            <label>Password</label>
                            <input type="password" class="le-input" placeholder="Password" name="pass">
                        </div><div class="field-row">
                            <label>Password repeat</label>
                            
							<input type="password" name="pass_repeat" placeholder="Password repeat" class="le-input" >
                        </div>
                <div class="buttons-holder">
                <input type="submit" name="signup" class="le-button huge login loginmodal-submit" value="signup">
					   </div>
					</form>

				</section>
			</div>

			<div class="col-md-6">
				<section class="section register inner-left-xs">
					<h2 class="bordered">Already Have  Account</h2>
					<p></p>

					<form role="form" class="register-form cf-style-1">
						
                        <div class="buttons-holder">
                            <a href="<?php echo base_url()?>login" class="le-button huge">Sign In</a>
                        </div><!-- /.buttons-holder -->
					</form>

					<h2 class="semi-bold">Sign In </h2>

					<ul class="list-unstyled list-benefits">
						<li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
						<li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
						<li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main><!-- /.authentication -->
