 
<div id="products-tab" class="wow fadeInUp">
    <div class="container">
        <div class="tab-holder">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" >
                
                <li class="active"><a href="#new-arrivals" data-toggle="tab">New arrivals</a></li>
                <li><a href="#top-sales" data-toggle="tab">Top Sales</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane" id="featured">
                
				<?php $feature_products_top = $this->Products_model->get_feature_products();foreach($feature_products_top as $topfeature){?>
                    <div class="product-grid-holder">
                    <div class="row">
                        <div class="col-sm-4 col-md-3 no-margin product-item-holder">
                            <div class="product-item">
                                <?php $pro_img=base_url("attachments/shop_images/").$topfeature['image'];?>
                                <div class="image">
								<?php if(!file_exists($pro_img)){?>
                                    <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url("attachments/shop_images/").$topfeature['image']; ?>" />
								<?php }else{?>
								<img src="<?php echo base_url("/assets/med/images/noimage.jpg");?>"/>
								<?php } ?>
                                </div>
                                <div class="body">
                                    
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $topfeature['url'] ?>"><?php echo $topfeature['title']; ?> </a>
                                    </div><?php $brand=$this->Brands_model->get_brand_name($topfeature['brand_id']);?>
                                    <div class="brand"><?php echo $brand["name"];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev"> <?php echo Rs. $topfeature['old_price']; ?></div>
                                    <div class="price-current pull-right"><?php echo Rs. $topfeature['price']; ?></div>
                                </div>

                                <div class="hover-area">
                                    <div><a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $topfeature['id'] ?>"><span class="le-button">  add to cart</span></a>
                                    </div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $topfeature['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $topfeature['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                           
                    </div>
                    <div clearfix></div>
                   
				<?php } /*
				   <div class="loadmore-holder text-center">
                        <a class="btn-loadmore" href="#">
                            <i class="fa fa-plus"></i>
                            load more products</a>
                    </div> 
*/?>
                </div>
				
                <div class="tab-pane active wow fadeInUp" id="new-arrivals">
                    <div class="product-grid-holder">
                    <div class="row">
                    <div class="carousel-holder">
                    
                    <div class="col-md-12">
                    <div class="title-nav">
                <h1 style="background:none;">New Arrivals</h1>
                <div class="nav-holder">
                    <a href="#prev" data-target="#new-arrival" class="slider-prev btn-prev fa fa-angle-left"></a>
                    <a href="#next" data-target="#new-arrival" class="slider-next btn-next fa fa-angle-right"></a>
                </div>
            </div>
            
            		<div class="row">
                    
                    <div id="new-arrival" class="owl-carousel product-grid-holder owl-theme">
                    
                      <?php $new_arrival = $this->Public_model->new_pro(); 
             				foreach($new_arrival as $new){	  ?>  
                        <div class="no-margin product-item-holder carousel-item">
                            <div class="product-item">
                                <div class="ribbon blue"><span>New!</span></div> 
                                <div class="image">
								<?php $pro_img_new=base_url("attachments/shop_images/").$new['image']; if(!file_exists($pro_img_new)){?>
                                    <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url("attachments/shop_images/").$new['image']; ?>" />
									<?php }else{?>	<img src="<?php echo base_url("/assets/med/images/noimage.jpg");?>"/><?php } ?>
                                </div>
                                <div class="body">
                                    <div class="label-discount clear"></div>
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $new['url'] ?>"><?php echo $new['title'];?></a>
                                    </div>
									<?php 
									$brand=$this->Brands_model->get_brand_name($new['brand_id'])?>
                                    <div class="brand"><?php echo $brand['name'];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev"><?php echo Rs. number_format($new['old_price'],2);?></div>
                                    <div class="price-current pull-right"><?php echo Rs. number_format($new['price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
                                        <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $new['id'] ?>">
                         <span class="le-button">add to cart</span>
                        </a>
                                    </div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $new['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $new['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php }?>
                      </div>
                      </div>
                      
                      </div>
                      </div>
                      </div>
					  </div>
                    

                </div>

                <div class="tab-pane" id="top-sales">
                    <div class="product-grid-holder">
                     <div class="row">
                     <div class="carousel-holder">
                     <div class="col-md-12">
                     <div class="title-nav">
                <h1 style="background:none;">Top Sales</h1>
                <div class="nav-holder">
                    <a href="#prev" data-target="#top-sale" class="slider-prev btn-prev fa fa-angle-left"></a>
                    <a href="#next" data-target="#top-sale" class="slider-next btn-next fa fa-angle-right"></a>
                </div>
            </div>
            </div>
             <div id="top-sale" class="owl-carousel product-grid-holder owl-theme">
                     <?php  $bestSellersv = $this->Public_model->topsale();
             				foreach($bestSellersv as $sale){	  ?>
                        <div class="no-margin product-item-holder">
                            <div class="product-item">
                                
                                <div class="ribbon green"><span>bestseller</span></div> 
                                <div class="image">
								<?php $pro_img_sale=base_url("attachments/shop_images/").$sale['image'];?>
<?php if(!file_exists($pro_img_sale)){?>
                                    <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url("attachments/shop_images/").$sale['image']; ?>" /><?php }else{?><img src="<?php echo base_url("/assets/med/images/noimage.jpg");?>"/><?php } ?>
                                </div>
                                <div class="body">
                                    <div class="label-discount clear"></div>
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $sale['url'] ?>"><?php echo $sale['title']; ?></a>
                                    </div>
                                    <div class="brand"><?php 
									$bc=$this->Brands_model->get_brand_name($sale['brand_id'])?><?php echo $bc['name'];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev"><?php echo number_format($sale['old_price'],2);?></div>
                                    <div class="price-current pull-right"><?php echo Rs. number_format($sale['price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
                                       <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $sale['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                    </div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $sale['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $sale['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php }?>
                    </div>
                    </div>
			</div>
                    </div>
                     
                </div> 
            </div>
        </div>
    </div>
</div>

<style>
#new-arrival .no-margin.product-item-holder.carousel-item {
    padding: 0px 15px;
}
#top-sale .no-margin.product-item-holder {
    padding: 0px 15px;
}
#top-sales .nav-holder {
    z-index: 1;
    position: relative;
}
.carousel-holder .title-nav .nav-holder:after {
	display:none;
	]
</style>
