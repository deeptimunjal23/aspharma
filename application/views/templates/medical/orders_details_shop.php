<?php  $this->load->view('templates/medical/top_head'); ?>

<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>My Account</h1>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>
      
    </div><!-- /.body -->
</div><!-- /.widget -->
<?php  $this->load->view('templates/medical/side_bar_products'); ?>    
         </div>
        <div class="col-sm-9" style="margin-top: 1px;">
        <div class="orderDetailsContent">
   <div class="" id="orderStatusContainer">
      <div class="mapTitle showDesktop">Order Details</div>
      <div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606">Name: </span><span class="_374y6 _3aKoJ" style="color:red">
			<?php echo ucfirst($single_address["first_name"]);?></span></div>
            <div><span class="_2tYiJ"style="color:#d88606"> Order No: </span><span class=""><strong># <?php echo $single_order["order_id"];?></strong></span>
			</br>
Placed On: <?= date('d-M-Y / H:i:s', $single_order['date']); ?></div>
         </div>
         <div class="_2vCS5">
            <div class="_217or">
               <span class="_2tYiJ"style="color:#d88606">Order Status:</span>
               <span class="_374y6">
			     <?php echo $single_order["order_id"];?>
                  <div class="_1kf4y"></div>
               </span>
               <div class="_26nvV"></div>
            </div>
            
         </div>
      </div>
   </div>
   <div id="orderStatusContainer">
      <div class="_2O7an">Delivery Address Details</div>
      <div class="_3Tg-d">
         
         <div class="_10XI8 ZKHfL">
            <div class="_3W6vY"><?php echo ucfirst($single_address["first_name"]);?></div>
            <div class="_365Xc">  <?php echo $single_address["address"];?></div>
            <div class="_365Xc"> <?php echo $single_address["first_name"];?></div>
            <div class="_365Xc"><?php echo $single_address["city"];?></div> 
			<div class="_365Xc">Pincode : <?php echo $single_address["post_code"];?></div>
			<div class="_365Xc"><img style="width:20px;" src="https://image.flaticon.com/icons/svg/35/35459.svg">  <?php echo $single_address["phone"];?></div>
         </div>
      </div>
   </div>
   <div class="_3Tg">
      <div class="HOjXH">
         <div><b>Products</b></div>
        
      </div>
      <div>
         <div class="_3Tg-d">
		 <ul class="tabled-data">
            <?php $arr_products = unserialize($single_order['products']);
			$total_amount = 0;
			foreach ($arr_products as $product_id => $product_quantity) {
				$productInfo = modules::run('admin/ecommerce/products/getProductInfo', $product_quantity['product_info']["id"], true);
				$product_name = $this->Products_model->get_single_product($productInfo["id"]);?>
	<?php		$total_amount += str_replace(' ', '', str_replace(',', '.',$productInfo['price']));?>
                        <li>
                            
                            <div class="value"><img src="<?= base_url('attachments/shop_images/' . $productInfo['image']) ?>" alt="Product" style="width:100px; margin-right:10px;" class="img-responsive">
				</br>
							</div>
                        </li>
                        <li>
                      <label>   <a  class="_1zv5A" href="<?php echo base_url().$product_name["url"];?>"><?php echo $product_name["title"];?></a></label>
                        </li>
                        <li>
                            <label>Qty</label>
                            <div class="value"><?php echo $product_quantity["product_quantity"]; 
			
			?></div>
                        </li>
						<?php }?>
                    </ul>
			
            <div class="_9ocZk">
</br>
<?php if(!empty($single_address['notes'])){?>
<h3>Extra Note </h3>
<ul class="tabled-data">
                        <li>
                        
                            <div class="value"><?php echo $single_address['notes'];?></div>
                        </li>
						</ul>
<?php } ?>
</div>
         </div>
      </div>
   </div>
  <div class="_3Eaqj">
      <div class="HOjXH">
         <div><b>Payment Detail</b></div>
         <div class="_1xNu6"></div>
      </div>
      <div>
         <div class="_3Tg-d">
            
            <div class="_9ocZk">
			<ul class="tabled-data">
                        <li>
                            <label>Total Amount</label>
                            <div class="value">
							<?php 
							 $t=$total_amount + $single_order['shipping_charges'];
							echo Rs.$t;?></br>
							<small>including shipping chages(<?php echo Rs.$single_order['shipping_charges'];?></small>)
							
							
							</div>
                        </li>
                        <li>
                            <label>Payment Type</label>
                            <div class="value">
							<?php if($single_order["payment_type"]=="cashOnDelivery"){echo "<b>Cash On Delivery</b>";}else{ echo $single_order["payment_type"];}?>
							
							</div>
                        </li>
                        <li>
                            <label>Order Status</label>
                            <div class="value">
							<?php  if(!empty($single_order['order_status'])){echo $single_order['order_status'];}else{echo "<b>pending</b>";}?>
							</div>
                        </li>
                        
                        <li>
                            <label></label>
                            <div class="value"></div>
                        </li>
                    </ul>
			</div> 
         </div>
      </div>
   </div><div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606"> </span><span class="_374y6 _3aKoJ" style="color:red">
			</span></div>
            <div><span class="_2tYiJ" style="color:#d88606"></span><span class=""><strong></strong></span></div>
         </div>
       </div>
</div>
</div>   
       </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url()?>/assets/med/css/order.css">
<style>#container {position: absolute; height: 100px; width: 170px; top: 200px; left: 200px;}

.popover {top: 0; left: 0px; position: relative;}

.big_img {height: 250px; position: absolute; top: -40px; left: 180px; display:none;}

#container:hover .big_img {display:block;}</style>