  <div class="widget">
	<h1 class="border">Special Offers</h1>
	<ul class="product-list">
      <?php foreach($newProducts as $product){?>
	   <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <?php $pro_img_new=base_url("attachments/shop_images/").$product['image'];if(file_exists($pro_img_new)){?>
						
						<img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" /><?php }else{?>	<img src="<?php echo base_url("/assets/med/images/noimage.jpg");?>"/><?php } ?>
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="<?= LANG_URL . '/' . $product['url'] ?>"><?php echo $product['title']; ?></a>
                    <div class="price">
                        <div class="price-prev">₹. <?php echo number_format($product['old_price'],2);?></div>
                        <div class="price-current">₹. <?php echo number_format($product['old_price'],2);?></div>
                    </div>
                </div>  
            </div>
        </li>
	  <?php } ?>
        </ul>
</div>