<?php  $this->load->view('templates/medical/top_head'); ?>
<?php
  if($this->session->flashdata('flash_welcome')){?>
  <div class="alert alert-success" style="text-align:center">      
    <?php echo $this->session->flashdata('flash_welcome')?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
<section id="category-grid">
    <div class="container">
        <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>Product Filters</h1>
    <div class="body bordered">
        
        <div class="category-filter">
            <h2>Brands</h2>
            <hr>
            <ul> <?php foreach($brands as $brand){?>
                <li>
				<a href="<?php echo base_url(); ?>home/medicines_by_brands/<?php echo $brand['id'];  ?>">
				<label><?php echo $brand['name'];  ?></label> <span class="pull-right"></span></a>
				</li>
				
			<?php } ?> 
            
			</ul>
        </div><!-- /.category-filter -->
        
      
    </div><!-- /.body -->
</div><!-- /.widget -->

    <div class="widget">
	<h1 class="border">Special offers</h1>
	<ul class="product-list">
        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url()?>assets/med/images/products/product-small-01.jpg" />
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Netbook Acer </a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>

        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url()?>assets/med/images/products/Pro-1.png" />
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">PowerShot Elph 115 16MP Digital Camera</a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>
</ul>
</div>
   <div class="widget">
	<div class="simple-banner">
		<a href="#"><img alt="" class="img-responsive" src="assets/images/blank.gif" data-echo="assets/images/banner/banner-simple.jpg" /></a>
	</div>
</div>
         </div>
        

        <div class="col-xs-12 col-sm-9 no-margin wide sidebar">

            <section id="recommended-products" class="carousel-holder hover small">

    <div class="title-nav">
        <h2 class="inverse">compare List</h2>
        <div class="nav-holder">
            <a style="font-size:15px;" href="<?php echo base_url()?>compare-list" data-target="#owl-recommended-products" class="">compare All</a>
            
        </div>
    </div><!-- /.title-nav -->

    <div id="owl-recommended-products" class="owl-carousel product-grid-holder">
	  <?php foreach($compair_list_items as $product){?>
        <div class="no-margin carousel-item product-item-holder hover size-medium">
            <div class="product-item">
                
                <div class="image">
                    <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                </div>
                <div class="body">
                    <div class="title">
                        <a href="single-product.html"><?php echo $product['title']; ?></a>
                    </div>
                    
                </div>
                <div class="prices">
                    <div class="price-current text-right">RS. <?php echo number_format($product['price'],2);?></div>
                </div>
             </div>
        </div><!-- /.carousel-item -->

	  <?php } ?>
	  
		</div>
</section>  </div>
        
         
    </div>
</section>
