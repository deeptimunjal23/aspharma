<?php  $this->load->view('templates/medical/top_head'); 
$p_id = $this->session->userdata('prescription_id');
?>

<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-9">
				<section class="section sign-in inner-right-xs">
				 <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                <p><small>Upload Prescription</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Delivery address </small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Payment Method</small></p>
            </div>
           <div class="stepwizard-step col-xs-3"> 
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Order Review</small></p>
            </div> 
        </div>
    </div>
    
    <form role="form" action="<?php echo base_url(); ?>home/upload_prescription_process" method="POST">
        <div class="panel panel-primary setup-content" id="step-1">
            
            <div class="panel-body">
                <div class="form-group">
				<?php 
				$user_id=$this->session->userdata('logged_user');
				$image=$this->Public_model->get_uploded_prescription($user_id ,$p_id);
				?>
                 
				<?php	if(!empty($image['prescription_image'])) {
									$file_name =$image['prescription_image'];
									$fileUrl = base_url().'attachments/prescription/'.$file_name;
?>
                    <img src="<?php echo $fileUrl; ?>" width="400" class="img-thumbnail" />
<?php } ?>
                </div>
                
                <button class="btn btn-primary nextBtn pull-right le-button" type="button">Next</button>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-2">
            
            <div class="panel-body">
                <div class="form-group">
           <label class="control-label">Patient Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control gtt" placeholder="Enter Patient Name" name="patient_name" id="patient_name"/>
                </div>
                <div class="form-group">
                    <label class="control-label">Building Name and Flat Number</label>
                    <input maxlength="200" type="text" required="required" class="form-control gtt" placeholder="Enter Building Name and Flat Number" name="flat_name" id="flat_name"  />
                </div>
				<div class="form-group">
                    <label class="control-label">Street Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control gtt" placeholder="Enter Street Name" name="street_name" id="street_name" />
                </div>
				<div class="form-group">
                    <label class="control-label">Address</label>
                    <textarea required="required" class="form-control gtt" placeholder="Enter Street Name" name="address" id="address"></textarea>
                </div>
				<div class="form-group row">
                  <div class="col-sm-6">
				  <label class="control-label">Pincode</label>
                    <input type="text" class="form-control gtt" id="pincode" name="pincode" value="" required="required">
					<input type="hidden"  name="p_id" value="<?php echo $p_id;?>">
                  </div>
                
                  <div class="col-sm-6">
				  <label class="control-label">Mobile Number</label>
                  <input type="text" class="form-control gtt" id="mobile_number" name="mobile_number" value="" required="required">
				  <span class="error_Msg">Error! Phone number can contain only numbers from 0-9 input 10 digits number Only</span>
                  </div>
                </div>
                <button class="btn btn-primary nextBtn pull-right le-button" type="button">Next</button>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-body">
                <div class="form-group">
                  <h1 style="color: #34ade2;"> Pay online later</h1>
				   <small>We will notify you to pay online after your order is packed. This usually takes less than 18 hrs.</small>
                </div>
              <button class="btn btn-primary nextBtn pull-right le-button" type="button">Next</button>
            </div>
         </div>
        
        <div class="panel panel-primary setup-content" id="step-4">
                <div class="panel-body">
			<div class="form-group row">
                  <div class="col-sm-6">
				  <?php	if(!empty($image['prescription_image'])) {
									$file_name =$image['prescription_image'];
									$fileUrl = base_url().'attachments/prescription/'.$file_name;
?>
                    <img src="<?php echo $fileUrl; ?>" width="200" class="img-thumbnail" />
<?php } ?>
                  </div>
                
                  <div class="col-sm-6">
				   <address>
Patient Name :<a href="#"><span id="pnm">N/A</span></a>.<br> 
Building Name and Flat Number:<br><span id="flt">N/A</span>
Address: <span id="add">N/A</span><br>
Mobile : <span id="mo">N/A</span><br>
Pincode : <span id="pin">N/A</span><br>

</address>
                  </div>
                </div>
                
                
				<button type="submit" name="save"  value="save" class="btn btn-lg btn-success le-button">Place Order</button>
				
            </div>
       
            
        </div> 
    </form>
	
					
					</section>
			</div>

			<div class="col-md-3">
				<section class="section register inner-left-xs">
					
					<p>You can also start by uploading a photo of your prescription</p>
					<small>Photo should not be larger than 10 MB</small>

				<h4 class="semi-bold"><button class="PEBtn">Attach Prescription</button></h4>

					<ul class="list-unstyled list-benefits">
						<li>valid prescription Contain</li>
						<li><i class="fa fa-check primary-color"></i> Upload Clear Image<br>
						<small>ensure that picture is taken with clear devices</small></li>
						<li><i class="fa fa-check primary-color"></i> government regulation with valid prescription</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->
</form>
		</div><!-- /.row -->
	</div><!-- /.container -->
</main>
<style>.uploadPrescriptionContainer .searchBoxContainer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
} .searchBox {
    width: 95%;
    padding: 24px;
    background-color: #34ade2;
    border-radius: 6px;
} .order{    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border: none;
    width: 90%;
    height: 55px;
    padding: 10px;
    border-radius: 30px;}.searchText{color: #fff;
    padding: 10px;}
	.PEBtn{padding: 10px;
    background: #34ade2;
    border: none;
    color: #fff;
    /* background-color: #013446; */
    color: #fff;
    text-align: center;
    cursor: pointer;
    height: 40px;
    width: 100%;
    border-radius: 6px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    transition: all .3s ease;
    box-shadow: 0 3px 20px 0 rgba(0,0,0,.23);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;}</style>
	<style>
/* Latest compiled and minified CSS included as External Resource*/

/* Optional theme */

/*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/
 body {
    margin-top:30px;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content:" ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}.panel-primary {
    border-color: #428bca;
    border: 1px solid rgba(0,0,0,.12);
    padding: 32px;
    background-color: #fff;
    border-radius: 6px;
}label {
    display: inline-block;
    /* margin-bottom: 5px; */
    font-weight: 700;
    display: inline-block;
    width: 100%;
    float: left;
    padding: 0;
    color: #8897a2;
    /* font-size: 12px; */
    font-weight: 500;
    margin-bottom: 16px;
}</style>
<script>$(document).ready(function () {
$('.error_Msg').hide();
    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;
      if(curStepBtn=="step-3"){
		mobile_number =  document.getElementById("mobile_number").value;
		patient_name =  document.getElementById("patient_name").value;
		flat_name =  document.getElementById("flat_name").value;
		address =  document.getElementById("address").value;pincode =  document.getElementById("address").pincode;
		 $("#mo").text(mobile_number);
		 $("#pnm").text(patient_name);
		 $("#flt").text(flat_name);
		 $("#add").text(address);
		 $("#pin").text(pincode); 
	  }
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
				 var data=$('#mobile_number').val();Validate();  
      
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});  function Validate() {
        var mobile = document.getElementById("mobile_number").value;
        var pattern = /^\d{10}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
		    $('.error_Msg').hide();
            return true;
        }
		 $('.error_Msg').show();
        
        return false;
    }</script>
<style>
.gt{width: 47% !important;}.gtt{border: 1px solid #8cc652; !important;}.error_Msg{color: #fa4b2a;
    padding-left: 10px;}
</style>
