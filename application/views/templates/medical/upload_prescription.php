<?php  $this->load->view('templates/medical/top_head');$p_id = $this->session->userdata('prescription_id'); ?>
<style>
li.tabs-nav__item.js-tabs-item.active {
    max-width: 100% !important;
    display: block;
    width: 100%;
}
span.tabs-nav__drag.js-tabs-drag.active {
    width: 100% !important;
    margin-top: 23px;
}
label.btn.btn-default.btn-file {
    width: 100%;
    text-align: left;
    padding: 20px 20px;
    line-height: 38px;
    margin-bottom: 20px;
}
section.section.register.inner-left-xs {
    margin: 0;
    padding: 0;
    box-shadow: none;
    border: none;
}
.box-2 {
    background: #fff;
    border: 1px solid #eee;
    padding: 10px 10px;
    border-radius: 4px;
    text-align: center;
    line-height: normal;
    margin: 28px 0px;
    box-shadow: 0px 0px 11px #EEE;
}
.box-2 ul.list-unstyled.list-benefits li {
    margin-bottom: 10px;
    line-height: 30px;
    border-bottom: 1px solid #f2f2f2;
    padding: 5px 0px;
}
.box-profile ._2zEPW.consultationTimeSlot.showDesktop {
    display: flex;
}
.box-profile ._326E6 {
    width: 120px;
    margin-right: 20px;
}
.box-profile ._3tdQS {
    line-height: 24px;
    font-size: 14px;
    margin-top: 9px;
}
.box-profile {
    background: #fff;
    border: 1px solid #eee;
    padding: 10px 10px;
    border-radius: 4px;
    line-height: normal;
    margin: 28px 0px;
    box-shadow: 0px 0px 11px #EEE;
}
.box-2 ul.list-unstyled.list-benefits {
    text-align: left;
    padding: 14px 10px;
    text-transform: capitalize;
}
</style>

<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-8">
				<section class="section sign-in inner-right-xs">
<div class="tabs js-tabs">
    <div class="tabs-nav js-tabs-nav" id="fade-example">
        <ul class="tabs-nav__list">
            <li class="tabs-nav__item js-tabs-item active">
                <a class="tabs-nav__link js-tabs-link" href="#tab-1">Browse Image</a>
            </li>
          <!--  <li class="tabs-nav__item js-tabs-item">
                <a class="tabs-nav__link js-tabs-link" href="#tab-2"><span>Prescriptions</span></a>
            </li>
            <li class="tabs-nav__item js-tabs-item">
                <a class="tabs-nav__link js-tabs-link" href="#tab-3">Third tab</a>
            </li>-->
        </ul>
        <span class="tabs-nav__drag js-tabs-drag"></span>
    </div>
    <div class="tabs-content js-tabs-wrap">
        <div class="tab js-tabs-content active" id="tab-1"> <div class="imageupload panel panel-default">
		 <form id="personal-info" action="<?php echo base_url(); ?>home/save_prescription" id="jq-validation-form" enctype="multipart/form-data" method="post">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Upload Image</h3>
                   
                </div>
                <div class="file-tab panel-body">
                    <label class="btn btn-default btn-file">
                        <span>Browse</span>
                      
                        <input type="file" name="image-file">
                    </label>
                    <button type="button" class="btn btn-default">Remove</button>
					
					<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> upload</button>
                </div>
			</form>
</div>
		
		
        <div class="tab js-tabs-content" id="tab-2">Second tab content<br>Second tab content<br>Second tab content<br>Second tab content</div>
        <div class="tab js-tabs-content" id="tab-3">Third tab content<br>Third tab content</div>
    </div>
</div>

				 </section>
			</div>

			<div class="col-md-4">
				<section class="section register inner-left-xs">
				<div class="box-2">	
					<p>You can also start by uploading a photo of your prescription</p>
					<small>Photo should not be larger than 10 MB</small>
                    
                 </div>   
<div class="box-2">	
				<h4 class="semi-bold">
				<?php if(!empty($p_id)){?>
				<a href="<?php echo base_url()?>prescription-process" class="PEBtn">PROCEED TO CHECKOUT</a>
				<?php }else{ ?>
				<a href="#" class="PEBtn">PROCEED TO CHECKOUT</a>
				<?php } ?>
				
				</h4>

</div>
<div class="box-2">	
					<ul class="list-unstyled list-benefits">
						<li>valid prescription Contain</li>
						<li>
						<i class="fa fa-check primary-color"></i> Upload Clear Image<br>
						<small>ensure that picture is taken with clear devices</small></li>
						<li><i class="fa fa-check primary-color"></i> government regulation with valid prescription</li>
					</ul>
                    </div>
     <div class="box-profile">	               
<div class="_2zEPW consultationTimeSlot showDesktop"><div class="_326E6"><img src="http://www.allwhitebackground.com/images/3/3313.jpg" alt=""></div><div class="_3tdQS"><div class="iHXzx">Our pharmacist will call within 1 hour to confirm your order.</div><div class="_3MdWu">Between 8 AM to 10 PM (all days)</div></div></div>
</div>
				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main>
<style>.uploadPrescriptionContainer .searchBoxContainer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
} .searchBox {
    width: 95%;
    padding: 24px;
    background-color: #34ade2;
    border-radius: 6px;
} .order{    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border: none;
    width: 90%;
    height: 55px;
    padding: 10px;
    border-radius: 30px;}.searchText{color: #fff;
    padding: 10px;}
	.PEBtn{padding: 10px;
    background: #34ade2;
    border: none;
    color: #fff;
    /* background-color: #013446; */
    color: #fff;
    text-align: center;
    cursor: pointer;
    height: 40px;
    width: 100%;
    border-radius: 6px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    transition: all .3s ease;
    box-shadow: 0 3px 20px 0 rgba(0,0,0,.23);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;}
	.section {
    text-align: left;
    margin: 30px 0;
    
    background: #fff;
    border: 1px solid #fff;
    -webkit-box-shadow: 1px 1px 4px 3px rgba(214,207,214,0.66);
    -moz-box-shadow: 1px 1px 4px 3px rgba(214,207,214,0.66);
    box-shadow: 1px 1px 4px 3px rgba(214,207,214,0.66);
}</style>

