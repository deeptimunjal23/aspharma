<?php  $this->load->view('templates/medical/top_head'); ?>

<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-9">
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered"></h2>
					<p>Showing all variants for <strong>ACNESOL</strong></p>
					
					<div id="list-view" class="products-grid fade tab-pane  active in">
                <div class="products-list">
                    
                    <div class="product-item product-item-holder">
                        <div class="row">
                            <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                <div class="image">
                                    <img alt="" src="<?php echo base_url()?>assets/med/images/products/Pro-1.png">
                                </div>
                            </div><!-- /.image-holder -->
                            <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                <div class="body">
                                    <div class="label-discount green">-50% sale</div>
                                    <div class="title">
                                        <a href="single-product.html">VAIO Fit Laptop - Windows 8 SVF14322CXW</a>
                                    </div>
                                    <div class="brand">sony</div>
                                    <div class="excerpt">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lobortis euismod erat sit amet porta. Etiam venenatis ac diam ac tristique. Morbi accumsan consectetur odio ut tincidunt.</p>
                                    </div>
                                    
                                </div>
                            </div><!-- /.body-holder -->
                            <div class="no-margin col-xs-12 col-sm-3 price-area">
                                <div class="right-clmn">
                                    <div class="price-current">$1199.00</div>
                                    <div class="price-prev">$1399.00</div>
                                    <div class="availability"><label>availability:</label><span class="available">  in stock</span></div>
                                    <a class="le-button" href="#">Add to cart</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
					</div>

              </div>
					
					
					
</section>
			</div>

			<div class="col-md-3">
				<section class="section register inner-left-xs">
					
					<p>You can also start by uploading a photo of your prescription</p>
					<small>Photo should not be larger than 10 MB</small>

				<h4 class="semi-bold"><button class="PEBtn">Attach Prescription</button></h4>

					<ul class="list-unstyled list-benefits">
						<li>valid prescription Contain</li>
						<li><i class="fa fa-check primary-color"></i> Upload Clear Image<br>
						<small>ensure that picture is taken with clear devices</small></li>
						<li><i class="fa fa-check primary-color"></i> government regulation with valid prescription</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main>
<style>.uploadPrescriptionContainer .searchBoxContainer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
} .searchBox {
    width: 95%;
    padding: 24px;
    background-color: #34ade2;
    border-radius: 6px;
} .order{    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border: none;
    width: 90%;
    height: 55px;
    padding: 10px;
    border-radius: 30px;}.searchText{color: #fff;
    padding: 10px;}
	.PEBtn{padding: 10px;
    background: #34ade2;
    border: none;
    color: #fff;
    /* background-color: #013446; */
    color: #fff;
    text-align: center;
    cursor: pointer;
    height: 40px;
    width: 100%;
    border-radius: 6px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    transition: all .3s ease;
    box-shadow: 0 3px 20px 0 rgba(0,0,0,.23);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;}</style>
	
    
    
<?php  $this->load->view('templates/medical/_parts/footer'); ?><script>
	jQuery(document).ready(function() {
			
	jQuery(".typeahead ").change(function() { 
	
	//alert('in jis');
	});
	});
	</script> 