<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>

<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>My Account</h1>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>
      
    </div><!-- /.body -->
</div><!-- /.widget -->
<?php  $this->load->view('templates/medical/side_bar_products'); ?>
           </div>
        <div class="col-sm-9" style="margin-top: 1px;">
            <div class="container">
  <table class="responsive-table">
    <caption>Orders History</caption>
    <thead>
      <tr>
        <th scope="col"><?= lang('usr_order_id') ?></th>
        <th scope="col"><?= lang('usr_order_date') ?></th>
        <th scope="col"><?= lang('usr_order_address') ?></th>
        <th scope="col"><?= lang('usr_order_phone') ?></th>
        <th scope="col"><?= lang('user_order_products') ?></th><th scope="col"></th>
        
      </tr>
    </thead>
   
    <tbody><?php
                        if (!empty($orders_history)) {
                            foreach ($orders_history as $order) {
                                ?>
      <tr>
	  
        <th scope="row"><?= $order['order_id'] ?></th>
        <td data-title="Released"><?= date('d.m.Y', $order['date']) ?></td>
        <td data-title="Studio"><?= $order['address'] ?></td>
        <td data-title="Worldwide Gross" data-type="currency"><?= $order['phone'] ?></td>
        <td data-title="Domestic Gross" data-type="currency"> <?php
                                        $arr_products = unserialize($order['products']);
										
                                        foreach ($arr_products as $product_id => $product_quantity) {
$productInfo = modules::run('admin/ecommerce/products/getProductInfo', $product_quantity['product_info']["id"], true);
                                            ?>
                                            <div style="word-break: break-all;">
                                                <div>
											
                                                    <img src="<?= base_url('attachments/shop_images/' . $productInfo['image']) ?>" alt="Product" style="width:100px; margin-right:10px;" class="img-responsive">
                                                </div>
                                                <a target="_blank" href="<?= base_url($productInfo['url']) ?>">
                                                    
                                                </a> 
                                               
                                                <div class="clearfix"><?php echo 
									$product_quantity["product_quantity"];
									?>	</div>
                                            </div>
                                            <hr>
                                        <?php } }
                                        ?></td>
										

        <td cla="s"><a href="<?php echo base_url()?>orders-view/<?= $order['order_id'] ?>" class="btn btn-sucess confirm-sucess">View Order</a></td>
      </tr>
       <?php
                           
                        } else {
                            ?>
                            <tr>
                                <td colspan="5"><?= lang('usr_no_orders') ?></td>
                            </tr>
                        <?php } ?>
    </tbody>
  </table>
</div>
        </div>
    </div>
</div>
<style>h2 {
    color: #34ade2;
}</style>