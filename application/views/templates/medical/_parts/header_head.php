<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" /> 
        <meta name="description" content="<?= $description ?>" />
        <meta name="keywords" content="<?= $keywords ?>" />
        <meta property="og:title" content="<?= $title ?>" />
        <meta property="og:description" content="<?= $description ?>" />
        <meta property="og:url" content="<?= LANG_URL ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="<?= base_url('assets/img/site-overview.png') ?>" />
        <title><?= $title ?></title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/bootstrap.min.css">
        
        <!-- Customizable CSS -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/green.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/animate.min.css">

        <!-- Demo Purpose Only. Should be removed in production -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/config.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/custom.css">

        <link href="<?php echo base_url()?>assets/med/css/green.css" rel="alternate stylesheet" title="Green color">
        <link href="<?php echo base_url()?>assets/med/css/blue.css" rel="alternate stylesheet" title="Blue color">
        <link href="<?php echo base_url()?>assets/med/css/red.css" rel="alternate stylesheet" title="Red color">
        <link href="<?php echo base_url()?>assets/med/css/orange.css" rel="alternate stylesheet" title="Orange color">
        <link href="<?php echo base_url()?>assets/med/css/navy.css" rel="alternate stylesheet" title="Navy color">
        <link href="<?php echo base_url()?>assets/med/css/dark-green.css" rel="alternate stylesheet" title="Darkgreen color">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/med/css/page.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/med/css/anitabs.css">
        <!-- Icons/Glyphs -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/med/css/font-awesome.min.css">
        
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url()?>assets/med/images/favicon.ico">

        <script src="<?php echo base_url()?>assets/med/js/jquery-1.10.2.min.js"></script>
        <script src="<?= base_url('loadlanguage/all.js') ?>"></script>
    </head>
<body>
    <div class="wrapper">
       
<nav class="top-bar animate-dropdown">
    <div class="container">
        <div class="col-xs-12 col-sm-7 no-margin">
            <div class="top-left">
            <p>
			<?php $msg = $this->Public_model->get_single_msg("2");
			echo $msg["value"];
			?>
			
			</p>
            </div>
        </div><!-- /.col -->

        <div class="col-xs-12 col-sm-5 no-margin">
            <ul class="right">
                <li class="act">
                   <a href="<?php echo base_url()?>ask-expert">Ask Expert</a>
                </li>
                 <?php  
				 if($this->session->userdata('logged_user')){
				 ?>
                 <li><a href="<?php echo base_url()?>account-information">My Account</a></li>
                <li><a href="<?php echo base_url()?>logout">Logout</a></li>
				 <?php }else{ ?>
				  
				<li><a href="<?php echo base_url()?>register">register</a></li>
                <li><a href="<?php echo base_url()?>login">Login</a></li>
				 <?php } ?>
            </ul>
        </div><!-- /.col -->
    </div><!-- /.container -->
</nav>
<header class="no-padding-bottom header-alt">
    <div class="container no-padding">
        
        <div class="col-xs-12 col-md-3 logo-holder">
           
<div class="logo">
    <a href="<?php echo base_url()?>">
	
        <img alt="logo" src="<?php echo base_url()?>assets/med/images/logo.png" width="233" />
    </a>
</div>     </div>

        <div class="col-xs-12 col-md-6 top-search-holder no-margin">
            
<div class="search-area">
    <form>
        <div class="control-group">
            <input class="typeahead  search-field" placeholder="Search for item" id="searchtext" />

           <a class="search-button" href="#" ></a>    

        </div>
    </form>
</div>    </div>

        <div class="col-xs-12 col-md-3 top-cart-row no-margin">
            <div class="top-cart-row-container">
    <div class="wishlist-compare-holder">
        <div class="wishlist ">
		<?php $count_wishlist="0"; 
			if($this->session->userdata('logged_user')){
  $user_id = $this->session->userdata('logged_user');
			$count_wishlist =$this->Public_model->get_count_wishlist_product($user_id);
			}else{
				$count_wishlist="0";
			}
			$count="0";
			$Ip_address = $_SERVER['REMOTE_ADDR'];
			$count =$this->Public_model->get_count_compair_product($Ip_address);?>
			
            <a href="<?php echo base_url()?>wishlist"><i class="fa fa-heart"></i> wishlist <span class="value">(<?php echo $count_wishlist;?>)</span> </a>
        </div>
        <div class="compare">
            <a href="<?php echo base_url();?>compare-list">
			
			
			<i class="fa fa-exchange"></i> compare <span class="value">(<?php echo $count;?>)</span> </a>
        </div>
    </div>

    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
    <div class="top-cart-holder dropdown animate-dropdown">
        
        <div class="basket">
            
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <div class="basket-item-count">
                    <span class="count"><?= $cartItems['array'] == 0 ? 0 : $sumOfItems ?></span>
                    <img src="<?php echo base_url()?>assets/med/images/icon-cart.png" alt="" />
                </div>

                <div class="total-price-basket"> 
                    <span class="lbl">Your Cart:</span>
                    <span class="total-price">
                        <span class="sign"><?php echo Rs;?></span><span class="value"><?php echo 0 ? 0 :  $cartItems['array']["0"]["sum_price"]; $sumOfItems;?></span>
                    </span>
                </div>
            </a>

            <ul class="dropdown-menu">
			<?= $load::getCartItems($cartItems); ?>
                

                <li class="checkout">
                    <div class="basket-item">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <a href="<?php echo base_url()?>shopping-cart" class="le-button inverse">View Cart</a>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <a href="<?php echo base_url()?>checkout" class="le-button">Checkout</a>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div><!-- /.basket -->
    </div><!-- /.top-cart-holder -->
</div>      </div><!-- /.top-cart-row -->

    </div>
<style>.alert-info {
    background-color: #FF5722 !important;
    border-color: #bce8f1 !important;
    color: #ffffff !important;
    text-align: center;
    text-transform: capitalize;
}
.alert {
    padding: 18px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
    margin-top: 20px;
}.item-in img {
    width: 100px;
}.item-in {
    display: block;
    margin: 3px 0;
    padding: 3px 10px;
}</style>