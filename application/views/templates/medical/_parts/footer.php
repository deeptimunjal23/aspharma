<footer id="footer" class="color-bg">
    

    <!-- /.sub-form-row -->

    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-3 ">
                <!-- ============================================================= CONTACT INFO ============================================================= --><div class="link-widget">
    <div class="widget">
        <h3>COMPANY INFORMATION</h3>
        <ul>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>myaccount">My Account</a></li>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>track-order">Order Tracking</a></li>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>wishlist">Wishlist</a></li>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>customer-service">Customer Service</a></li>
            <li>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>faqs">FAQs</a></li>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>product-support">product-support</a></li>
  <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>return-policy">Return Policy</a></li>
        </ul>
    </div><!-- /.widget -->
</div>
            </div>

            <div class="col-xs-12 col-md-9 no-margin">
                
<div class="row">
<div class="col-md-3">                
<div class="link-widget">
    <div class="widget">
        <h3>PRESCRIPTION DRUGS</h3>
        <div class="upload">
        <a href="<?php echo base_url()?>upload-prescription">Upload Prescription </a>
        </div>
        <ul>
        <h4>NEED HELP?</h4>
        
            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 098717 16364</a></li>
            <li><a href="<?php echo base_url(); ?>contacts"><i class="fa fa-envelope" aria-hidden="true"></i> Write to us</a></li>
   
        </ul>
    </div><!-- /.widget -->
</div></div>
<div class="col-md-3">
<div class="link-widget">
    <div class="widget">
        <h3>Top Brands</h3>
        <ul> <?php $i=1; 
				$brs =	$this->Brands_model->getBrands();	 
						 foreach($brs as $brand){?>
						 <?php if($i <= 8 && $i>=1){ ?>
            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="<?php echo base_url(); ?>medicines-by-brands/<?php echo '1';  ?>?&&product_order=new&price_range=&order_quantity=&brands_id=<?php echo $brand['id'];?>"><?php echo $brand['name'];  ?></a></li>
						 <?php }
						$i++; }						 ?>
        </ul>
    </div><!-- /.widget -->
</div></div>
<div class="col-md-3">
<div class="link-widget">
    <div class="widget">
        <h3>Newsletter</h3>
        <div class="contact-info">
    <!-- /.footer-logo -->
    <div class="social-icons">
      <!--  <ul>
            <li><a href="http://facebook.com/transvelo" class="fa fa-facebook"></a></li>
            <li><a href="#" class="fa fa-twitter"></a></li>
            <li><a href="#" class="fa fa-pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin"></a></li>
            <li><a href="#" class="fa fa-stumbleupon"></a></li>

        </ul>!--->
    </div>
    <p>Get a free subscription to our health and fitness tip and stay tuned to our latest offers </p>
        <form role="form">
                    <input placeholder="Subscribe our newsletter">
                    <button class="le-button">Go</button>
                </form><!-- /.container -->
    
    <!-- /.social-icons -->

</div>
    </div><!-- /.widget -->
</div></div>
<div class="col-md-3">
<div class="link-widget">
<div class="widget">
        <h3>Location </h3>
        <div class="address">
                  <p><strong>Address</strong>
                  X-17, F Block, Pocket X, Okhla Phase II, Okhla Industrial Area, New Delhi, Delhi 110020</p>
<p><strong>Phone:</strong> 098717 16364</p>
                </div>
    </div>
    <div class="widget">
        <h3></h3>
        <div class="payment-methods ">
               <!--     <ul>
                        <li><img alt="" src="assets/images/payments/payment-visa.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-master.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-paypal.png"></li>
                        <li><img alt="" src="assets/images/payments/payment-skrill.png"></li>
                    </ul>-->               </div>
         <p>Copyright © <?php date("Y");?> AS Phrama .All rights Reserved</p>       
    </div><!-- /.widget -->
</div>
</div>
<!-- /.link-widget -->
<!-- ============================================================= LINKS FOOTER : END ============================================================= -->            </div>
        </div><!-- /.container -->
    </div><!-- /.link-list-row -->

    <!-- /.copyright-bar -->

</footer><div id="notificator" class="alert"></div>   </div><!-- /.wrapper -->

    
    
    
    <script src="<?php echo base_url()?>assets/med/js/jquery-migrate-1.2.1.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script src="<?php echo base_url()?>assets/med/js/gmap3.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/css_browser_selector.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/echo.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/jquery.easing-1.3.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/bootstrap-slider.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/jquery.raty.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/jquery.prettyPhoto.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/jquery.customSelect.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/wow.min.js"></script>
    <script src="<?php echo base_url()?>assets/med/js/scripts.js"></script>
	<script src="<?php echo base_url()?>assets/med/js/system.js"></script><script src="<?php echo base_url()?>assets/med/js/all.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script><script type="text/javascript">
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get('<?php echo base_url()?>home/ajaxpro', { query: query }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });
</script>

</body>
</html>
<?php if ($this->session->flashdata('emailAdded')) { ?>
    <script>
        $(document).ready(function () {
            ShowNotificator('alert-info', '<?= lang('email_added') ?>'); 
			
        });
    </script>
    <?php
}
//echo $addedJs;
?>
</div>
</div>


<script src="<?= base_url('assets/js/bootstrap-confirmation.min.js') ?>"></script>
<script src="<?= base_url('assets/bootstrap-select-1.12.1/js/bootstrap-select.min.js') ?>"></script>
<script src="<?= base_url('assets/js/placeholders.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap-datepicker.min.js') ?>"></script>
<script>
var variable = {
    clearShoppingCartUrl: "<?= base_url('clearShoppingCart') ?>",
    manageShoppingCartUrl: "<?= base_url('manageShoppingCart') ?>",
    discountCodeChecker: "<?= base_url('discountCodeChecker') ?>"
};
</script>
<script src="<?php echo base_url()?>assets/med/js/anitabs.js" ></script>
<script src="<?php echo base_url()?>assets/med/dist/js/bootstrap-imageupload.js"></script>
<script>
            var $imageupload = $('.imageupload');
            $imageupload.imageupload();

            $('#imageupload-disable').on('click', function() {
                $imageupload.imageupload('disable');
                $(this).blur();
            })

            $('#imageupload-enable').on('click', function() {
                $imageupload.imageupload('enable');
                $(this).blur();
            })

            $('#imageupload-reset').on('click', function() {
                $imageupload.imageupload('reset');
                $(this).blur();
            });
        </script>

<style>.sidebar .widget .bordered {
    border: 2px solid #ffffff !important;
    border-radius: 10px;
    
}h2 {
    color: #000;
}.sidebar .widget .category-filter ul li {
    position: relative;
    border-bottom: .5px solid #f7e7e7;
    padding: 10px;
}.caa{background-color: #419cd2;}

.caa a{color: #fff;}
</style><script>
	jQuery(document).ready(function() {
			
	jQuery(".typeahead ").change(function() { 
    	var searchtext = $("#searchtext").val();

	window.location="<?php echo base_url();?>shop?category=&in_stock=&search_in_title="+searchtext+"&order_new=&order_price=&order_procurement=&brand_id=&quantity_more=&added_after=&added_before=&search_in_body=&price_from=&price_to=";
	});
	});
	</script> 
<script src="<?= base_url('templatejs/mine.js') ?>"></script>

</body>
</html>
<footer>