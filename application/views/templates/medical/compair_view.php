<?php  $this->load->view('templates/medical/top_head'); ?>

<div class="comparison">

  <table>
  <?php if(!empty($compair_list_items)){?>
    <thead>
      <tr>
        <th class="tl tl2"></th>
        <th class="qbse">
         Product
        </th>
        <th colspan="2" class="qbo">          
        </th>
      </tr>
	  
      <tr><?php $i=1;
	  $medicine_type ="";
	  $d ="";
	  $sd =""; 
	  $gn ="";
	  $u =""; 
	  $mc ="";
	  $ids ='';
	  foreach($compair_list_items as $product){?>
	  <?php if($i=="1"){?>
        <th class="t1l"></th>
	  <?php }?>
        <th class="compare-heading">
          <img alt="" src="<?php echo base_url()?>assets/med/images/blank.gif" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
		  <a href="<?php echo $product['url']; ?>"><?php echo $product['title']; ?>
		  
		  
		  </a></br>
		  
   <?php echo Rs .number_format($product['price'],2); ?></br>
		  <?php $medicine_type[] = array('md'=>$product['medicine_type']);
		  $gn[] = array('gn'=>$product['genric_name']);$d[] = array('d'=>$product['description']);
		  $sd[] = array('sd'=>$product['basic_description']);
		  $u[] = array('u'=>$product['unit']);
		  $mc[] = array('mc'=>$product['medicinec_category']);
		  $ids[] = array('i'=>$product['c_id']);
             
			 ?>
        </th>
		
        <?php $i++;}?>
      </tr>
	  <?php //print_r($medicine_type);?>
      </thead>
    <tbody>
      <tr>
        <td></td>
        <td colspan="4">Product</td>
      </tr>
      <tr class="compare-row">
        <td>Medicine Type</td>
       
		<?php foreach($medicine_type as $md){?>
        <td><?php echo $md["md"];?></td>
		<?php } ?>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td colspan="4"></td>
      </tr>
      <tr class="compare-row">
        <td>Short Description</td>
        <?php foreach($sd as $d){?>
        <td><?php echo $d["sd"];?></td>
		<?php } ?>
       
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="4">Description</td>
      </tr>
      <tr>
        <td>Genric Name</td>
        <?php foreach($gn as $g){?>
        <td><?php echo $g["gn"];?></td>
		<?php } ?>
       
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="4">Works on PC, Mac & mobile</td>
      </tr>
      <tr class="compare-row">
        <td>Unit</td>
        <?php foreach($u as $md){?>
        <td><?php echo $md["u"];?></td>
		<?php } ?>
       
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="4">Create invoices & estimates</td>
      </tr>
      <tr>
        <td>Medicinec Category</td>
        
        <?php foreach($mc as $m){?>
        <td><?php echo $m["mc"];?></td>
		<?php } ?>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="4">Manage VAT</td>
      </tr>
	  <tr>
        <td></td>
        
        <?php foreach($ids as $id){?>
        <td><a href="<?php echo base_url();?>delcpmitem/<?php echo $id["i"];?>">remove</a></td>
		<?php } ?>
      </tr>
	  
   </tbody><?php }else{ ?>
   
	  <tr><th>No Item In Compare List</th></tr>
	  <tr>
      
        <th class="qbse">
         No Item In Compare List
        </th>
        
      </tr>
	  <?php }?>
  </table>

</div>
<style>.comparison {
  max-width:940px;
  margin:0 auto;
  font:13px/1.4 "Helvetica Neue",Helvetica,Arial,sans-serif;
  text-align:center;
  padding:10px;
}

.comparison table {
  width:100%;
  border-collapse: collapse;
  border-spacing: 0;
  table-layout: fixed;
  border-bottom:1px solid #CCC;
}

.comparison td, .comparison th {
  border-right:1px solid #CCC;
  empty-cells: show;
  padding:10px;
}

.compare-heading {
  font-size:13px;
  font-weight:bold !important;
  border-bottom:0 !important;
  padding-top:10px !important;
}

.comparison tbody tr:nth-child(odd) {
  display:none;
}

.comparison .compare-row {
  background:#F5F5F5;
}

.comparison .tickblue {
  color:#0078C1;
}

.comparison .tickgreen {
  color:#009E2C;
}

.comparison th {    text-align: center;
  font-weight:normal;
  padding:0;
  border-bottom:1px solid #CCC;
}

.comparison tr td:first-child {
  text-align:left;
}
  
.comparison .qbse, .comparison .qbo, .comparison .tl {
  color:#FFF;
  padding:10px;
  font-size:13px;
  border-right:1px solid #CCC;
  border-bottom:0;
}

.comparison .tl2 {
  border-right:0;
}

.comparison .qbse {
  background:#0078C1;
  border-top-left-radius: 3px;
  border-left:0px;
}

.comparison .qbo {
  background:#009E2C;
  border-top-right-radius: 3px;
  border-right:0px;
}

.comparison .price-info {
  padding:5px 15px 15px 15px;
}

.comparison .price-was {
  color:#999;
  text-decoration: line-through;
}

.comparison .price-now, .comparison .price-now span {
  color:#ff5406;
}

.comparison .price-now span {
  font-size:32px;
}

.comparison .price-small {
    font-size: 18px !important;
    position: relative;
    top: -11px;
    left: 2px;
}

.comparison .price-buy {
  background:#ff5406;
  padding:10px 20px;
  font-size:12px;
  display:inline-block;
  color:#FFF;
  text-decoration:none;
  border-radius:3px;
  text-transform:uppercase;
  margin:5px 0 10px 0;
}

.comparison .price-try {
  font-size:12px;
}

.comparison .price-try a {
  color:#202020;
}

@media (max-width: 767px) {
  .comparison td:first-child, .comparison th:first-child {
    display: none;
  }
  .comparison tbody tr:nth-child(odd) {
    display:table-row;
    background:#F7F7F7;
  }
  .comparison .row {
    background:#FFF;
  }
  .comparison td, .comparison th {
    border:1px solid #CCC;
  }
  .price-info {
  border-top:0 !important;
  
}
  
}

@media (max-width: 639px) {
  .comparison .price-buy {
    padding:5px 10px;
  }
  .comparison td, .comparison th {
    padding:10px 5px;
  }
  .comparison .hide-mobile {
    display:none;
  }
  .comparison .price-now span {
  font-size:16px;
}

.comparison .price-small {
    font-size: 16px !important;
    top: 0;
    left: 0;
}
  .comparison .qbse, .comparison .qbo {
    font-size:12px;
    padding:10px 5px;
  }
  .comparison .price-buy {
    margin-top:10px;
  }
  .compare-heading {
  font-size:13px;
}
}</style>