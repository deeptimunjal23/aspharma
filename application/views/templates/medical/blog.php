<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php  $this->load->view('templates/medical/top_head'); ?>
<style>
.add-to-cart img {
    display: none;
}
#small_carousel a.right.carousel-control {
    height: auto;
    background: none;
    line-height: 20px;
    top: -44px;
    bottom: auto;
    font-size: 5px;
    padding: 0px;
	 opacity: 1;
}
#small_carousel a.left.carousel-control {
    line-height: normal;
    top: -44px;
    bottom: auto;
    font-size: 5px;
    background: none;
    padding: 0;
    right: 26px !important;
    left: auto !important;
    opacity: 1;
}
#small_carousel .product-list.item .inner a.add-to-cart.btn-add.more-blue {
    background: #8cc652;
    color: #fff;
    padding: 6px 20px;
    border-radius: 4px;
	display: inline-block;
}
ol.carousel-indicators {
    display: none;
}
.product-list.item h2 {
    margin-top: 10px;
}
a.info-btn.gradient-color {
    display: none;
}
#small_carousel .product-list.item .inner a.add-to-cart.btn-add {
    display: none;
}
#small_carousel .product-list.item .inner {
    padding: 20px 15px 30px;
    background: #f2f2f2;
    margin-bottom: 20px;
    text-align: center;
    line-height: 30px;
}
.filter-sidebar .title {
    background: #8cc652;
    font-size: 20px;
    padding: 8px 20px;
    margin: 10px 0px;
    color: #fff;
}
div#search-input-blog {
    background: #f2f2f2;
    padding: 10px 11px;
    margin-top: 20px;
    position: relative;
}
div#search-input-blog button.btn.btn-danger {
    background: #8cc652;
    position: absolute;
    right: 0;
    z-index: 2;
    border-color: #8cc652;
}
div#search-input-blog button.btn.btn-danger span.glyphicon.glyphicon-search::before {
    content: "\f002";
    font-family: fontawesome;
}
.blog-home-left-categ {
    display: inline-flex;
    margin-top: 20px;
}
div#search-input-blog input.search-query.form-control {
    border: 1px solid #ccc;
    border-radius: 0px;
}
div#latest-blog .alone.title h3 {
    margin: 20px 0px;
    text-transform: uppercase;
}
#latest-blog .caption p.description {
    margin: 10px 0px;
    line-height: 26px;
}
#latest-blog .caption {
    padding: 20px 20px;
}
</style>
<div class="container" id="blog">
    <div class="row eqHeight">
        <div class="col-sm-4 col-md-3">
            <div class="blog-home-left-categ">
                <?= $archives ?>
            </div>
            <div id="search-input-blog">
                <div class="input-group col-md-12">
                    <form method="GET" action="">
                        <input type="text" class="search-query form-control" value="<?= isset($_GET['find']) ? $_GET['find'] : '' ?>" name="find" placeholder="<?= lang('search') ?>" />
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </form>
                </div>
            </div>
            <div class="filter-sidebar">
                <div class="title">
                    <span><?= lang('best_sellers') ?></span>
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                </div>
                <?= $load::getProducts($bestSellers, '', true) ?>
            </div>
        </div>
        <div id="latest-blog" class="col-sm-8 col-md-9">
            <div class="alone title">
                <h3><?= lang('latest_blog') ?></h3>
            </div>
            <div class="row">
                <?php
                if (!empty($posts)) {
                    foreach ($posts as $post) {
                        ?>
                        <div class="col-sm-6 blog-col">
                            <div class="thumbnail blog-list">
                                <a href="<?= LANG_URL . '/blog/' . $post['url'] ?>" class="img-container">
                                    <img src="<?= base_url('attachments/blog_images/' . $post['image']) ?>" alt="<?= $post['title'] ?>">
                                </a>
                                <div class="caption">
                                    <h5>
                                        <?= character_limiter($post['title'], 85) ?>
                                    </h5>
                                    <small>
                                        <span>
                                            <i class="fa fa-clock-o"></i>
                                            <?= date('M d, y', $post['time']) ?>
                                        </span>
                                    </small>
                                    <p class="description"><?= character_limiter(strip_tags($post['description']), 200) ?></p>
                                    <a class="btn btn-blog pull-right" href="<?= LANG_URL . '/blog/' . $post['url'] ?>">
                                        <i class="fa fa-long-arrow-right"></i>
                                        <?= lang('read_mode') ?>
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="alert alert-info"><?= lang('no_posts') ?></div>
                <?php } ?>
            </div>
            <?= $links_pagination ?>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    //Event for pushed the video
    $('#small_carousel').carousel({
        pause: true,
        interval: false
    });
});
</script>