<?php  $this->load->view('templates/medical/top_head'); ?>
<div class="container">
		<div class="row">
			
			<div class="col-md-8">
				<section class="section leave-a-message">
					<h2 class="bordered">Track Order </h2>
					<p>Maecenas dolor elit, semper a sem sed, pulvinar molestie lacus. Aliquam dignissim, elit non mattis ultrices, neque odio ultricies tellus, eu porttitor nisl ipsum eu massa.</p>
					<form id="contact-form" class="contact-form cf-style-1 inner-top-xs" method="post" action="<?php echo base_url()?>track-order">
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                        <div class="field-row">
                            <label>Order Id</label>
                            <input type="text" class="le-input" name="order_id">
                        </div>
						<div class="field-row">
                            <label>Email Address</label>
                            <input type="text" class="le-input" name="email">
                        </div><!-- /.field-row -->


                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge" name="track">Track</button>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.contact-form -->
					<?php if(!empty($data["my_order_status"])){?>
					<h2 class="bordered">Tracking Information </h2>
					<p>Your Order Status is: <?php echo $data["my_order_status"]["order_status"];?><a>click here to see more detail</a></p>
					
					<?php } ?></section><!-- /.leave-a-message -->
			</div><!-- /.col -->

			<div class="col-md-4">
				<section class="our-store section inner-left-xs">
					
				</section><!-- /.our-store -->
			</div><!-- /.col -->

		</div><!-- /.row -->
	</div>