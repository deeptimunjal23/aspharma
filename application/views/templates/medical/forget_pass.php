<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-6">
			<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-danger" style="text-align:center">  
	<?php echo $this->session->flashdata('message');?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered">Forgot Password?</h2>
	<form role="form" class="login-form cf-style-1" method="POST" action="<?php echo base_url()?>forget-pass">
						<div class="field-row">
                            <label>Email</label>
                            <input type="text" class="le-input" placeholder="Enter Email" name="email"value="<?php if(isset($_POST['name'])){echo$_POST['email'];} ?>">
                        </div><!-- /.field-row -->
 <div class="buttons-holder">
                <input type="submit" name="forget" class="le-button huge login loginmodal-submit" value="forget">
					   </div>
					</form>

				</section>
			</div>

			<div class="col-md-6">
				<section class="section register inner-left-xs">
					<h2 class="bordered">Already Have  Account</h2>
					<p></p>

					<form role="form" class="register-form cf-style-1">
						
                        <div class="buttons-holder">
                            <a href="<?php echo base_url()?>login" class="le-button huge">Sign In</a>
                        </div><!-- /.buttons-holder -->
					</form>

					<h2 class="semi-bold">Sign In </h2>

					<ul class="list-unstyled list-benefits">
						<li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
						<li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
						<li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main><!-- /.authentication -->
