<nav id="top-megamenu-nav" class="megamenu-vertical animate-dropdown">
    <div class="container">
        <div class="yamm navbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mc-horizontal-menu-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.navbar-header -->
            <div class="collapse navbar-collapse" id="mc-horizontal-menu-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="<?php echo base_url()?>online-medicine-order" class="dropdown-toggle" >Order Medicine</a>
                        
                    </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Medicine By Brands</a>
                        <ul class="dropdown-menu">
                            <li><div class="yamm-content">
    <div class="row">
       <div class="col-xs-12 col-sm-4">
          
            <ul>
			 <?php $i=1; 
				$brandss =	$this->Brands_model->getBrands();	 
						 foreach($brandss as $brand){?>
						 <?php if($i <= 10 && $i>=1){ ?>
                <li><a href="<?php echo base_url(); ?>medicines-by-brands/<?php echo '1';  ?>?&&product_order=new&price_range=&order_quantity=&brands_id=<?php echo $brand['id'];?>"><?php echo $brand['name'];  ?></a></li>
						 <?php $i++;}
						 }?>
            </ul>
			
        </div>

        <div class="col-xs-12 col-sm-4">           
            <ul><?php 
			foreach($brandss as $brc){?>
						 <?php if($brc['id'] < 25 && $brc['id'] >=11){ ?>
                <li><a href="<?php echo base_url(); ?>medicines-by-brands/<?php echo '1';  ?>?&&product_order=new&price_range=&order_quantity=&brands_id=<?php echo $brc['id'];?>"><?php echo $brc['name'];  ?></a></li>
                <?php }
						 }?>
            </ul>
        </div><!-- /.col -->

        <div class="col-xs-12 col-sm-4">
           
            <ul><?php 
			foreach($brandss as $brand){?>
				<?php if($brand["id"]>=25 && $brand["id"] <= 50){ ?>
                <li><a href="<?php echo base_url(); ?>medicines-by-brands/<?php echo '1';  ?>?&&product_order=new&price_range=&order_quantity=&brands_id=<?php echo $brand['id'];?>"><?php echo $brand['name'];  ?></a></li>
               <?php }
						 }?>
            </ul>
        </div>
    </div>
</div></li>
                        </ul>
                    </li>      
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Medicine by Disease</a>
						   <ul class="dropdown-menu">
                           
<?php $diseases = $this->Brands_model->get_disease();     //print_r($diseases);      
            foreach($diseases as $d){?>
						   <li><a href="<?php echo base_url(); ?>medicines-by-disease/<?php echo $d["id"];?>"><?php echo $d["name"];?></a></li>
<?php } ?>
                            
                        </ul>
                    
                     </li>
                    
                    <li class="dropdown">
                        <a href="<?php echo base_url()?>blog" class="dropdown-toggle">Health Library (Blog)</a>
                    </li>
                    <li class="dropdown navbar-right hidden-md last-tab">
                        <a href="<?php echo base_url(); ?>upload-prescription" class="dropdown-toggle" > <i class="fa fa-cloud-download" aria-hidden="true"></i>Upload Prescription</a>
                        
                    </li>
                </ul><!-- /.navbar-nav -->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.navbar -->
    </div><!-- /.container -->
</nav>
    
</header>


<style>
.product-grid-holder .product-item-holder.size-small .image {
    padding: 20px 0px;
    overflow: hidden;
}
.product-item-holder .image {
    overflow: hidden;
}
#product-list .product-item:hover {
    border: none;
}
#product-list .product-item .carousel-item.product-item-holder.size-small.hover.clearfix {
    border: 1px solid#eee;
    background: #f2f2f2;
}
#product-list .product-item .carousel-item.product-item-holder.size-small.hover.clearfix:hover {
    border-color: #34ade2;
}
#top-brands .title-nav h1 {
    background: none;
}
</style>