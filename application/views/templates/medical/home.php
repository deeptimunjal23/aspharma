<?php  $this->load->view('templates/medical/top_head'); ?>
<?php  $this->load->view('templates/medical/slider'); ?>

<section id="banner-holder" class="wow fadeInUp">
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-lg-4 banner">
		<div class="cmsms_column one_third">
<div id="cmsms_fb_5cd5b000211110_60925545" class="cmsms_featured_block">
<div class="featured_block_inner">
<div class="featured_block_text">
<h1>
<div id="cmsms_icon_5cd5b0002115a5_15508079" class="cmsms_icon_wrap"><i class="fa fa-gift" aria-hidden="true"style="color:#fff;"></i></div>
<span style="color: #ffffff;">Beneficial Offers</span></h1>
<p><span style="color: #ffffff;">Pharmacy is the science and technique of preparing and dispensing drugs.</span></p>
</div>
</div>
</div>
</div>

        </div>
        <div class="col-xs-12 col-lg-4 banner">
            <div class="cmsms_column one_third">
<div id="cmsms_fb_5cd5b00020f4d6_77726902" class="cmsms_featured_block">
<div class="featured_block_inner">
<div class="featured_block_text">
<h1>
<div id="cmsms_icon_5cd5b00020fd42_47289698" class="cmsms_icon_wrap"><i class="fa fa-truck" aria-hidden="true"style="color:#fff;"></i></div>
<span style="color: #ffffff;">Free Delivery</span></h1>
<p><span style="color: #ffffff;">Pharmacy is the science and technique of preparing and dispensing drugs.</span></p>
</div>
</div>
</div>
</div>

        </div>
        <div class="col-xs-12 col-lg-4 banner">
         <div class="cmsms_column one_third">
<div id="cmsms_fb_5cd5b000210562_76531912" class="cmsms_featured_block">
<div class="featured_block_inner">
<div class="featured_block_text">
<h1>
<div id="cmsms_icon_5cd5b000210a01_47149682" class="cmsms_icon_wrap"><i class="fa fa-shopping-cart" aria-hidden="true" style="color: #ffffff;"></i>  </div>
<span style="color: #ffffff;">Easy Purchases</span></h1>
<p><span style="color: #ffffff;">Pharmacy is the science and technique of preparing and dispensing drugs.</span></p>
</div>
</div>
</div>
</div>   
        </div>
        </div>
            </div><!-- /.container -->
</section><!-- /#banner-holder -->
<?php  $this->load->view('templates/medical/product_tabs'); ?>

<section id="category-box">
	<div class="container">
    	<div class="row">
        <h3>TOP BRANDS</h3>
        <div class="col-md-3 text-center">
        	<div class="grid-cate">
            	<div class="img">
				<a href="<?php echo base_url();?>medicines-by-brands/8">
                <img src="<?php echo base_url();?>assets/med/images/bicon.jpg" class="image-responsive"></a>
                </div>
                <h4>Bicon</h4>
            </div>
        </div>
        <div class="col-md-3 text-center">
        	<div class="grid-cate">
            	<div class="img"><a href="<?php echo base_url();?>medicines-by-brands/7">
                <img src="<?php echo base_url();?>assets/med/images/cipla.jpg" class="image-responsive"></a>
                </div>
                <h4>Cipla</h4>
            </div>
        </div>
        <div class="col-md-3 text-center">
        	<div class="grid-cate">
            	<div class="img"><a href="<?php echo base_url();?>medicines-by-brands/10">
                <img src="<?php echo base_url();?>assets/med/images/fe.jpg" class="image-responsive"></a>
                </div>
                <h4>FERRING</h4>
            </div>
        </div>
        <div class="col-md-3 text-center">
        	<div class="grid-cate">
            	<div class="img">
				<a href="<?php echo base_url();?>medicines-by-brands/15">
                <img src="<?php echo base_url();?>assets/med/images/hito.jpg" class="image-responsive"></a>
                </div>
                <h4>HETERO</h4>
            </div>
        </div>
        </div>
    </div>
</section>
<?php  $this->load->view('templates/medical/feature_products'); ?>
<section id="product-list">
	<div class="container">
    	<div class="row">
        <div class="col-md-12">
        <div class="title-nav"> <a style="float: right;margin-top: 19px;
    margin-right: 10px;" href="<?php echo base_url(); ?>medicines-by-brands/<?php echo '1';  ?>?&&product_order=new&price_range=&order_quantity=&brands_id=" data-target="#owl-recently-viewed" class="">View All</a> 
                <h1>PRODUCTS Listing</h1>
				 
            </div>
			
            </div>
			<?php $product_listing = $this->Products_model->listing_pro();  
             				foreach($product_listing as $product_listing){	  ?>
			   <div class="col-md-3">
                <div class="product-item" style="margin-bottom:20px;">
        <div class="carousel-item product-item-holder size-small hover clearfix">
                
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product_listing['image']; ?>">
                                </div>
                                <div class="body">
                                    <div class="label-discount clear"></div>
                                  
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $product_listing['url'] ?>"><?php echo $product_listing['title']; ?></a>
                                    </div>
                                </div>
                                 <div class="prices">
                                    <div class="price-prev"><?php echo Rs. number_format($product_listing['old_price'],2);?></div>
                                    <div class="price-current pull-right"><?php echo Rs. number_format($product_listing['price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
                                        
										<a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product_listing['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                    </div>
									
                                    <div class="wish-compare">
                                         <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $product_listing['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $product_listing['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div>
                    <!-- /.product-item -->
                    </div>
                    </div>
    
	<?php }?>
		</div>
            </div>

</section>

<section id="top-brands" class="wow fadeInUp">
    <div class="container">
        <div class="carousel-holder" >
            
            <div class="title-nav">
                <h1>Recent News</h1>
                <div class="nav-holder">
                    <a href="#prev" data-target="#owl-brands" class="slider-prev btn-prev fa fa-angle-left"></a>
                    <a href="#next" data-target="#owl-brands" class="slider-next btn-next fa fa-angle-right"></a>
                </div>
            </div><!-- /.title-nav -->
            
            <div class="row">
            
            <div id="owl-brands" class="owl-carousel brands-carousel">
                <?php 
			//	print_r($lastBlogs);
				foreach($lastBlogs as $blog){?>
                <div class="carousel-item">
                   <div class="blog-grid">
                   	
					<img src="<?php echo base_url()?>attachments/blog_images/<?php echo $blog['image'];?>">
                    <div class="text-1">
                    <h3><?php echo $blog['title'];?></h3>
                    <p><?php 
					$limit = 180;
$summary = $blog['description'];
					
					if (strlen($summary) > $limit)
      $summary = substr($summary, 0, strrpos(substr($summary, 0, $limit), ' ')) . '...';
      echo $summary;?></p>
                    <div class="readmore">
                    <a href="<?php echo base_url().'blog/'.$blog['url'];?>">Read More</a>
                    </div>
                    </div>
                   </div>
                </div>
               <?php } ?>
		   </div><!-- /.brands-caresoul -->
           </div>

        </div><!-- /.carousel-holder -->
    </div><!-- /.container -->
</section><!-- /#top-brands --><style type="text/css"> 
#cmsms_row_5cd5b00020e5f2_68844268 .cmsms_row_outer_parent { 
	padding-top: 50px; 
} 

#cmsms_row_5cd5b00020e5f2_68844268 .cmsms_row_outer_parent { 
	padding-bottom: 50px; 
} 

 

#cmsms_fb_5cd5b00020f4d6_77726902 { 
	padding-top:15px; 
	padding-bottom:25px; 
	-webkit-border-radius:3%; 
	border-radius:3%; 
	background-color:#80dcdf;
} 

#cmsms_fb_5cd5b00020f4d6_77726902 .featured_block_inner { 
	width: 100%; 
	padding: ; 
	text-align: center; 
	margin:0 auto; 
} 

#cmsms_fb_5cd5b00020f4d6_77726902 .featured_block_text { 
	text-align: center; 
} 

 
#cmsms_icon_5cd5b00020fd42_47289698 { 
	display:inline; 
	text-align:center; } 

#cmsms_icon_5cd5b00020fd42_47289698 .cmsms_simple_icon { 
	border-width:0px; 
	width:40px; 
	height:40px; 
	font-size:30px; 
	line-height:40px; 
	text-align:center; 
	-webkit-border-radius:50%; 
	border-radius:50%; 
	color:#ffffff;} 
 

#cmsms_fb_5cd5b000210562_76531912 { 
	padding-top:15px; 
	padding-bottom:25px; 
	-webkit-border-radius:3%; 
	border-radius:3%; 
	background-color:#ec7da2;
} 

#cmsms_fb_5cd5b000210562_76531912 .featured_block_inner { 
	width: 100%; 
	padding: ; 
	text-align: center; 
	margin:0 auto; 
} 

#cmsms_fb_5cd5b000210562_76531912 .featured_block_text { 
	text-align: center; 
} 

 
#cmsms_icon_5cd5b000210a01_47149682 { 
	display:inline; 
	text-align:center; } 

#cmsms_icon_5cd5b000210a01_47149682 .cmsms_simple_icon { 
	border-width:0px; 
	width:40px; 
	height:40px; 
	font-size:30px; 
	line-height:40px; 
	text-align:center; 
	-webkit-border-radius:50%; 
	border-radius:50%; 
	color:#ffffff;} 
 

#cmsms_fb_5cd5b000211110_60925545 { 
	padding-top:15px; 
	padding-bottom:25px; 
	-webkit-border-radius:3%; 
	border-radius:3%; 
	background-color:#70dbbb;
} 

#cmsms_fb_5cd5b000211110_60925545 .featured_block_inner { 
	width: 100%; 
	padding: ; 
	text-align: center; 
	margin:0 auto; 
} 

#cmsms_fb_5cd5b000211110_60925545 .featured_block_text { 
	text-align: center; 
} 

 
#cmsms_icon_5cd5b0002115a5_15508079 { 
	display:inline; 
	text-align:center; } 

#cmsms_icon_5cd5b0002115a5_15508079 .cmsms_simple_icon { 
	border-width:0px; 
	width:40px; 
	height:40px; 
	font-size:30px; 
	line-height:40px; 
	text-align:center; 
	-webkit-border-radius:50%; 
	border-radius:50%; 
	color:#ffffff;} 
 
#cmsms_row_5cd5b0002118d7_68028920 .cmsms_row_outer_parent { 
	padding-top: 0px; 
} 

#cmsms_row_5cd5b0002118d7_68028920 .cmsms_row_outer_parent { 
	padding-bottom: 15px; 
} 

 
#cmsms_heading_5cd5b000212743_84967349 { 
	text-align:left; 
	margin-top:15px; 
	margin-bottom:15px; 
} 

#cmsms_heading_5cd5b000212743_84967349 .cmsms_heading { 
	text-align:left; 
} 

#cmsms_heading_5cd5b000212743_84967349 .cmsms_heading, #cmsms_heading_5cd5b000212743_84967349 .cmsms_heading a { 
	font-weight:bold; 
	font-style:normal; 
} 

#cmsms_heading_5cd5b000212743_84967349 .cmsms_heading_divider { 
} 


#cmsms_button_5cd5b000212b83_91636926 { 
	float:left; 
} 

#cmsms_button_5cd5b000212b83_91636926 .cmsms_button:before { 
	margin-right:.5em; 
	margin-left:0; 
	vertical-align:baseline; 
} 

#cmsms_button_5cd5b000212b83_91636926 .cmsms_button { 
	font-weight:normal; 
	font-style:normal; 
	border-style:solid; 
} 
#cmsms_button_5cd5b000212b83_91636926 .cmsms_button:hover { 
} 
 
#cmsms_row_5cd5b00024bdf0_23719115 .cmsms_row_outer_parent { 
	padding-top: 15px; 
} 

#cmsms_row_5cd5b00024bdf0_23719115 .cmsms_row_outer_parent { 
	padding-bottom: 25px; 
} 

 
#cmsms_divider_5cd5b00024d336_05867320 { 
	border-bottom-width:1px; 
	border-bottom-style:solid; 
	padding-top:0px; 
	margin-bottom:0px; 
} 
 
#cmsms_row_5cd5b00024d613_85333596 .cmsms_row_outer_parent { 
	padding-top: 0px; 
} 

#cmsms_row_5cd5b00024d613_85333596 .cmsms_row_outer_parent { 
	padding-bottom: 15px; 
} 

 
#cmsms_heading_5cd5b00024e0a9_96300034 { 
	text-align:left; 
	margin-top:15px; 
	margin-bottom:15px; 
} 

#cmsms_heading_5cd5b00024e0a9_96300034 .cmsms_heading { 
	text-align:left; 
} 

#cmsms_heading_5cd5b00024e0a9_96300034 .cmsms_heading, #cmsms_heading_5cd5b00024e0a9_96300034 .cmsms_heading a { 
	font-weight:bold; 
	font-style:normal; 
} 

#cmsms_heading_5cd5b00024e0a9_96300034 .cmsms_heading_divider { 
} 


#cmsms_button_5cd5b00024e648_29086926 { 
	float:left; 
} 

#cmsms_button_5cd5b00024e648_29086926 .cmsms_button:before { 
	margin-right:.5em; 
	margin-left:0; 
	vertical-align:baseline; 
} 

#cmsms_button_5cd5b00024e648_29086926 .cmsms_button { 
	font-weight:normal; 
	font-style:normal; 
	border-style:solid; 
} 
#cmsms_button_5cd5b00024e648_29086926 .cmsms_button:hover { 
} 
 
#cmsms_row_5cd5b00027b0b2_18103177 .cmsms_row_outer_parent { 
	padding-top: 15px; 
} 

#cmsms_row_5cd5b00027b0b2_18103177 .cmsms_row_outer_parent { 
	padding-bottom: 25px; 
} 

 
#cmsms_divider_5cd5b00027b874_45281573 { 
	border-bottom-width:1px; 
	border-bottom-style:solid; 
	padding-top:0px; 
	margin-bottom:0px; 
} 
 
#cmsms_row_5cd5b00027ba54_82800634 .cmsms_row_outer_parent { 
	padding-top: 0px; 
} 

#cmsms_row_5cd5b00027ba54_82800634 .cmsms_row_outer_parent { 
	padding-bottom: 30px; 
} 

 
#cmsms_heading_5cd5b00027c187_62099906 { 
	text-align:left; 
	margin-top:15px; 
	margin-bottom:15px; 
} 

#cmsms_heading_5cd5b00027c187_62099906 .cmsms_heading { 
	text-align:left; 
} 

#cmsms_heading_5cd5b00027c187_62099906 .cmsms_heading, #cmsms_heading_5cd5b00027c187_62099906 .cmsms_heading a { 
	font-weight:bold; 
	font-style:normal; 
} 

#cmsms_heading_5cd5b00027c187_62099906 .cmsms_heading_divider { 
} 


#cmsms_button_5cd5b00027c591_65946227 { 
	float:left; 
} 

#cmsms_button_5cd5b00027c591_65946227 .cmsms_button:before { 
	margin-right:.5em; 
	margin-left:0; 
	vertical-align:baseline; 
} 

#cmsms_button_5cd5b00027c591_65946227 .cmsms_button { 
	font-weight:normal; 
	font-style:normal; 
	border-style:solid; 
} 
#cmsms_button_5cd5b00027c591_65946227 .cmsms_button:hover { 
} 
</style>