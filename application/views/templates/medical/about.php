<?php  $this->load->view('templates/medical/top_head'); ?>
<main id="about-us">
	<div class="container inner-top-xs inner-bottom-sm">

		<div class="row">
			<div class="col-xs-12 col-md-11 col-lg-11 col-sm-8">

				<section id="who-we-are" class="section m-t-0">
					<h2>Who we are</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis diam erat. Duis velit lectus, posuere a blandit sit amet, tempor at lorem. Donec ultricies, lorem sed ultrices interdum. Leo metus luctus sem, vel vulputate diam ipsum sed lorem. Donec tempor arcu nisl, et molestie massa scelerisque ut. Nunc at rutrum leo. Mauris metus mauris, tristique quis sapien eu, rutrum vulputate enim.</p>
					<p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat. Praesent varius ultrices massa at faucibus. Aenean dignissim, orci sed faucibus pharetra, dui mi dignissim tortor, sit amet condimentum mi ligula sit amet augue.</p>
				</section><!-- /#who-we-are -->

				<section id="our-goal-and-idea" class="section">
					<h2>Our Goal and Idea</h2>
					<p>Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus. Ut nec odio facilisis, ultricies nunc eget, fringilla orci. Nulla lobortis sem dapibus, aliquet turpis eu, ornare neque. Sed nec sem diam. Mauris neque purus, malesuada at velit vel, tempus congue nisl. Ut aliquam semper augue hendrerit varius. Fusce pretium tempus volutpat. Vivamus dignissim posuere aliquet. In hac habitasse platea dictumst. </p>
				</section><!-- /#our-goal-and-idea -->
			
			</div><!-- /.col -->
			</div><!-- /.row -->
		
	</div><!-- /.container -->

</main>