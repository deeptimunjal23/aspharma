<?php  $this->load->view('templates/medical/top_head'); ?>
<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-success" style="text-align:center">  
	<?php echo $this->session->flashdata('message');?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
     <div class="mapTitle showDesktop">My Account</div>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>
      
    </div><!-- /.body -->
</div><!-- /.widget -->
<?php  $this->load->view('templates/medical/side_bar_products'); ?>
          </div>
        <div class="col-sm-9" style="margin-top: 1px;">
        <div class="orderDetailsContent">
   <div class="" id="orderStatusContainer">
      <div class="mapTitle showDesktop">Account Information</div>
	  <?php $user_id = $this->session->userdata('logged_user');
        $data['userInfo'] = $this->Public_model->getUserProfileInfo($user_id);
		$single= $this->Public_model->get_single_order_new($user_id);
		$single_address= $this->Public_model->get_single_address($single["id"]);
		//print_r($single_address);
		
		?>
      <div class="row">
        <div class="col-sm-6">
         <div class="_2mTOw">
         <div class="ZeDVy">
            <div><span class="_2tYiJ" style="color:#d88606"></span><ul class="tabled-data">
                        <li>
                            <label>Name</label>
                            <div class="value">
							<?php if(!empty($data['userInfo']["name"])){echo $data['userInfo']["name"];} ?></div>
                        </li>
                        <li>
                            <label>Email Id</label>
                            <div class="value" style="text-transform: lowercase;">
							<?php echo  $data['userInfo']["email"]?></div>
                        </li>
                        <li>
                            <label>Phone</label>
                            <div class="value"><?php echo  $data['userInfo']["phone"]?></div>
                        </li>
                        
                        <li>
                            <label></label>
                            <div class="value"></div>
                        </li>
                    </ul>
			</div>
            
         </div>
         <div class="_2vCS5">
            <div class="_217or">
               <span class="_2tYiJ"style="color:#d88606"></span>
               <span class="_374y6">
				 <span class="u-boldText" style="color:green"></span>
				 <button type="button"  class="le-button" data-toggle="modal" data-target="#add-modal"><b>Edit</b></button> 
               </span>
               <div class="_26nvV"></div>
            </div>
          </div>
          </div>
          </div>
          
          <div class="col-sm-6">
          <div id="orderStatusContainer">
      <div class="_3Tg-d">
         
         <div class="_10XI8 ZKHfL">
            <div class="_3W6vY"><?php //echo ucfirst($single_item["patient_name"]);?></div>
            <div class="_365Xc"><i class="fa fa-user" aria-hidden="true"></i> <strong>Name: </strong><?php echo $single_address["first_name"]." ".$single_address["last_name"];?></div>
            <div class="_365Xc"><i class="fa fa-building-o" aria-hidden="true"></i> <strong>City:</strong> <?php echo $single_address["city"];?></div>
            <div class="_365Xc"><i class="fa fa-globe" aria-hidden="true"></i> <strong>Address:</strong><?php echo $single_address["address"];?></div> 
			<div class="_365Xc"><i class="fa fa-map-marker" aria-hidden="true"></i> <strong>Pincode:</strong> <?php echo $single_address["post_code"];?></div>
			<div class="_365Xc"> <?php echo $single_address["phone"];?></div>
         </div>
      </div>
   </div>
          </div>
      </div>
   </div>
   
   <div class="_3Tg">
     
      <div>
         <div class="_3Tg-d">
         <table class="responsive-table" width="100%;">
    <caption>Latest Orders History</caption>
    <thead>
      <tr>
        <th scope="col">Order id</th>
        <th scope="col">Date</th>
        <th scope="col">Address</th>
        <th scope="col">Phone</th>
        <th scope="col">Products</th>
        
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="7"></td>
      </tr>
    </tfoot>
    <tbody>      
	<?php $sn=1; 
	$all= $this->Public_model->get_all_order_new($user_id);
	 foreach($all  as $order){
		 $single_address= $this->Public_model->get_single_address($order["id"]);
		 if($sn <=3){?>
	
	<tr>
	  
        <th scope="row">#<?= $order['order_id'] ?></th>
        <td data-title="Released"><?= date('d.m.Y', $order['date']) ?></td>
        <td data-title="Studio"><?= $single_address['address'] ?></td>
        <td data-title="Worldwide Gross" data-type="currency"><?= $single_address['phone'] ?></td>
        <td><a class="btn btn-sucess confirm-sucess" href="<?php echo base_url()?>orders-view/<?= $order['order_id'] ?>">View Order</a></td></tr>
		 <?php } ?>            
			</tbody>
	 <?php $sn++;}?></table>
   
 </div>
</br>
<?php if(!empty($single_item['admin_extra_note'])){?>
<h3>Extra Note From Supplier</h3>
<ul class="tabled-data">
                        <li>
                        
                            <div class="value"><?php echo $single_item['admin_extra_note'];?></div>
                        </li>
						</ul>
<?php } ?>
</div>
         </div>
      </div>
   </div>
 </div>
</div>   
       </div>
    </div>
</div>
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="add-modal-label">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    	<form class="form-horizontal" id="add-form" method="POST" action="<?php echo base_url();?>users/edit_user">
			      <div class="modal-header" >
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="add-modal-label">Edit Profile</h4>
			      </div>
			      <div class="modal-body">
			        	<div class="form-group">
					    	<label for="add-firstname" class="col-sm-2 control-label">Name</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="form-control" id="add-firstname" name="firstname" placeholder="Firstname" required value="<?php if(!empty($data['userInfo']["name"])){echo $data['userInfo']["name"];} ?>">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="add-email" class="col-sm-2 control-label">E-mail</label>
					    	<div class="col-sm-10">
					      		<input type="email" class="form-control" id="add-email" name="email" placeholder="E-mail address" required readonly value="<?php if(!empty($data['userInfo']["email"])){echo $data['userInfo']["email"];} ?>">
								<input type="hidden" value="<?php echo $data['userInfo']["id"];?>" name="user_id"/>
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="add-mobile" class="col-sm-2 control-label">Mobile</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="form-control" id="add-mobile" name="mobile" placeholder="Mobile" required value="<?php if(!empty($data['userInfo']["phone"])){echo $data['userInfo']["phone"];} ?>">
					    	</div>
					  	</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <input type="submit" class="btn btn-lg btn-success le-button" name="save" value="save">
			      </div>
		      	</form>
		    </div>
		  </div>
		</div>

			
<link rel="stylesheet" href="<?php echo base_url()?>/assets/med/css/order.css">
<style>#container {position: absolute; height: 100px; width: 170px; top: 200px; left: 200px;}

.popover {top: 0; left: 0px; position: relative;}

.big_img {height: 250px; position: absolute; top: -40px; left: 180px; display:none;}

#container:hover .big_img {display:block;}</style>