<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-6">
			<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-danger" style="text-align:center">  
	<?php echo $this->session->flashdata('message');?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered">Reset Password</h2>
	<form role="form" class="login-form cf-style-1" method="POST" action="<?php echo base_url()?>reset-password/<?php echo $this->uri->segment(2);?>">
						<div class="field-row">
                            <label>Enter New Password</label>
                            <input type="password" class="le-input" placeholder="Enter New password" name="newpass"value="">
                        </div>
						<div class="field-row">
                            <label>Re-Enter New Password</label>
                            <input type="password" class="le-input" placeholder="Enter New password" name="renewpass"value="">
                        </div>
 <div class="buttons-holder">
                <input type="submit" name="resetpass" class="le-button huge login loginmodal-submit" value="resetpass">
					   </div>
					</form>

				</section>
			</div>

			<div class="col-md-6">
				<section class="section register inner-left-xs">
					<h2 class="bordered">Already Have  Account</h2>
					<p></p>

					<form role="form" class="register-form cf-style-1">
						
                        <div class="buttons-holder">
                            <a href="<?php echo base_url()?>login" class="le-button huge">Sign In</a>
                        </div><!-- /.buttons-holder -->
					</form>

					<h2 class="semi-bold">By Sign In  </h2>

					<ul class="list-unstyled list-benefits">
						<li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
						<li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
						<li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main><!-- /.authentication -->
