<?php  $this->load->view('templates/medical/top_head'); ?>

<section id="category-grid">
    <div class="container">
        <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">


   <?php  $this->load->view('templates/medical/side_bar_products'); ?> 
            <div class="widget">
	<div class="simple-banner">
		<a href="#"><img alt="" class="img-responsive" src="assets/images/blank.gif" data-echo="assets/images/banner/banner-simple.jpg" /></a>
	</div>
</div>
         </div>
        

        <div class="col-xs-12 col-sm-9 no-margin wide sidebar">  <section id="gaming">
    <div class="grid-list-products">
        <h2 class="section-title"></h2>
        
           <div class="control-bar" style="height: 52px;">
         <?php /*   <div id="popularity-sort" class="le-select" >
                <select class="filter" id="new_pro" data-placeholder="sort by popularity">
                    <option <?php if(isset($_GET["product_order"])){ if($_GET["product_order"]=="newest"){ echo "selected"; } } ?> value="newest">Newest </option>
					<option <?php if(isset($_GET["product_order"])){ if($_GET["product_order"]=="old"){ echo "selected"; } } ?> value="old">Old </option>
                    
                </select>
            </div>
            
            <div  id="item-count" class="le-select">
                <select id="price_range" class="filter">
                    <option value="">Sort by price..</option>
                    <option <?php if(isset($_GET["price_range"])){ if($_GET["price_range"]=="low"){ echo "selected"; } } ?> value="low">Low price</option>
                    <option <?php if(isset($_GET["price_range"])){ if($_GET["price_range"]=="high"){ echo "selected"; } } ?> value="high">High price </option>
                </select>
            </div>
			
			<div   class="le-select">
                <select class="filter" id="order_by">
                    <option value="">Sort Order By..</option>
                    <option <?php if(isset($_GET["order_quantity"])){ if($_GET["order_quantity"]=="high"){ echo "selected"; } } ?> value="high">High Quantity </option> 
					<option <?php if(isset($_GET["order_quantity"])){ if($_GET["order_quantity"]=="low"){ echo "selected"; } } ?> value="low">Low Quantity </option>
                </select>
            </div>
*/?>
            <div class="grid-list-buttons">
                <ul>
                    <li class="grid-list-button-item active"><a data-toggle="tab" href="#grid-view"><i class="fa fa-th-large"></i> Grid</a></li>
                    <li class="grid-list-button-item "><a data-toggle="tab" href="#list-view"><i class="fa fa-th-list"></i> List</a></li>
                </ul>
            </div>
        </div><!-- /.control-bar -->
                                
       
		
       <div class="tab-content">
            <div id="grid-view" class="products-grid fade tab-pane in active">
                
                <div class="product-grid-holder">
                    <div class="row no-margin">
                       <?php 
					   if (!empty($products)) {
					   foreach($products as $product){?> 
                        <div class="col-xs-12 col-sm-4 no-margin product-item-holder">
                          
						   <div class="product-item">
                                 
                                <div class="image">
<?php  $pro_img=base_url()."/attachments/shop_images/".$product['image'];?>
<?php if(!file_exists($pro_img)){?>

                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                <?php }else{?><img src="<?php echo base_url("/assets/med/images/noimage.jpg");?>"/><?php } ?>
								</div>
                                <div class="body">
                                    <div class="label-discount green">
									<?php
$oldFigure = $product['price'];
 $newFigure = $product['old_price'];
 
 $percentChange = (1 - $oldFigure / $newFigure) * 100;
 
 
$total_off= round($percentChange);
									if($total_off >0){
									 echo "-" .round($percentChange) . "%"." sale";
									}?></div>
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $product['url'] ?>"><?php echo $product['title']; ?></a>
                                    </div>
									<?php 
									//$b=$this->Brands_model->get_brand_name($product['brand_id'])?>
                                    <div class="brand"><?php //echo $b['name'];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev price"><?php  echo Rs. number_format($product['old_price'],2);?></div>
                                    <div class="price-current pull-right"><?php  echo Rs. number_format($product['price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
									
                                        
 
                        <?php if($product['quantity']
						>1){?>
                        <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
						<?php }else{ ?>
						<span style="color:red">Out Of stock</span>
						<?php }?>
						</div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $product['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $product['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div><!-- /.product-item -->
						   
						</div>
						
<?php }  } else { ?>
                <script>
                    jQuery(document).ready(function () {
                        ShowNotificator('alert-info', '<?= lang('no_results') ?>');
                    });
                </script>
                <?php
            }
            ?>
                        

                     </div>
                </div>
                
             </div>
<?php if ($links_pagination != '') { ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= $links_pagination ?>
            </div>
        </div>
    <?php } ?>
            <div id="list-view" class="products-grid fade tab-pane ">
                <div class="products-list">
                   <?php foreach($products as $product){?>
                    <div class="product-item product-item-holder">
                        <div class="ribbon red"><span>sale</span></div> 
                        <div class="ribbon blue"><span>new!</span></div>
                        <div class="row">
                            <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                </div>
                            </div><!-- /.image-holder -->
                            <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                <div class="body">
                                    <div class="label-discount green"><?php
$oldFigure = $product['price'];
 $newFigure = $product['old_price'];
 
 $percentChange = (1 - $oldFigure / $newFigure) * 100;
 
 
$total_off= round($percentChange);
									if($total_off >0){
									 echo "-" .round($percentChange) . "%"." sale";
									}?></div>
                                    <div class="title">
                                        <a href="<?php echo base_url().$product['url']; ?>"><?php echo $product['title']; ?></a>
                                    </div>
                                    
                                    <div class="excerpt">
                                        <p></p>
                                    </div>
                                    <div class="addto-compare">
                                        <a class="btn-add-to-compare" href="<?php echo base_url()?>add_to_compair_list/<?php echo $product['id']?>">add to compare list</a>
                                    </div>
                                </div>
                            </div><!-- /.body-holder -->
                            <div class="no-margin col-xs-12 col-sm-3 price-area">
                                <div class="right-clmn">
                                    <div class="price-current"><?php echo Rs. number_format($product['price'],2);?></div>
                                    <div class="price-prev"><?php echo Rs. number_format($product['old_price'],2);?></div>
                                    <div class="availability"><label>availability:</label><span class="available">  in stock</span></div>
                                    <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                    <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                </div>
                            </div><!-- /.price-area -->
                        </div><!-- /.row -->
                    </div><!-- /.product-item -->

<?php  } ?>
                   </div><!-- /.products-list -->

              </div><!-- /.products-grid #list-view -->

        </div>
    </div>

</section>           
        </div>
         
    </div>
</section>
<?php //print_r($_GET);?>

<script>
jQuery(document).ready(function() {
			
	jQuery(".filter").change(function() { 
    	//var searchtext = $("#searchtext").val();
     	var searchtext=  $("#new_pro option:selected").val();
		var price_range=  $("#price_range option:selected").val();
		var seller=  $("#seller option:selected").val();
		var order_by=  $("#order_by option:selected").val();
		//alert(searchtext);
		//return false;
	window.location="<?php echo base_url();?>medicines-by-brands/"+<?php echo $this->uri->segment(2);;?>+"?&&product_order="+searchtext+"&price_range="+price_range+"&order_quantity="+order_by+"&quantity_more=&added_after=&brands_id="+<?php echo $bc;?>+"";
	});
	});
	</script> 
<?php  //$this->load->view('templates/medical/_parts/footer'); ?>