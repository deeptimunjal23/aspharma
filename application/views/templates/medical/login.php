<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
		
			
			<div class="col-md-6">
			<?php
  if($this->session->flashdata('message')){?>
  <div class="alert alert-success" style="text-align:center">  
	<?php echo $this->session->flashdata('message');?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
			<?php
  if($this->session->flashdata('userError')){?>
  <div class="alert alert-danger" style="text-align:center">      
    <?php echo $this->session->flashdata('userError')?>
	<button data-dismiss="alert" class="close" type="button" style="margin-right: 14px;">×</button>
  </div>
<?php } ?>
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered">Sign In</h2>
					<p>Hello, Welcome to your account</p>

					<form role="form" class="login-form cf-style-1" action="<?php echo base_url()?>login" method="POST"  >
						<div class="field-row">
                            <label>Email</label>
                            <input type="email" class="le-input" placeholder="Enter Email" name="email">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Password</label>
                            <input type="password" class="le-input" placeholder="password" name="pass">
                        </div><!-- /.field-row -->

                        <div class="field-row clearfix">
                        	<span class="pull-left">
                        		<label class="content-color"><input type="checkbox" class="le-checbox auto-width inline"> <span class="bold">Remember me</span></label>
                        	</span>
                        	<span class="pull-right">
                        		<a href="<?php echo base_url()?>forget-pass" class="content-color bold">Forgotten Password ?</a>
                        	</span>
                        </div>

                        <div class="buttons-holder">
                          
                       
<input type="submit" name="login" class="le-button huge login loginmodal-submit" value="<?= lang('login') ?>">
					   </div><!-- /.buttons-holder -->
					</form><!-- /.cf-style-1 -->

				</section><!-- /.sign-in -->
			</div><!-- /.col -->

			<div class="col-md-6">
				<section class="section register inner-left-xs">
					<h2 class="bordered">Create New Account</h2>
					<p></p>

					<form role="form" class="register-form cf-style-1">
						

                        <div class="buttons-holder">
                            <a href="<?php echo base_url()?>register" class="le-button huge">Sign Up</a>
                        </div><!-- /.buttons-holder -->
					</form>

					<h2 class="semi-bold">Sign up today and you'll be able to :</h2>

					<ul class="list-unstyled list-benefits">
						<li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
						<li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
						<li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main><!-- /.authentication -->

