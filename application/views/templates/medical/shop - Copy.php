<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('templates/medical/top_head'); ?>
<link rel="stylesheet" href="<?= base_url('assets/bootstrap-select-1.12.1/bootstrap-select.min.css') ?>">
<div class="inner-nav">
    <div class="container">
        <?= lang('home') ?> <span class="active"> > <?= lang('shop') ?></span>
    </div>
</div>
<div class="container" id="shop-page">
    <div class="row">
	<div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>Product Filters</h1>
    <div class="body bordered">
        
        <div class="category-filter">
            <h2>Categories</h2>
            <hr>
            <ul> <?php //foreach($disease as $disease){?>
                <li>
				 <div class="categories">
                <div class="title">
                    <span><?//= lang('categories') ?></span>
                    <?php if (isset($_GET['category']) && $_GET['category'] != '') { ?>
                        <a href="javascript:void(0);" class="clear-filter" data-type-clear="category" data-toggle="tooltip" data-placement="right" title="<?= lang('clear_the_filter') ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                    <?php } ?>
                </div>
                <div class="body">
                    <a href="javascript:void(0)" id="show-xs-nav" class="visible-xs visible-sm">
                        <span class="show-sp"><?= lang('showXsNav') ?><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>
                        <span class="hidde-sp"><?= lang('hideXsNav') ?><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>
                    </a>
                    <div id="nav-categories">
                        <?php

                        function loop_tree($pages, $is_recursion = false)
                        {
                            ?>
                            <ul class="<?= $is_recursion === true ? 'children' : 'parent' ?>">
                                <?php
                                foreach ($pages as $page) {
                                    $children = false;
                                    if (isset($page['children']) && !empty($page['children'])) {
                                        $children = true;
                                    }
                                    ?>
                                    <li>
                                        <?php if ($children === true) {
                                            ?>
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        <?php } else { ?>
                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                        <?php } ?>
                                        <a href="javascript:void(0);" data-categorie-id="<?= $page['id'] ?>" class="go-category left-side <?= isset($_GET['category']) && $_GET['category'] == $page['id'] ? 'selected' : '' ?>">
                                            <?= $page['name'] ?>
                                        </a>
                                        <?php
                                        if ($children === true) {
                                            loop_tree($page['children'], true);
                                        } else {
                                            ?>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <?php
                            if ($is_recursion === true) {
                                ?>
                                </li>
                                <?php
                            }
                        }

                        loop_tree($home_categories);
                        ?>
                    </div>
                </div>
            </div>
            
				</li>
				
			<?php //} ?> 
            
			</ul>
        </div><!-- /.category-filter -->
        
      
    </div><!-- /.body -->
</div></div>
        <div class="col-md-9 eqHeight" id="products-side">
            <h1><?= lang('products') ?></h1>
            <div class="product-sort gradient-color">
                <div class="row">
                    <div class="ord col-sm-4">
                        <div class="form-group">
                            <select class="selectpicker order form-control" data-style="btn-green" data-order-to="order_new">
                                <option <?= isset($_GET['order_new']) && $_GET['order_new'] == "desc" ? 'selected' : '' ?> <?= !isset($_GET['order_new']) || $_GET['order_new'] == "" ? 'selected' : '' ?> value="desc"><?= lang('new') ?> </option>
                                <option <?= isset($_GET['order_new']) && $_GET['order_new'] == "asc" ? 'selected' : '' ?> value="asc"><?= lang('old') ?> </option>
                            </select>
                        </div>
                    </div>
                    <div class="ord col-sm-4">
                        <div class="form-group">
                            <select class="selectpicker order form-control" data-style="btn-green" data-order-to="order_price" title="<?= lang('price_title') ?>..">
                                <option label="<?= lang('not_selected') ?>"></option>
                                <option <?= isset($_GET['order_price']) && $_GET['order_price'] == "asc" ? 'selected' : '' ?> value="asc"><?= lang('price_low') ?> </option>
                                <option <?= isset($_GET['order_price']) && $_GET['order_price'] == "desc" ? 'selected' : '' ?> value="desc"><?= lang('price_high') ?> </option>
                            </select>
                        </div>
                    </div>
                    <div class="ord col-sm-4">
                        <div class="form-group">
                            <select class="selectpicker order form-control" data-style="btn-green" data-order-to="order_procurement" title="<?= lang('procurement_title') ?>..">
                                <option label="<?= lang('not_selected') ?>"></option>
                                <option <?= isset($_GET['order_procurement']) && $_GET['order_procurement'] == "desc" ? 'selected' : '' ?> value="desc"><?= lang('procurement_desc') ?> </option>
                                <option <?= isset($_GET['order_procurement']) && $_GET['order_procurement'] == "asc" ? 'selected' : '' ?> value="asc"><?= lang('procurement_asc') ?> </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if (!empty($products)) {
                foreach ($products as $product) {
                    ?>
                    <div class="col-sm-6 col-md-4 product-inner">
                        <a href="<?= LANG_URL . '/' . $product['url'] ?>">
                            <img src="<?= base_url('attachments/shop_images/' . $product['image']) ?>" alt="" class="img-responsive" style="max-width: 52%;">
                        </a>
                        <h3 class="title"><?= $product['title'] ?></h3>
                        <span class="price">₹.<?= $product['price']; ?></span>
						</br>
                        <a class="add-to-cart le-button" data-goto="<?= LANG_URL . '/checkout' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                            ADD TO CART
                        </a>
                    </div>
                    <?php
                }
            } else {
                ?>
                <script>
                    $(document).ready(function () {
                        ShowNotificator('alert-info', '<?= lang('no_results') ?>');
                    });
                </script>
                <?php
            }
            ?>
        </div>
    </div>
    <?php if ($links_pagination != '') { ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= $links_pagination ?>
            </div>
        </div>
    <?php } ?>
</div>
<script src="<?= base_url('assets/bootstrap-select-1.12.1/js/bootstrap-select.min.js') ?>"></script>
<style>.title{font-size: 16px;
    font-weight: 700;
    display: flex;
    -webkit-box-pack: justify;
    justify-content: space-between;
    margin-bottom: 4px;
    -webkit-box-align: center;
align-items: center;}<style>