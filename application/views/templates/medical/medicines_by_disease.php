<?php  $this->load->view('templates/medical/top_head'); ?>
<section id="category-grid">
    <div class="container">
   <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>Product Filters</h1>
    <div class="body bordered">
        
        <div class="category-filter">
            <h2>Medicine </h2>
            <hr>
            <ul> <?php $i=1; foreach($disease as $disease){ if($i <=10){?>
                <li>
				<a href="<?php echo base_url(); ?>medicines-by-disease/<?php echo $disease['id'];  ?>">
				<label><?php echo $disease['name'];  ?></label> <span class="pull-right"></span></a>
				</li>
				
			<?php $i++;}} ?> 
            
			</ul>
        </div><!-- /.category-filter -->
        
      
    </div><!-- /.body -->
</div><!-- /.widget -->

       <?php  $this->load->view('templates/medical/side_bar_products'); ?>   
            <div class="widget">
	<div class="simple-banner">
		<a href="#"><img alt="" class="img-responsive" src="assets/images/blank.gif" data-echo="assets/images/banner/banner-simple.jpg" /></a>
	</div>
</div>
         </div>
  <div class="col-xs-12 col-sm-9 no-margin wide sidebar">

    <section id="gaming">
    <div class="grid-list-products">
        <h2 class="section-title"><?php echo $disease_name['name'];?></h2>
        <?php // echo "<pre>"; print_r($product_by_brands);?>
          
		
        <div class="control-bar">
         <?php echo $disease_name['name'];?>
            <div class="grid-list-buttons">
                <ul>
                    <li class="grid-list-button-item active"><a data-toggle="tab" href="#grid-view"><i class="fa fa-th-large"></i> Grid</a></li>
                    <li class="grid-list-button-item "><a data-toggle="tab" href="#list-view"><i class="fa fa-th-list"></i> List</a></li>
                </ul>
            </div>
        </div><!-- /.control-bar -->
                                
        <div class="tab-content">
            <div id="grid-view" class="products-grid fade tab-pane in active">
                
                <div class="product-grid-holder">
                    <div class="row no-margin">
                       <?php 
					   if (!empty($product_by_disease)) {
					   foreach($product_by_disease as $product){?> 
                        <div class="col-xs-12 col-sm-4 no-margin product-item-holder hover">
                          
						   <div class="product-item">
                                <div class="ribbon red"><span>sale</span></div> 
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                </div>
                                <div class="body">
                                    <div class="label-discount green">
									<?php
$oldFigure = $product['price'];
 $newFigure = $product['old_price'];
 
 $percentChange = (1 - $oldFigure / $newFigure) * 100;
 
 
$total_off= round($percentChange);
									if($total_off >0){
									 echo "-" .round($percentChange) . "%"." sale";
									}?></div>
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $product['url'] ?>"><?php echo $product['title']; ?></a>
                                    </div>
									<?php 
									$b=$this->Brands_model->get_brand_name($product['brand_id'])?>
                                    <div class="brand"><?php echo $b['name'];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev price">₹.<?php echo number_format($product['price'],2);?></div>
                                    <div class="price-current pull-right">₹.<?php echo number_format($product['old_price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
									
                                        
 <h3><?//= //$product['title'] ?></h3>
                        
                        <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a></div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="#">compare</a>
                                    </div>
                                </div>
                            </div><!-- /.product-item -->
						   
						</div>
<?php }  } else { ?>
                <script>
                    jQuery(document).ready(function () {
                        ShowNotificator('alert-info', '<?= lang('no_results') ?>');
                    });
                </script>
                <?php
            }
            ?>
                        

                     </div>
                </div>
                
             </div>

            <div id="list-view" class="products-grid fade tab-pane ">
                <div class="products-list">
                   <?php foreach($product_by_disease as $product){?>
                    <div class="product-item product-item-holder">
                        <div class="ribbon red"><span>sale</span></div> 
                        <div class="ribbon blue"><span>new!</span></div>
                        <div class="row">
                            <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                </div>
                            </div><!-- /.image-holder -->
                            <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                <div class="body">
                                    <div class="label-discount green"><?php
$oldFigure = $product['price'];
 $newFigure = $product['old_price'];
 
 $percentChange = (1 - $oldFigure / $newFigure) * 100;
 
 
$total_off= round($percentChange);
									if($total_off >0){
									 echo "-" .round($percentChange) . "%"." sale";
									}?></div>
                                    <div class="title">
                                        <a href="#"><?php echo $product['title']; ?></a>
                                    </div>
                                    <div class="brand"><?php echo $b['name']?></div>
                                    <div class="excerpt">
                                        <p><?php echo $product['basic_description']; ?></p>
                                    </div>
                                    <div class="addto-compare">
                                        <a class="btn-add-to-compare" href="#">add to compare list</a>
                                    </div>
                                </div>
                            </div><!-- /.body-holder -->
                            <div class="no-margin col-xs-12 col-sm-3 price-area">
                                <div class="right-clmn">
                                    <div class="price-current"><?php echo Rs. number_format($product['price'],2);?></div>
                                    <div class="price-prev"><?php echo Rs. number_format($product['old_price'],2);?></div>
                                    <div class="availability"><label>availability:</label><span class="available">  in stock</span></div>
                                    <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                    <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                </div>
                            </div><!-- /.price-area -->
                        </div><!-- /.row -->
                    </div><!-- /.product-item -->

<?php  } ?>
                   </div><!-- /.products-list -->

              </div><!-- /.products-grid #list-view -->

        </div>
    </div>

</section>           
        </div>
         
    </div>
</section>
