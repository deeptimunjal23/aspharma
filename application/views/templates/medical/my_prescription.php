<?php  $this->load->view('templates/medical/top_head'); ?>

<div class="container user-page">
    <div class="row">
       <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">

<div class="widget">
    <h1>My Account</h1>
    <div class="body bordered">
        
       <?php  $this->load->view('templates/medical/user_sidebar'); ?>
      
    </div><!-- /.body -->
</div><!-- /.widget -->

            <div class="widget">
	<h1 class="border">special offers</h1>
	<ul class="product-list">
        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img alt="" src="<?php echo base_url();?>assets/med/images/products/product-small-01.jpg">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Netbook Acer </a>
                    <div class="price">
                        <div class="price-prev">$2000</div>
                        <div class="price-current">$1873</div>
                    </div>
                </div>  
            </div>
        </li>

        <li>
            <div class="row">
                <div class="col-xs-4 col-sm-4 no-margin">
                    <a href="#" class="thumb-holder">
                        <img width="200" height=" 100px" alt="" src="<?php echo base_url()?>/assets/med/images/products/Pro-1.png">
                    </a>
                </div>
                <div class="col-xs-8 col-sm-8 no-margin">
                    <a href="#">Order History</a>
                    <div class="price">
                        <div class="price-prev"></div>
                        <div class="price-current"></div>
                    </div>
                </div>  
            </div>
        </li>
 </ul>
</div><!-- /.widget -->
           
         </div>
        <div class="col-sm-9" style="margin-top: 1px;">
           
  <table class="responsive-table">
    <caption>My Uploaded Prescription</caption>
    <thead>
	
      <tr>
        <th scope="col">Image</th> <th scope="col">Order Id</th>
        <th scope="col">Patient Name</th>
        
        <th scope="col">Payment Status</th>
       
        <th scope="col">Order Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="7"></td>
      </tr>
    </tfoot>
    <tbody>
	<?php
                        if (!empty($prescriptions)) {
                            foreach ($prescriptions as $order) {
								if(!empty($order["patient_name"])){
                                ?>
      <tr>
        <th scope="row"><?php	if(!empty($order['prescription_image'])) {
									$file_name =$order['prescription_image'];
									$fileUrl = base_url().'attachments/prescription/'.$file_name;
?>
                    <img src="<?php echo $fileUrl; ?>" width="100" class="img-thumbnail" />
<?php } ?></th>
        <td data-title="Released"><?php echo $order["order_id"];?></td>
        <td data-title="Studio"><?php echo ucfirst($order['patient_name']);?></td>
        
        <td data-title="Domestic Gross" data-type="currency"><strong><?php echo $order['payment_status'];?></strong></td>
        
        <td data-title="Budget" data-type="currency"><strong><?php echo $order['order_status'];?></strong></td> 
		<td data-title="Budget" data-type="currency">
		<a href="<?php echo base_url();?>orders-detail/<?php  echo $order['prescription_id'];?>" class="btn btn-sucess confirm-sucess">View</a>
		<?php if($order['is_deleted']=="0"){?>
		<a href="<?php echo base_url();?>delpre/<?php  echo $order['prescription_id'];?>" class="btn btn-danger confirm-delete">Cancel</a>
		<?php }?>
		
		
		</td>
      </tr>
								<?php }
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="5"><?= lang('usr_no_orders') ?></td>
                            </tr>
                        <?php } ?>
      
    </tbody>
  </table>

        </div>
    </div>
</div>