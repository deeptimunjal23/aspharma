<section id="recently-reviewd" class="wow fadeInUp">
    <div class="container">
        <div class="carousel-holder hover">
            <?php $feature_products = $this->Products_model->get_feature_products(); ?>
            <div class="title-nav">
                <h2 class="h1">FEATURED PRODUCTS</h2>
                <div class="nav-holder">
                    <a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
                    <a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
                </div>
            </div><!-- /.title-nav -->
            
            <div class="row">


            <div id="owl-recently-viewed" class="owl-carousel product-grid-holder"  data-plugin-options="{'loop': false, 'autoplay': true, 'autoplayTimeout': 3000}">

			<?php foreach($feature_products as $product){?>
                <div class="no-margin carousel-item product-item-holder size-small hover">
				
                <div class="product-item">
                                <div class="ribbon blue"><span>new!</span></div> 
                                <div class="image">
                                    <img alt="" src="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$product['image']; ?>" />
                                </div>
                                <div class="body">
                                    <div class="label-discount clear"></div>
                                    <div class="title">
                                        <a href="<?= LANG_URL . '/' . $product['url'] ?>"><?php echo $product['title']; ?></a>
                                    </div><?php 
									$b=$this->Brands_model->get_brand_name($product['brand_id'])?>
                                    <div class="brand"><?php echo $b['name'];?></div>
                                </div>
                                <div class="prices">
                                    <div class="price-prev"><?php echo Rs. number_format($product['old_price'],2);?></div>
                                    <div class="price-current pull-right"> <?php  echo Rs. number_format($product['price'],2);?></div>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
                                        <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>">
                         <span class="le-button">  add to cart</span>
                        </a>
                                    </div>
                                    <div class="wish-compare">
                                        <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $product['id']; ?>">add to wishlist</a>
                                        <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $product['id']; ?>">compare</a>
                                    </div>
                                </div>
                            </div>
                   
                </div> <?php } ?><!-- /.product-item-holder -->

                 <div class="no-margin carousel-item product-item-holder size-small hover">
                    <!-- /.product-item -->
                </div><!-- /.product-item-holder -->

                <!-- /.product-item-holder -->

                <!-- /.product-item-holder -->
            </div><!-- /#recently-carousel -->
            </div>

        </div><!-- /.carousel-holder -->
    </div><!-- /.container -->
</section><!-- /#recently-reviewd -->

