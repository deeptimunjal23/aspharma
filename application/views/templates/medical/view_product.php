<?php  $this->load->view('templates/medical/top_head'); ?>

<div id="single-product">
  <div class="container" style="margin-top: 20px;">
    <div class="row">
      <div class="col-md-12">
      <div class="no-margin col-md-5 gallery-holder">
        <div class="product-item-holder size-big single-product-gallery small-gallery">
          <div id="owl-single-product">
            <div class="single-product-gallery-item" id="slide1"> <a data-rel="prettyphoto" href="<?= base_url('/attachments/shop_images/' . $product['image']) ?>"> <img class="img-responsive" alt="" src="<?= base_url('/attachments/shop_images/' . $product['image']) ?>" data-echo="<?= base_url('/attachments/shop_images/' . $product['image']) ?>" /> </a> </div>
          </div>
          
          <div class="single-product-gallery-thumbs gallery-thumbs">
            <div id="owl-single-product-thumbnails">
              <?php
            if ($product['folder'] != null) {
                $dir = "attachments/shop_images/" . $product['folder'] . '/';
                ?>
              <?php
                    if (is_dir($dir)) {
                        if ($dh = opendir($dir)) {
                            $i = 1;
                            while (($file = readdir($dh)) !== false) {
                                if (is_file($dir . $file)) {
                                    ?>
              <a class="horizontal-thumb <?php if($i=="1"){echo "active";}?>" data-target="#owl-single-product" data-slide="0" href="#slide1"> <img width="67" alt="<?= str_replace('"', "'", $product['title']) ?>" src="<?= base_url($dir . $file) ?>" data-echo="<?= base_url($dir . $file) ?>" /> </a>
              <?php
                                    $i++;
                                }
                            }
                            closedir($dh);
                        }
                    }
                    ?>
            </div>
            <?php
            }
            ?>
          
            
            <div class="nav-holder left hidden-xs"> <a class="prev-btn slider-prev" data-target="#owl-single-product-thumbnails" href="#prev"></a> </div>
           
            
            <div class="nav-holder right hidden-xs"> <a class="next-btn slider-next" data-target="#owl-single-product-thumbnails" href="#next"></a> </div>
          
            
          </div>
          
        </div>
        </div>
      </div>
      <div class="no-margin col-md-7 body-holder">
        <div class="info-right">
          <div class="star-holder inline">
            <div class="star" data-score="41"></div>
          </div>
          <div class="availability">
            <label>Availability:</label>
            <?php if($product['quantity']>0){?>
            <span class="available"> in stock</span>
            <?php }else{?>
            <span style="color:red">Out Of stock</span>
            <?php }?>
          </div>
          <div class="title"><a href="#">
            <?= $product['title'] ?>
            </a></div>
          <?php 	$brand =$this->Brands_model->get_brand_name($product['brand_id']); ?>
          <div class="brand"><?php echo $brand['name'];?></div>
          <div class="social-row"> <span class="st_facebook_hcount"></span> <span class="st_twitter_hcount"></span> <span class="st_pinterest_hcount"></span> </div>
          <div class="buttons-holder"> <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $product['id']; ?>">add to wishlist</a> <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $product['id']; ?>">add to compare list</a> </div>
          <div class="excerpt">
            <p>
              <?= $product['basic_description'] ?>
            </p>
          </div>
          <div class="prices">
            <div class="price-current"> <?php echo Rs. number_format($product['price'],2);?></div>
            <div class="price-prev"> <?php echo Rs. number_format($product['old_price'],2);?></div>
          </div>
          <div class="qnt-holder">
            <?php if($product['quantity']
						>1){?>
            <div class="le-quantity">
              <form>
                <a class="minus" href="#reduce"></a>
                <input name="quantity" readonly="readonly" type="text" value="1" />
                <a class="plus" href="#add"></a>
              </form>
            </div>
            <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $product['id'] ?>"> <span class="le-button huge"> add to cart</span> </a>
            <?php }else{?>
            <span style="color:red">Out Of stock</span>
            <?php } ?>
          </div>
        </div>
        
        
      </div>
    </div>
  </div>
</div>
<section id="single-product-tab">
  <div class="container">
    <div class="tab-holder">
      <ul class="nav nav-tabs simple" >
        <li class="active"><a href="#description" data-toggle="tab">Description</a></li>
        <li><a href="#additional-info" data-toggle="tab">Additional Information</a></li>
      </ul>
      <!-- /.nav-tabs -->
      
      <div class="tab-content">
        <div class="tab-pane active" id="description">
          <?php  echo $product["description"];?>
        </div>
        <!-- /.tab-pane #description -->
        
        <div class="tab-pane" id="additional-info">
          <p>
          <ul class="tabled-data">
            <?php //print_r($product); ?>
            <?php if(!empty($product["weight"])){ ?>
            <li>
              <label>weight</label>
              <div class="value"><?php echo $product["weight"];?></div>
            </li>
            <?php } ?>
            <?php if(!empty($product["unit"])){ ?>
            <li>
              <label>Unit</label>
              <div class="value">
                <?php $unit=$this->Public_model->get_single_unit($product['unit']); //echo $b['name']; 
							echo $unit["name"];?>
              </div>
            </li>
            <?php } ?>
            <?php if(!empty($product["medicine_type"])){ ?>
            <li>
              <label>Medicine Type</label>
              <div class="value"><?php echo $product["medicine_type"];?></div>
            </li>
            <?php } ?>
            <?php if(!empty($product["medicinec_category"])){ ?>
            <li>
              <label>Medicine Category</label>
              <div class="value"><?php echo $product["medicinec_category"];?></div>
            </li>
            <?php } ?>
            <?php if(!empty($product["brand_id"])){ ?>
            <li>
              <label>Brand</label>
              <div class="value">
                <?php
									$b=$this->Brands_model->get_brand_name($product['brand_id']); echo $b['name'];?>
              </div>
            </li>
            <?php } ?>
          </ul>
          </p>
        </div>
        <!-- /.tab-pane #additional-info -->
        
        <div class="tab-pane" id="reviews">
          <div class="comments">
            <div class="comment-item">
              <div class="row no-margin">
                <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                  <div class="avatar"> <img alt="avatar" src="assets/images/default-avatar.jpg"> </div>
                  <!-- /.avatar --> 
                </div>
                <!-- /.col -->
                
                <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                  <div class="comment-body">
                    <div class="meta-info">
                      <div class="author inline"> <a href="#" class="bold">John Smith</a> </div>
                      <div class="star-holder inline">
                        <div class="star" data-score="4"></div>
                      </div>
                      <div class="date inline pull-right"> 12.07.2013 </div>
                    </div>
                    <!-- /.meta-info -->
                    <p class="comment-text"> Integer id purus ultricies nunc tincidunt congue vitae nec felis. Vivamus sit amet nisl convallis, faucibus risus in, suscipit sapien. Vestibulum egestas interdum tellus id venenatis. </p>
                    <!-- /.comment-text --> 
                  </div>
                  <!-- /.comment-body --> 
                  
                </div>
                <!-- /.col --> 
                
              </div>
              <!-- /.row --> 
            </div>
            <!-- /.comment-item -->
            
            <div class="comment-item">
              <div class="row no-margin">
                <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                  <div class="avatar"> <img alt="avatar" src="assets/images/default-avatar.jpg"> </div>
                  <!-- /.avatar --> 
                </div>
                <!-- /.col -->
                
                <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                  <div class="comment-body">
                    <div class="meta-info">
                      <div class="author inline"> <a href="#" class="bold">Jane Smith</a> </div>
                      <div class="star-holder inline">
                        <div class="star" data-score="5"></div>
                      </div>
                      <div class="date inline pull-right"> 12.07.2013 </div>
                    </div>
                    <!-- /.meta-info -->
                    <p class="comment-text"> Integer id purus ultricies nunc tincidunt congue vitae nec felis. Vivamus sit amet nisl convallis, faucibus risus in, suscipit sapien. Vestibulum egestas interdum tellus id venenatis. </p>
                    <!-- /.comment-text --> 
                  </div>
                  <!-- /.comment-body --> 
                  
                </div>
                <!-- /.col --> 
                
              </div>
              <!-- /.row --> 
            </div>
            <!-- /.comment-item -->
            
            <div class="comment-item">
              <div class="row no-margin">
                <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                  <div class="avatar"> <img alt="avatar" src="assets/images/default-avatar.jpg"> </div>
                  <!-- /.avatar --> 
                </div>
                <!-- /.col -->
                
                <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                  <div class="comment-body">
                    <div class="meta-info">
                      <div class="author inline"> <a href="#" class="bold">John Doe</a> </div>
                      <div class="star-holder inline">
                        <div class="star" data-score="3"></div>
                      </div>
                      <div class="date inline pull-right"> 12.07.2013 </div>
                    </div>
                    <!-- /.meta-info -->
                    <p class="comment-text"> Integer id purus ultricies nunc tincidunt congue vitae nec felis. Vivamus sit amet nisl convallis, faucibus risus in, suscipit sapien. Vestibulum egestas interdum tellus id venenatis. </p>
                    <!-- /.comment-text --> 
                  </div>
                  <!-- /.comment-body --> 
                  
                </div>
                <!-- /.col --> 
                
              </div>
              <!-- /.row --> 
            </div>
            <!-- /.comment-item --> 
          </div>
          <!-- /.comments --> 
          
        </div>
        <!-- /.tab-pane #reviews --> 
      </div>
      <!-- /.tab-content --> 
      
    </div>
    <!-- /.tab-holder --> 
  </div>
  <!-- /.container --> 
</section>
<!-- /#single-product-tab -->

<section id="recently-reviewd" class="wow fadeInUp">
  <div class="container">
    <div class="carousel-holder hover">
      <div class="title-nav">
        <h2 class="h1">Recently Viewed</h2>
        <div class="nav-holder"> <a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a> <a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a> </div>
      </div>
      <!-- /.title-nav -->
      
      <div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
        <?php 
 //print_r($viewed_product);
  foreach($viewed_product as $vied){
	  if($vied["product_id"] !==$product["id"]){
	  ?>
        <div class=" no-margin carousel-item product-item-holder size-small hover">
          <div class="product-item">
            <div class="ribbon blue"><span>new!</span></div>
            <div class="image">
              <?php $data['product'] = $this->Public_model->getOneProduct($vied["product_id"]); 
						//print_r($data['product']);?>
              <img alt="" src="<?php echo base_url("attachments/shop_images/").$data['product']['image']; ?>" data-echo="<?php echo base_url("attachments/shop_images/").$data['product']['image']; ?>" /> </div>
            <div class="body">
              <div class="title"> <a href="<?php echo $data['product']['url']; ?>"><?php echo $data['product']['title']; ?></a> </div>
              <div class="brand">
                <?php
									$b=$this->Brands_model->get_brand_name($data['product']['brand_id']); echo $b['name'];?>
              </div>
            </div>
            <div class="prices">
              <div class="price-current text-right">₹.<?php echo number_format($data['product']['price'],2);?></div>
            </div>
            <div class="hover-area">
              <div class="add-cart-button"> <a class="add-to-cart add-cart-button" data-goto="<?= LANG_URL . '/shopping-cart' ?>" href="javascript:void(0);" data-id="<?= $data['product']['id'] ?>"> <span class="le-button"> add to cart</span> </a> </div>
              <div class="wish-compare"> <a class="btn-add-to-wishlist" href="<?php echo base_url() ?>add-to-wish-list/<?php echo $data['product']['id']; ?>">Add to Wishlist</a> <a class="btn-add-to-compare" href="<?php echo base_url() ?>add_to_compair_list/<?php echo $data['product']['id']; ?>">Compare</a> </div>
            </div>
          </div>
        </div>
        <?php } 
	  }?>
      </div>
      <!-- /#recently-carousel --> 
      
    </div>
    <!-- /.carousel-holder --> 
  </div>
  <!-- /.container --> 
</section>
<!-- /#recently-reviewd --> 
<script src="http://w.sharethis.com/button/buttons.js"></script>
<style>
<style>.table td,
.table th {
  font-size: 0.875rem; }
.table-slick td{
  padding:6px !important;	
}
.table-bordered td,
.table-bordered th {
  border: 1px solid rgba(0, 0, 0, 0.05); }

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #f2f2f2; }

.table tbody td {
  vertical-align: unset; }

.table tbody + tbody {
  border-top: 2px solid #f2f2f2; }

.table .table {
  background-color: #f3f3f3; }

.table-sm td,
.table-sm th {
  padding: 0.75rem 1rem; }

.table-bordered,
.table-bordered td,
.table-bordered th {
  border: 1px solid #f2f2f2; }

.table-bordered thead td,
.table-bordered thead th {
  border-bottom-width: 2px; }

.table-inverse.table-bordered,
.table-responsive.table-bordered {
  border: 0; }

.table-active,
.table-active > td,
.table-active > th,
.table-hover tbody tr:hover,
.table-striped tbody tr:nth-of-type(odd) {
  background-color: #fafafa; }

.table-hover .table-active:hover,
.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: #e5e5e5; }

.table-success,
.table-success > td,
.table-success > th {
  background-color: #57d59f; }

.table-hover .table-success:hover,
.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #43d093; }

.table-info,
.table-info > td,
.table-info > th {
  background-color: #2ebcfc; }

.table-hover .table-info:hover,
.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #14b4fc; }

.table-warning,
.table-warning > td,
.table-warning > th {
  background-color: #ffc721; }

.table-hover .table-warning:hover,
.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #ffc107; }

.table-danger,
.table-danger > td,
.table-danger > th {
  background-color: #ff6f6c; }

.table-hover .table-danger:hover,
.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #ff5652; }

.thead-inverse th {
  color: #f3f3f3;
  background-color: #404c54; }

.thead-default th {
  color: #464a4c;
  background-color: #fbfbfb; }

.table-inverse {
  color: #f3f3f3;
  background-color: #404c54; }

.table-responsive {
  /* display: block; */
  width: 50%;
  overflow-x: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar; }

.table thead th {
  border-bottom-width: 1px; }

.table tr[class*=table-] td,
.table tr[class*=table-] th,
.table tr[class*=table-] + tr td,
.table tr[class*=table-] + tr th {
  border: 0; }

.table:not(.table-bordered) > tbody:first-child td,
.table:not(.table-bordered) > tbody:first-child th,
.table:not(.table-bordered) > thead:first-child td,
.table:not(.table-bordered) > thead:first-child th {
  border-top: 0; }

.table-inverse td,
.table-inverse th,
.table-inverse thead th {
  border-color: #505b63; }

.table > tbody > tr > td,
.table > tbody > tr > th,
.table > tfoot > tr > td,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > thead > tr > th {
  border-top: 1px solid rgba(243, 243, 243, 0.7);
  font-size: 0.875rem; }

.table-no-border > tbody > tr > td,
.table-no-border > tbody > tr > th,
.table-no-border > tfoot > tr > td,
.table-no-border > tfoot > tr > th,
.table-no-border > thead > tr > td,
.table-no-border > thead > tr > th {
  border-top: 0; }
html {
    font-size: 100% !important;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
}
.table-profile > tbody > tr > td:not(.td-actions) {
  font-size: 0.875rem; }

.table-profile > tbody > tr > td.td-label {
  font-size:  62.5%; 
  font-family: "Roboto", sans-serif;
  font-weight: 500;
  color: #000000;
  text-transform: uppercase;
  white-space: nowrap; }

.table-profile > tbody > tr > td.td-label > span {
  display: block;
  padding-right: 1rem; }

.table-profile > tbody > tr > td.td-actions > .btn {
  opacity: 0; }

.table-profile > tbody > tr:hover > td.td-actions > .btn {
  opacity: 1; }

.table-cart > thead > tr > th {
  padding: 12px 0;
  border: 0;
  font-weight: 400; }

.table-cart > tbody > tr > td,
.table-cart > tbody > tr > th,
.table-cart > tfoot > tr > td,
.table-cart > tfoot > tr > th,
.table-cart > thead > tr > td,
.table-cart > thead > tr > th {
  border-color: rgba(243, 243, 243, 0.7);
  vertical-align: middle; }

.table-cart > tbody > tr > td {
  padding: 1rem 0 !important; }

.table-cart .cart-item-img {
  width: 120px; }

.table-cart .cart-item-img img {
  width: 100%; }

.table-cart .cart-item-content {
  vertical-align: middle;
  width: 100%; }

.table-cart .cart-item-content .cart-item-title {
  font-weight: 500;
  font-size: 1rem;
  color: #2b2b2c; }

.table-cart .cart-item-content .cart-item-title:hover {
  color: #5E32E1; }

.table-cart .cart-item-content .label-quantity {
  color: #818a91;
  font-weight: 400;
  font-size: 0.875rem; }

.table-cart .cart-item-content .label-value {
  font-weight: 600; }

.table-cart .cart-item-unit-price {
  vertical-align: middle;
  font-size: 18px; }

.table-cart .cart-item-price {
  width: 20%;
  vertical-align: middle;
  text-align: right; }

.table-cart .cart-item-price > .price {
  font-weight: 600;
  font-size: 1.25rem;
  display: block;
  color: #2b2b2c; }

.table-cart .cart-item-price > .price.discount {
  font-weight: 500;
  font-size: 0.875rem;
  text-decoration: line-through;
  color: #ff3b30;
  margin-top: 8px; }

.table-cart .cart-item-price > .price.savings {
  font-weight: 400;
  font-size: 0.75rem; }

.table-cart .cart-item-count {
  vertical-align: middle; }

.table-cart .label-subtotal {
  padding-top: 15px;
  text-align: center;
  font-size: 14px;
  text-transform: uppercase; }

.table-cart .table-cart-footer {
  padding: 15px;
  border-top: 1px solid rgba(243, 243, 243, 0.7); }

.table-cart .cart-items {
  display: block;
  padding: 15px;
  font-size: 14px;
  font-weight: 500;
  background: #eee;
  color: #ccc;
  border-bottom: 1px solid #ccc; }
 table td {
    padding: 7px 5px;
    border-bottom: 1px solid #eaeaea;
    font-size: 16px;
    vertical-align: top;
}.tdwdt {
    width: 30%;
    color: #848484;
}<style>
</style>
