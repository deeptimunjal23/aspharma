<?php  $this->load->view('templates/medical/top_head'); ?>

<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">
			
			<div class="col-md-9">
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered"></h2>
					<p>Start by entering the name of medicine or healthcare products to get faster delivery</p>
					<div class="searchBox"><div class="searchText">Start by entering the name of medicine or healthcare products to get faster delivery</div>
					<div class="">
					<input class="typeahead form-control"  placeholder="Search for medicines or healthcare products " type="text" >
					
					</div>
					</div>
</section>
			</div>

			<div class="col-md-3">
				<section class="section register inner-left-xs">
					
					<p>You can also start by uploading a photo of your prescription</p>
					<small>Photo should not be larger than 10 MB</small>

				<h4 class="semi-bold"><button class="PEBtn">Attach Prescription</button></h4>

					<ul class="list-unstyled list-benefits">
						<li>valid prescription Contain</li>
						<li><i class="fa fa-check primary-color"></i> Upload Clear Image<br>
						<small>ensure that picture is taken with clear devices</small></li>
						<li><i class="fa fa-check primary-color"></i> government regulation with valid prescription</li>
					</ul>

				</section><!-- /.register -->

			</div><!-- /.col -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</main>
<style>.uploadPrescriptionContainer .searchBoxContainer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
} .searchBox {
    width: 95%;
    padding: 24px;
    background-color: #34ade2;
    border-radius: 6px;
} .order{    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border: none;
    width: 90%;
    height: 55px;
    padding: 10px;
    border-radius: 30px;}.searchText{color: #fff;
    padding: 10px;}
	.PEBtn{padding: 10px;
    background: #34ade2;
    border: none;
    color: #fff;
    /* background-color: #013446; */
    color: #fff;
    text-align: center;
    cursor: pointer;
    height: 40px;
    width: 100%;
    border-radius: 6px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    transition: all .3s ease;
    box-shadow: 0 3px 20px 0 rgba(0,0,0,.23);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;}</style>
	
    
    
<?php  $this->load->view('templates/medical/_parts/footer'); ?><script>
	jQuery(document).ready(function() {
			
	jQuery(".typeahead ").change(function() { 
	
	//alert('in jis');
	});
	});
	</script> 