<?php  $this->load->view('templates/medical/top_head'); ?>
<div class="container">
		<div class="row">
			<?php
                if ($this->session->flashdata('resultSend')) {
                    ?>
                    <hr>
                    <div class="alert alert-info"><?= $this->session->flashdata('resultSend') ?></div>
                    <hr>
                <?php }
                ?>
			<div class="col-md-8">
				<section class="section leave-a-message">
					<h2 class="bordered">Ask our Expert </h2>
					<p>Please write to us we will get back to you as soon as possible.</p>
					<form id="contact-form" class="contact-form cf-style-1 inner-top-xs" method="post" action="<?php echo base_url()?>ask-expert">
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Your Name*</label>
                                 <input class="le-input" name="name" placeholder="Enter Your Name" required>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Your Email*</label>
                                <input class="le-input" name="email" placeholder="Enter email" required>
                            </div>
                        </div><!-- /.field-row -->
                        
                        <div class="field-row">
                            <label>Subject</label>
                            <input type="text" class="le-input" name="subject" placeholder="subject">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Your Message</label>
                            <textarea rows="8" class="le-input" name="message"></textarea>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge" name="send">Send Message</button>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.contact-form -->
				</section><!-- /.leave-a-message -->
			</div><!-- /.col -->

			<div class="col-md-4">
				<section class="our-store section inner-left-xs">
					<h2 class="bordered">Our Store</h2>
					<address>
						
						X-17, F Block,<br> Pocket X, Okhla Phase II, Okhla Industrial Area, New Delhi, Delhi 110020
					</address>
					<h3>Hours of Operation</h3>
					<ul class="list-unstyled operation-hours">
						<li class="clearfix">
							<span class="day">Monday:</span>
							<span class="pull-right hours">12-6 PM</span>
						</li>
						<li class="clearfix">
							<span class="day">Tuesday:</span>
							<span class="pull-right hours">12-6 PM</span>
						</li>
						<li class="clearfix">
							<span class="day">Wednesday:</span>
							<span class="pull-right hours">12-6 PM</span>
						</li>
						<li class="clearfix">
							<span class="day">Thursday:</span>
							<span class="pull-right hours">12-6 PM</span>
						</li>
						<li class="clearfix">
							<span class="day">Friday:</span>
							<span class="pull-right hours">12-6 PM</span>
						</li>
						<li class="clearfix">
							<span class="day">Saturday:</span>
							<span class="pull-right hours">12-6 PM</span> 
						</li>
						<li class="clearfix">
							<span class="day">Sunday</span>
							<span class="pull-right hours">Closed</span>
						</li>
					</ul>
					<h3>Career</h3>
					<p>If you're interested in employment opportunities at MediaCenter, please email us: <a href="mailto:contact@yourstore.com">contact@aspharmapvtltd.com</a></p>
				</section><!-- /.our-store -->
			</div><!-- /.col -->

		</div><!-- /.row -->
	</div>